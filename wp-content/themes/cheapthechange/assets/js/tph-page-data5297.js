'use strict';
if (typeof tph != 'undefined' && typeof tph.utility != 'undefined') {
    jQuery(document).ready(function($) {
        // set up the object to receive data
        tph.page_data = {};

        // add mobile classes to body
        if ('ontouchstart' in window || 'ontouch' in window) {
            $('body').addClass('touch-device');
        } else {
            $('body').addClass('non-touch-device');
        };

        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
            $('body').addClass('ios-device');
        } else {
            $('body').addClass('non-ios-device');
        };

        // set page data for easy access in other scripts
        // zero is fallback when post_id can't be determined.
        if ($("link[rel='shortlink']").length && $("link[rel='shortlink']").attr('href').indexOf('?p=') != -1) {
            tph.page_data.post_id = $("link[rel='shortlink']").attr('href').split('?p=')[1];
        } else {
            tph.page_data.post_id = 0;
        }
        tph.page_data.aff_id = tph.utility.get_browser_query_parameter('aff_id') || 2;
    })
}