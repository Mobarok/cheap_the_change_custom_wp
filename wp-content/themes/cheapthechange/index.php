
<?php
/*
    Template Name:Home Page
*/
get_header();
?>

    <div class="page-container home-page">

        <?php

            $num_of_post = array('numberposts' => '1', 'category__not_in' => array( 86 ) );
            $recent_post = wp_get_recent_posts($num_of_post);
          ?>
        <div class="row masthead-block flex-row">
            <div class="container flex-container">
        <?php
          foreach ($recent_post as $recent){
        ?>
                    <div class="at-masthead-block col-sm-12 col-md-9 masthead-block-article editable flex-row-inline">
                <div class="at-masthead-image col-xs-12 col-sm-8 masthead-block-article-image">
                    <?php
                        if(has_post_thumbnail($recent['ID'])){
                            $img = get_the_post_thumbnail($recent['ID'],'thumbnail');
                        }
                    ?>
                    <a href="<?php echo get_permalink($recent["ID"])?>" class="has-youtube-video-masthead">
                        <div class="four_by_six"><?php echo $img; ?>
						</div>        
					</a>
                </div>

                        <?php
                            $category  = get_the_category($recent['ID']);
                        ?>
        <div class="col-xs-12 col-sm-4 masthead-block-article-content vcenter">
            <div class="masthead-block-article-content-container">
                <a href="<?= $category[count($category)-1]->slug;?>" class="at-masthead-cat masthead-block-article-content-category category-title category-heading verticals-heading">
                    <?php
                        echo $category[count($category)-1]->name;
                    ?>
                 </a>
                <a href="<?php echo get_permalink($recent["ID"])?>" class="at-masthead-post-title masthead-block-article-content-title post-title-text text-heading"> <?=$recent['post_title'] ?></a>

                
                <time class="at-masthead-timestamp masthead-block-article-content-author time-stamp-text text-timestamp"> <?=time_ago()?></time>
                <span class="at-masthead-author masthead-block-article-content-author author-name-text  text-author">  <?php
                    echo the_author_meta('display_name', $recent['post_author']);
                ?>
                </span>
            </div>
        </div>
    </div>

          <?php } ?>
                <div class="hidden-xs hidden-sm col-md-3 flex-column">
                    
<div class="at-newsletter-block newsletter-social-block">
    <div class="at-newsletter-heading newsletter-stats">
        <div class="text-subheading">Want to learn tons of ways to make extra money?</div>
        <p>Sign up for free updates...</p>
        <form id="sidebarForm" accept-charset="utf-8" action="#" method="post" class="email-form iterable-form" form-location="homepage-header">
    <input type="hidden" name="name">
    <label for="76dfd00113afd35df8fbb0bff11ef6f6" style="position: absolute;z-index: 1;pointer-events: none;color: transparent;">Email address</label>
     <?php echo do_shortcode('[contact-form-7 id="12264" title="Sign Up"]');?>
</form>
        <p class="email-privacy-policy-blurb-purple"><a href="<?php bloginfo('home')?>/privacy-policy">Privacy Policy</a></p>
    </div>
    <div class="social-stats">
        <div class="active-subscribers">
            <span class="at-newsletter-total-Subscribers total_subscribers text-bignumber">8,221,701</span>
            <span class="totalSubscribersText">Active Cheap The Change</span>
        </div>
        <ul>
            <li class="at-newsletter-FB social-site">
                <a target="_blank" href="#">
                    <img class="lazyload" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/facebook.svg" alt="Facebook Likes"/>
                    <span class="at-newsletter-fb-count facebook_like_count">6,892,523</span>
                </a>
            </li>
            <li class="at-newsletter-pinterest social-site">
                <a target="_blank" href="#">
                    <img class="lazyload" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/pinterest.svg"alt="Pinterest"/>
                    <span class="at-newsletter-pinterest-count pinterest_follower_count">129,178</span>
                </a>
            </li>
            <li class="at-newsletter-email social-site">
                <a target="_blank" href="#">
                    <img class="lazyload" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/email.svg"  alt="E-mail subscribers"/>
                    
                    <span class="at-newsletter-email-count email_subscriber_count">1,510,190</span>
                </a>
            </li>
        </ul>
    </div>
</div>
                </div>
            </div>
        </div>

        <div class="row trending-block flex-row">
            <div class="container flex-container">

                <?php

                    echo do_shortcode("[top_three_related_post title='Trending Posts' category='trending-posts']")
                ?>

                <div class="mobile-newsletter col-xs-12 hidden-md hidden-lg">
                    <div class="container flex-container">
			            
						<div class="newsletter-social-block">
							<div class="newsletter-stats">
								<div class="text-subheading">Want to learn tons of ways to make extra money?</div>
								<p>Sign up for free updates...</p>
								<form accept-charset="utf-8" action="#" method="post" class="email-form iterable-form" form-location="newsletter-social-block">
									<input type="hidden" name="name">
									<label for="emailaddresshomebottom" class="sr-only">Email address</label>
									<input id="emailaddresshomebottom" type="email" name="email" class="form-control" placeholder="enter email address"/>
									<span class="input-group-btn">
										<input type="submit" value="Sign Up" class="at-submit-button btn btn-primary">
									</span>
									<p class="email-privacy-policy-blurb-purple">
                                        <a href="<?php bloginfo('home')?>/privacy-policy">
                                            Privacy Policy</a>
                                    </p>
								</form>
							</div>
							<div class="social-stats">
								<div class="active-subscribers">
									<span class="totalSubscribers text-bignumber">8,221,701</span>
									<span class="totalSubscribersText">Active penny hoarders</span>
								</div>
							</div>
						</div>
					</div>
                </div>

                <?php

                echo do_shortcode("[top_three_related_post title='Editors Picks' category='editor-picks' ]")
                ?>

            </div>

         </div>

        <div class="row main-vertical-block flex-column flex-row">
            <div class="container flex-container">

        <?php echo do_shortcode("[middle_three_cat_post title='Make Money' category='make-money']"); ?>
        <?php echo do_shortcode("[middle_three_cat_post title='Smart Money' category='smart-money']"); ?>
        <?php echo do_shortcode("[middle_three_cat_post title='Food' category='food']"); ?>
        <?php echo do_shortcode("[last_three_cat_post title='Life' category='life']"); ?>
        <?php echo do_shortcode("[last_three_cat_post title='Deals' category='deals']"); ?>
        <?php echo do_shortcode("[last_three_cat_post title='Guides' category='guides']"); ?>

            </div>
        </div>


        <?php
//            if (is_active_sidebar( 'top-footer-panel' ) ) :
                dynamic_sidebar('top-footer-panel');
//            endif
        ?>

        <div class="row photo-essay flex-row">
            <div class="single-posts-recommended">


                <?php echo do_shortcode("[recommended-content]"); ?>

            </div>
        </div>

        <div class="row">
            <div class="container flex-container">
                <hr class="mobile-spacer hidden-sm hidden-md hidden-lg">
            </div>
        </div>



         <?php
                            $queryObject = new  Wp_Query( array(
                                'posts_per_page'=> 20,
                                'ignore_sticky_posts' => 0,
                                'category__not_in' => array( 86, 66, 67, 1 ),
                                'post__in'  => get_option( 'sticky_posts' ),
                            ));

                            $i = $j = 1;

                            if ( $queryObject->have_posts() ) :
                            while ( $queryObject->have_posts() ) :
                            $queryObject->the_post();

                            if($i==1){

          ?>


        <div class="row posts-block flex-row <?php if($j==1) echo 'posts-block-1 editable'?>"  <?php if($j>1) {?>style="display: none;" <?php } ?> data-placement-id="posts-block-<?=$j?>">
            <div class="container flex-container">
<?php $j++; } ?>

                 <div class="at-4-post-sticky-column col-xs-12 col-sm-6 col-md-3 flex-column" data-id="<?php the_ID();?>">
        
					<div class="at-4-post-sticky-image panel-heading col-xs-6 col-sm-12">
						<a href="<?php echo esc_url(get_permalink());?>" class=" has-youtube-video has-youtube-video-masthead" data-yt-video-id="ikN9Yje277g">
                            <div class="video-responsive" data-video-id="video-ikN9Yje277g">
								<div data-iframe-id="iframe-video-ikN9Yje277g"></div>
							</div>
							<span class="youtube-play-icon"></span>
							
							<div class="four_by_six">
                                <?php
                                if(has_post_thumbnail()):
                                    the_post_thumbnail('post');
                                endif
                                ?>
							</div>        
						</a>
					</div>
					<div class=" col-xs-6 col-sm-12 panel-body text-center ">
            
						<div class="posts-block-container">
							<span class="at-4-post-sticky-category-post-1 posts-block-category-title category-title hidden-xs">
                                <?php
                                    $category  = get_the_category();
                                    $cat_name = $category[count($category)-1]->name;
                                echo $cat_name;
                                ?>
                            </span>

							<time class="at-4-post-non-sticky-timestamp-post-1 posts-block-datetime time-stamp-text text-timestamp hidden-xs"><?=time_ago()?></time>

							<span class="at-4-post-sticky-title-post-1 posts-block-category-title hidden-sm hidden-md hidden-lg"><?php  echo $cat_name; ?> </span>

							<a href="<?php echo esc_url(get_permalink());?>" class="posts-block-post-title post-title-text text-subheading">
                                <?php the_title();?>
							</a>

                            <time class="at-4-post-sticky-timestamp-video-post-1 posts-block-datetime time-stamp-text text-timestamp hidden-sm hidden-md hidden-lg"><?=time_ago()?></time>
							
							<span class="at-4-post-sticky-author-post-1 posts-block-author author-name-text text-author"> <?php
                                    echo the_author_meta('display_name', $recent['post_author']);
                                ?>
                            </span>
						</div>
					</div>
				</div>

                <?php
                if($i==4){
                    $i=0;
                    echo "</div>";
                    echo "</div>";
                }
                $i++;
                ?>
                                <div class="row posts-block hidden flex-row">
                                    <div class="container flex-container">
                                    </div>
                                </div>

                 <?php


                            endwhile;
                            endif;
         wp_reset_postdata();
         ?>


        <div class="row load-more-button">
            <div class="container flex-container text-center">
                <a href="#" class="at-load-more-button btn btn-default btn-load-more">
                    Load More
                </a>
            </div>
        </div>

        <?php

//         echo do_shortcode("[footer_image_navigation]");

         get_footer();

         ?>