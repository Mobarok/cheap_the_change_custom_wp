<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/18/2018
 * Time: 3:58 PM

 * Template Name: Press

 */
get_header();
?>

<div class="page-container default-page press-page">
    <div class="press-hero">
        <h1>Press Room</h1>
    </div>
    <div class="press-body-copy text-center">
        <div class="container">
            <p class="press-intro">
                Cheap The Chnage is a personal finance website that reaches millions of readers each month.
                Our mission is to put more money in people’s pockets; we do this by sharing job opportunities, personal success stories,
                practical tips and more. Whether you’re looking for information on weekend side hustles, ways to shave dollars off your
                grocery budget, or the differences between an IRA or 401(k), we’re here to help you make and save more money.
            <hr>
            <h3 class="press-contact">
                Press Contact
            </h3>
            <p class="press-contact-info press-contact-name">
                Seans
            </p>
            <p class="press-contact-info press-contact-phone">
               111-111-1111
            </p>
        </div>
    </div>
    <div class="press-as-seen-on">
        <div class="container">
            <h3 class="press-heading">As seen on</h3>
            <hr class="press-spacer">
            <ul id="as-seen-on-slider">
                <li>
                    <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/yahoo-finance-logo-vector.png" alt="Yahoo! Finance" class="img-responsive">
                </li>
                <li>
                    <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/world-news-tonight.png" alt="World News Tonight" class="img-responsive">
                </li>
                <li>
                    <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/the_oprah_magazine_logo.png" alt="The Oprah Magazine" class="img-responsive">
                </li>
                <li>
                    <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/family-circle-logo-copy.png" alt="Family Circle" class="img-responsive">
                </li>
                <li>
                    <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/Inc-Magazine-Logo.png" alt="Inc Magazine" class="img-responsive">
                </li>
                <li>
                    <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/Forbes_logo.png" alt="Forbes" class="img-responsive">
                </li>
                <li>
                    <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/The_New_York_Times_logo.png" alt="The New York Times" class="img-responsive">
                </li>
                <li>
                    <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/FL-business-observer-logo.png" alt="Business Observer" class="img-responsive">
                </li>
            </ul>
        </div>
    </div>

    <div class="row press-news-and-media">
        <div class="container">
            <div class="col-md-6 press-media">
                <h2>In the Media</h2>
                <div id="press-media-carousel" class="press-news-media-carousel">

                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2018</p>
                            <p class="press-carousel-month">Oct</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">The Morning Blend</span>
                                <span class="label label-default">Broadcast</span>
                            </h4>
                            <p class="press-carousel-text">
                                The Morning Blend features Cheap The Chnage&#8217;s personal finance advice column, Dear Penny
                            </p>
                            <a href="https://www.abcactionnews.com/morning-blend/the-penny-hoarder-finance-advice" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2018</p>
                            <p class="press-carousel-month">Sep</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">News Channel 5</span>
                                <span class="label label-default">Broadcast</span>
                            </h4>
                            <p class="press-carousel-text">
                                5 Side Gigs To Make Extra Cash
                            </p>
                            <a href="https://www.newschannel5.com/talk-of-the-town/make-extra-cash-with-the-penny-hoarder" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2018</p>
                            <p class="press-carousel-month">Sep</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Bradenton Herald </span>
                                <span class="label label-default">Online </span>
                            </h4>
                            <p class="press-carousel-text">
                                This Lakewood Ranch High grad dropped out of college. He now runs a $35 million business
                            </p>
                            <a href="https://www.bradenton.com/news/business/article218117620.html" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2018</p>
                            <p class="press-carousel-month">Aug</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Fox 25</span>
                                <span class="label label-default">Broadcast</span>
                            </h4>
                            <p class="press-carousel-text">
                                Consumer Watch: Three creative budget plans to help you save money
                            </p>
                            <a href="https://okcfox.com/news/consumer-watch/conusmer-watch-three-creative-budget-plans-to-help-you-save-money" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2018</p>
                            <p class="press-carousel-month">Aug</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Glassdoor</span>
                                <span class="label label-default">Online</span>
                            </h4>
                            <p class="press-carousel-text">
                                20 Coolest Offices in America
                            </p>
                            <a href="https://www.glassdoor.com/blog/20-coolest-offices-in-america/" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2018</p>
                            <p class="press-carousel-month">Aug</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">MSN Money</span>
                                <span class="label label-default">Online</span>
                            </h4>
                            <p class="press-carousel-text">
                                8 money moves for when you expect one baby &#8230; but find out you’re having two (or three)
                            </p>
                            <a href="https://www.msn.com/en-us/money/personalfinance/8-money-moves-for-when-you-expect-one-baby-but-find-out-you’re-having-two-or-three/ss-BBMh772" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2018</p>
                            <p class="press-carousel-month">Aug</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Fox-19</span>
                                <span class="label label-default">Broadcast</span>
                            </h4>
                            <p class="press-carousel-text">
                                Debunking 9 credit score myths
                            </p>
                            <a href="http://www.fox19.com/story/38923122/the-penny-hoarder-debunking-9-credit-score-myths" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>

                </div>
                <div id="press-media-slick-nav" class="press-slick-nav"></div>
            </div>
            <div class="col-md-6 press-recent-news">
                <h2>Recent News</h2>
                <div id="press-recent-posts-carousel" class="press-news-media-carousel">

                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2018</p>
                            <p class="press-carousel-month">Oct</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Cheap The Chnage’s Survey Finds 1 Out of 4 Americans Have Gone Into Debt to Afford Child Care</span>
                                <span class="label label-default">Survey</span>
                            </h4>
                            <p class="press-carousel-text">
                                More than 25 percent of Americans have gone into debt to afford child care, according to a new survey by The Penny Hoard...                    </p>
                            <a href="https://www.thepennyhoarder.com/child-care-survey/" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2018</p>
                            <p class="press-carousel-month">Sep</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Cheap The Chnage Ranks No. 11 Fastest-Growing Company On Florida Fast 100</span>
                                <span class="label label-default">Press Release</span>
                            </h4>
                            <p class="press-carousel-text">
                                Cheap The Chnage was named one of this year&#8217;s fastest-growing private companies in Florida.
                            </p>
                            <a href="https://www.thepennyhoarder.com/penny-hoarder-ranks-no-11-fastest-growing-company-florida-fast-100/" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2018</p>
                            <p class="press-carousel-month">Jul</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Tampa Bay Times partners with Cheap The Chnage to publish personal finance advice column Dear Penny</span>
                                <span class="label label-default">Press Release</span>
                            </h4>
                            <p class="press-carousel-text">
                                Starting July 29, the Tampa Bay Times will publish Cheap The Chnage’s Dear Penny personal finance advice column.
                            </p>
                            <a href="https://www.thepennyhoarder.com/tampa-bay-times-partners-penny-hoarder-publish-personal-finance-advice-column-dear-penny/" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2017</p>
                            <p class="press-carousel-month">Nov</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Cheap The Chnage Expands In Pinellas, Expects To Create 165 New Jobs</span>
                                <span class="label label-default">NEWS </span>
                            </h4>
                            <p class="press-carousel-text">
                                Today, Kyle Taylor, CEO of Cheap The Chnage, owned by Taylor Media, Inc. Magazine&#8217;s top-ranked private media comp...                    </p>
                            <a href="https://www.pced.org/page/tph" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2017</p>
                            <p class="press-carousel-month">Nov</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Taylor Media</span>
                                <span class="label label-default">Corporate Blog</span>
                            </h4>
                            <p class="press-carousel-text">
                                We&#8217;re Gonna&#8217; Need More Desks
                            </p>
                            <a href="https://www.taylormedia.com/blog/were-gonna-need-more-desks" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2017</p>
                            <p class="press-carousel-month">Feb</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Small-Sized Companies: The Best Company Cultures in 2017</span>
                                <span class="label label-default">News</span>
                            </h4>
                            <p class="press-carousel-text">
                                Entrepreneur magazine&#8217;s Top Company Culture list has placed Cheap The Chnage as the #2 in the small-sized company...                    </p>
                            <a href="https://www.entrepreneur.com/article/289219" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2016</p>
                            <p class="press-carousel-month">Oct</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Taylor Media Ends Q3 with Major National Award, Valuable Hires and Larger Headquarters</span>
                                <span class="label label-default">Press Release</span>
                            </h4>
                            <p class="press-carousel-text">
                                Cheap The Chnage brings on seasoned journalism pro John Schlander as Managing Editor
                            </p>
                            <a href="https://www.thepennyhoarder.com/taylor-media-ends-q3-with-major-national-award-valuable-hires-and-larger-headquarters/" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2016</p>
                            <p class="press-carousel-month">Aug</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Inc. Magazine Unveils 35th Annual List of America’s Fastest-Growing Private Companies—the Inc. 5000</span>
                                <span class="label label-default">Press Release</span>
                            </h4>
                            <p class="press-carousel-text">
                                Cheap The Chnage Ranks No. 32 on the 2016 List with a Three-Year Growth of 6,934%
                            </p>
                            <a href="https://www.thepennyhoarder.com/press/Cheap The Chnage Ranks No. 32 on the 2016 List with a Three-Year Growth of 6,934%" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2016</p>
                            <p class="press-carousel-month">Feb</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Taylor Media’s 2015 Growth Makes It One of the Fastest-Growing Digital Media Companies</span>
                                <span class="label label-default">Press Release</span>
                            </h4>
                            <p class="press-carousel-text">
                                Taylor Media, which runs personal finance blog Cheap The Chnage, today released growth figures from 2015 that show it�...                    </p>
                            <a href="https://www.thepennyhoarder.com/taylor-medias-2015-growth/" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2015</p>
                            <p class="press-carousel-month">Nov</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Taylor Media Opens Office in St. Petersburg; Hosts Launch Party for Tampa Bay Business Community</span>
                                <span class="label label-default">Press Release</span>
                            </h4>
                            <p class="press-carousel-text">
                                After operating remotely for five years, Cheap The Chnage has opened an office in downtown St. Petersburg on the 600 bl...                    </p>
                            <a href="https://www.thepennyhoarder.com/taylor-media-opens-office-in-st-petersburg-hosts-launch-party-for-tampa-bay-business-community/" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>


                    <div class="row press-carousel-item">
                        <div class="col-xs-3 col-sm-2 press-carousel-date">
                            <p class="press-carousel-year">2015</p>
                            <p class="press-carousel-month">Jul</p>
                        </div>
                        <div class="col-xs-9 col-sm-10 press-carousel-content">
                            <h4 class="press-carousel-heading">
                                <span class="heading-text">Taylor Media Announces Acqui-Hire of Content Marketing Company Socialexis</span>
                                <span class="label label-default">Press Release</span>
                            </h4>
                            <p class="press-carousel-text">
                                Taylor Media will bring in-house the founder of Socialexis, Alexis Grant, to oversee its growing editorial team as Execu...                    </p>
                            <a href="https://www.thepennyhoarder.com/taylor-media-announces-acqui-hire/" class="press-carousel-cta" target="_blank">
                                Read more                    </a>
                        </div>
                    </div>

                </div>
                <div id="press-recent-posts-slick-nav" class="press-slick-nav"></div>
            </div>
        </div>
    </div>

    <div class="press-media-assets">
        <div class="container">
            <h2>Media Assets</h2>

            <div class="press-logos">
                <h3>Logos</h3>
                <hr class="press-logos-spacer">
                <div class="row press-logos-container">
                    <div class="container">
                        <div class="col-xs-6 col-sm-3 press-logo">
                            <a class="new-tab-bg-color" href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/logos/TPH_Stacked_White.png">
                                <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/TPH_Stacked_White_bg.png" alt="TPH Stacked White" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 press-logo">
                            <a href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/logos/TPH_Stacked.png">
                                <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/TPH_Stacked_bg.png" alt="TPH Stacked" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 press-logo">
                            <a class="new-tab-bg-color" href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/logos/TPH_Logo_Horizontal_v2_white.png">
                                <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/TPH_Logo_Horizontal_v2_White.png" alt="TPH Horizontal Logo (White)" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 press-logo">
                            <a href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/logos/TPH_Logo_Horizontal_v2.png">
                                <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/TPH_Logo_Horizontal_v2.png" alt="TPH Horizontal Logo" class="img-responsive">
                            </a>
                        </div>
                    </div>
                </div>
                <p class="press-logos-text">
                    Cheap The Chnage logos should not be altered in any way. If you are a member of the news media in need of
                    additional information or support, please contact our communications team using the form below.
                </p>
            </div>

            <div class="press-photos">
                <h3>Press Photos</h3>
                <hr class="press-logos-spacer">
                <ul id="press-photos-carousel">
                    <li>
                        <a href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/presspage_image01.jpg" target="_blank" download>
                            <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/press-1.png" alt="Press Photo 1" class="img-responsive">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/presspage_image02.jpg" target="_blank" download>
                            <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/press-2.png" alt="Press Photo 2" class="img-responsive">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/presspage_image03.jpg" target="_blank" download>
                            <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/press-3.png" alt="Press Photo 3" class="img-responsive">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/presspage_image04.jpg" target="_blank" download>
                            <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/press-4.png" alt="Press Photo 4" class="img-responsive">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/presspage_image05.jpg" target="_blank" download>
                            <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/press-5.png" alt="Employees 1" class="img-responsive">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/presspage_image06.jpg" target="_blank" download>
                            <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/press-6.png" alt="Employees 2" class="img-responsive">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/presspage_image07.jpg" target="_blank" download>
                            <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/press-7.png" alt="Employees 3" class="img-responsive">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/presspage_image08.jpg" target="_blank" download>
                            <img src="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/press-8.png" alt="Employees 4" class="img-responsive">
                        </a>
                    </li>
                </ul>
                <div id="press-photos-slick-nav" class="press-slick-nav"></div>
            </div>

        </div>
    </div>

    <div class="press-contact-form">
        <div class="container">
            <h3 class="press-heading">Contact Us</h3>
            <hr class="press-spacer">

            <div id="press-form-success" style="display: none;">
                <h4>Success</h4>
                <p>
                    Thanks! Placeholder copy here lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Maecenas eleifend nulla non urna tristique mattis.
                </p>
            </div>

            <form id="press-contact-form" method="post" action="https://www.thepennyhoarder.com/wp-content/themes/pennyhoarder/scripts/press_form_send_email.php">
                <input type="hidden" id="submit_mail" name="submit_mail" value="e95d3b2a88"/><input type="hidden" name="_wp_http_referer" value="/press/?aff_sub2=homepage"/>                    <div class="form-group">
                    <label for="ap_inputname">Name</label>
                    <input type="text" class="form-control" id="ap_inputname" name="ap_inputname">
                </div>
                <div class="form-group">
                    <label for="ap_inputemail">Email</label>
                    <input type="email" class="form-control" id="ap_inputemail" name="ap_inputemail">
                </div>
                <div class="form-group">
                    <label for="ap_inputmessage">Message</label>
                    <textarea class="form-control" rows="10" name="ap_inputmessage" id="ap_inputmessage"></textarea>
                </div>
                <div class="form-group captcha-holder">
                    <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                    <div class="g-recaptcha" data-sitekey="6LdyiToUAAAAAD_d5sLmxRne3kVtjFRGBI2YZimC" data-callback="removeGerror"></div>
                </div>
                <div class="col-xs-12 form-group cp-submit">
                    <button type="submit" class="btn btn-block btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
    get_footer();
?>



