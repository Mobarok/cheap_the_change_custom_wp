<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/18/2018
 * Time: 4:07 PM
 */


    // Enable shortcodes in text widgets
//    add_filter('widget_text', 'do_shortcode');

    add_shortcode('top_three_related_post', function ($attr, $title) {
        ob_start();
        extract(shortcode_atts(array(
            'title' => 'Categories Title',
            'category' => ' Uncategorized',
            'bottom_post_count' => 3
        ), $attr));



    ?>
        <div class="col-xs-12 col-md-6 trending-block-trending">
            <div class="panel-heading">
            <span class="at-trending-section-title trending-block-title">
                <?php echo $title; ?>
            </span>
                <hr class="trending-spacer">
            </div>
            <?php
                $queryObject = new  Wp_Query( array(
                    'showposts' => $bottom_post_count,
                    'post_type' => array('post'),
                    'category_name' => $category,
                    'order'            => 'DESC'
                ));
            ?>
            <div class="panel-body">
                <?php
                    if ( $queryObject->have_posts() ) :
                        while ( $queryObject->have_posts() ) :
                            $queryObject->the_post();
                ?>

                <div class="at-mostpopular-block-1  trending-block-trending-main trending-block-trending-0 vcenter">
                    <a href="<?php echo esc_url( get_permalink() )?>" class="at-mostpopular-image col-xs-12 col-md-4">
                        <div class="four_by_six">
                            <?php if ( has_post_thumbnail() ) {
                                the_post_thumbnail( 'post');
                            } ?>
                        </div>
                    </a>

                    <div class="col-xs-12 col-md-8 trending-block-container">


                        <a href="<?php echo esc_url( get_permalink() )?>" class="at-mostpopular-title-link trending-block-post-title post-title-text">
                           <?php the_title() ?>              </a>

                        <time class="at-mostpopular-datetime trending-block-post-datetime time-stamp-text">
                            <?php echo  time_ago(); ?>
                        </time>

                    </div>
                </div>

               <?php

                        endwhile;
                    endif;
                    wp_reset_postdata();
                ?>

            </div>
        </div>


    <?php

        return ob_get_clean();
    });



// For Middle Three Category Post
    add_shortcode('middle_three_cat_post', function ($attr, $title) {
        ob_start();
        extract(shortcode_atts(array(
            'title' => 'Categories Title',
            'category' => ' Uncategorized',
            'bottom_post_count' => 3
        ), $attr));



    ?>

    <div class="editable col-xs-12 col-sm-6 col-md-4
        <?php if($title=='Make Money')echo 'main-vertical-block-1'; elseif($title=='Smart Money') echo 'main-vertical-block-2'; else echo 'main-vertical-block-3';?> flex-column clearfix ">
        <div class="panel-heading">
            <a href="<?php if($category !="guides") echo bloginfo('home')."/".$category; ?>" class="main-vertical-block-title verticals-heading">
                <?php echo $title ?>
            </a>
        </div>
        <?php
            $queryObject = new  Wp_Query( array(
                'showposts' => $bottom_post_count,
                'post_type' => array('post'),
                'category_name' => $category,
                'order'            => 'DESC'
            ));


        ?>
        <div class="panel-body col-sm-12">
            <?php
                // The Loop
                $i = 1;
                if ( $queryObject->have_posts() ) :
                while ( $queryObject->have_posts() ) :
                $queryObject->the_post();
            ?>
            <div class="col-xs-12 <?php if($i==1)echo "col-xs-12"; else echo "col-xs-6 ";?> col-sm-12 ">
                <a href="<?php echo esc_url( get_permalink() )?>" class="col-sm-12 <?php if($i!=1)echo "col-sm-3"; ?> image-holder">
                    <div class="four_by_six">
                        <?php if ( has_post_thumbnail() ) {
                            the_post_thumbnail( 'post');
                        } ?>
                    </div>
                </a>
                <div class="col-xs-12  <?php if($i==1)echo "col-sm-12 text-center main-vertical-featured-post"; else echo "col-sm-9 m text-left";?>  main-vertical-block-featured-container ">
                    <a href="<?php echo esc_url( get_permalink() )?>" class="main-vertical-block-featured-post-title post-title-text text-subheading ">
                        <?php the_title()?>
                    </a>

                    <p class="<?php if($i!=1)echo 'hidden-xs hidden-sm hidden-md hidden-lg'; ?>">
                        <time class="main-vertical-block-featured-post-datetime time-stamp-text text-timestamp"><?php echo  time_ago(); ?> </time>
                        <span class="main-vertical-block-featured-post-author author-name-text text-author"><?php the_author()?></span>
                    </p>

                </div>

            </div>
                    <hr class="main-vertical-block-separator hidden-sm">

            <?php
                      $i++;
                endwhile;
                endif;
                wp_reset_postdata();
            ?>

        </div>
    </div>

<?php
        return ob_get_clean();
    });


        add_shortcode('last_three_cat_post', function ($attr) {
            ob_start();
            extract(shortcode_atts(array(
            'title' => 'Categories Title',
            'category' => ' Uncategorized',
            'bottom_post_count' => 3
            ), $attr));



        ?>

        <div class="editable col-xs-12 col-sm-6 col-md-12 main-vertical-block-4 flex-column clearfix ">
            <div class="panel-heading">
                <?php if($category !="guides") { ?> <a href="<?php echo bloginfo('home') . "/" . $category; ?>"
                                                       class="main-vertical-block-title verticals-heading">
                    <?php echo $title ?>
                </a>
                    <?php
                } else {
                ?>
                <a  class="main-vertical-block-title verticals-heading">
                    <?php echo $title ?>
                </a>
                <?php } ?>
            </div>
            <?php
            $queryObject = new  Wp_Query( array(
                'showposts' => $bottom_post_count,
                'post_type' => array('post'),
                'category_name' => $category,
                'order'            => 'DESC'
            ));


            ?>
            <div class="panel-body col-sm-12">
                <?php
                    // The Loop
                    $i = 1;
                    if ( $queryObject->have_posts() ) :

                        while ( $queryObject->have_posts() ) :
                            $queryObject->the_post();

                        if($i<3):
                    ?>
                        <div class="col-md-6 main-vertical-special-wrapper">
                    <?php
                        endif;

                        if($i==1):
                    ?>

                            <div class="col-xs-12" >
                        <a href="<?php echo esc_url( get_permalink() )?>" class="col-xs-5 col-sm-6 image-holder">

                            <div class="four_by_six">
                                <?php if ( has_post_thumbnail() ) {
                                    the_post_thumbnail( 'post');
                                } ?>
                            </div>
                        </a>
                        <div class="col-xs-7 col-sm-6 main-vertical-featured-post main-vertical-block-featured-container text-center">

                            <a href="<?php echo esc_url( get_permalink() )?>" class="main-vertical-block-featured-post-title post-title-text text-subheading">
                                <?php the_title()?>
                            </a>

                            <p class="hidden-xs hidden-sm">
                                <time class="main-vertical-block-featured-post-datetime time-stamp-text text-timestamp">
                                    <?php echo time_ago(); ?>
                                </time>
                                <span class="main-vertical-block-featured-post-author author-name-text text-author">
                                    <?php the_author() ?>
                                </span>
                            </p>

                        </div>
                        </div>

                            <?php
                                else:
                            ?>

                            <div class="col-xs-12" >
                                        <a href="<?php echo esc_url( get_permalink() )?>" class="col-xs-3 col-sm-3 image-holder">
                                            <div class="four_by_six">
                                                <?php if ( has_post_thumbnail() ) {
                                                    the_post_thumbnail( 'post');
                                                } ?>
                                            </div>
                                        </a>
                                        <div class="col-xs-9 col-sm-9 main-vertical-block-featured-container text-left">
                                            <a href="<?php echo esc_url( get_permalink() )?>" class="main-vertical-block-featured-post-title post-title-text text-subheading">
                                               <?php the_title() ?>
                                            </a>

                                            <p class="hidden-xs hidden-sm hidden-md hidden-lg">
                                                <time class="main-vertical-block-featured-post-datetime time-stamp-text text-timestamp"><?php echo time_ago();  ?></time>
                                                <span class="main-vertical-block-featured-post-author author-name-text text-author"><?php the_author() ?></span>
                                            </p>

                                        </div>

                                    <hr class="main-vertical-block-separator hidden-sm">
                            </div>
                            <?php
                                endif;
                                if($i==1||$i==3) :
                            ?>

                                </div>
                            <?php
                                    endif
                            ?>


                <hr class="main-vertical-block-separator hidden-sm">

                <?php

                    $i++;
                    endwhile;
                    endif;
                    wp_reset_postdata();
                ?>

            </div>
        </div>


<?php
        return ob_get_clean();
    });

    function footer_image_navigation(){
        ?>

        <div class="row verticals-bottom-block flex-row">
            <div class="container flex-container">

                <div class="at-make-money-bottom-vert lazy-load-background col-xs-12 col-sm-2 verticals-bottom-vertical vertical-1" data-interaction="single-posts-below-content-verticals-make-money">
                    <a href="<?php bloginfo('home')?>/make-money">
                        <div class="verticals-bottom-block-container">
                            <div class="verticals-bottom-block-desc">
                                <div class="verticals-bottom-block-desc-title text-center">Make Money</div>
                            </div>
                        </div>
                        <div class="verticals-bottom backstretch"></div>
                    </a>
                </div>

                <div class="at-deals-bottom-vert lazy-load-background col-xs-12 col-sm-2 verticals-bottom-vertical vertical-2"  data-interaction="single-posts-below-content-verticals-deals">
                    <a href="<?php bloginfo('home')?>/deals">
                        <div class="verticals-bottom-block-container">
                            <div class="verticals-bottom-block-desc">
                                <div class="verticals-bottom-block-desc-title">Deals</div>
                            </div>
                        </div>
                        <div class="verticals-bottom backstretch"></div>
                    </a>
                </div>

                <div class="at-food-bottom-vert lazy-load-background col-xs-12 col-sm-2 verticals-bottom-vertical vertical-3"  data-interaction="single-posts-below-content-verticals-food">
                    <a href="<?php bloginfo('home')?>/food">
                        <div class="verticals-bottom-block-container">
                            <div class="verticals-bottom-block-desc">
                                <div class="verticals-bottom-block-desc-title">Food</div>
                            </div>
                        </div>
                        <div class="verticals-bottom backstretch"></div>
                    </a>
                </div>

                <div class="at-smart-money-bottom-vert lazy-load-background col-xs-12 col-sm-2 verticals-bottom-vertical vertical-4"  data-interaction="single-posts-below-content-verticals-smart-money">
                    <a href="<?php bloginfo('home')?>/smart-money">
                        <div class="verticals-bottom-block-container">
                            <div class="verticals-bottom-block-desc">
                                <div class="verticals-bottom-block-desc-title">Smart Money</div>
                            </div>
                        </div>
                        <div class="verticals-bottom backstretch"></div>
                    </a>
                </div>

                <div class="at-life-bottom-vert lazy-load-background col-xs-12 col-sm-2 verticals-bottom-vertical vertical-5" data-interaction="single-posts-below-content-verticals-life">
                    <a href="<?php bloginfo('home')?>/life">
                        <div class="verticals-bottom-block-container">
                            <div class="verticals-bottom-block-desc">
                                <div class="verticals-bottom-block-desc-title">Life</div>
                            </div>
                        </div>
                        <div class="verticals-bottom backstretch"></div>
                    </a>
                </div>
            </div>
        </div>

<?php

    }

    add_shortcode('footer_image_navigation','footer_image_navigation');

    function footer_sign_up(){
        ?>

        <div class="row newsletter-wrap flex-row">
            <div class="container flex-container">
                <div class="col-xs-12 newsletter-form">
                    <div class="col-xs-12 col-sm-6 newsletter-form-title">
                        <div class="text-subheading">Want to learn tons of ways to make extra money?</div>
                        <p>Sign up for free updates...</p>
                        <p class="email-privacy-policy-blurb-white">
                            <a href="<?php echo bloginfo('home');?>/privacy-policy">Privacy Policy</a>
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 newsletter-form-form">
                        <form id="sidebarForm" accept-charset="utf-8" action="#" method="post" class="email-form iterable-form" form-location="vertical">
                            <input type="hidden" name="name">
                            <label for="a6ca3063566fc2ea6731f4209ec89d60" style="position: absolute;z-index: 1;pointer-events: none;color: transparent;">Email address</label>
<!--                            <input id="a6ca3063566fc2ea6731f4209ec89d60" type="email" name="email" class="at-newsletter-email-field form-control" placeholder="enter email address" required />-->
<!--                            <span class="input-group-btn">-->
<!--        <input type="submit" value="Sign Up" class="at-submit-button btn btn-primary"/>-->
<!--    </span>-->
                            <?php echo do_shortcode('[contact-form-7 id="12264" title="Sign Up"]');?>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    <?php

    }

    add_shortcode('footer_sign_up','footer_sign_up');


    function recommended_content($attr, $title){

        ob_start();
        extract(shortcode_atts(array(
            'post_number' => 6,
            'title' => 'Recommended Content',
        ), $attr));

        ?>

                <div class="container">

                    <div class="col-xs-12 post-recommended-title text-center">
                        <h3><?=$title;?></h3>
                    </div>

                    <div class="col-xs-12 post-recommended-inner text-center">
                        <?php
                        $queryObject = new  Wp_Query( array(
                            'showposts' => $post_number,
                            'meta_key' => 'post_views_count',
                            'orderby' => 'meta_value_num',
                            'post_status' => 'publish',
                            'date_query'    => array(
                                'column'  => 'post_date',
                                'after'   => '- 60 days'
                            )
                        ));

                        if ( $queryObject->have_posts() ) :
                            while ( $queryObject->have_posts() ) :
                                $queryObject->the_post();

                        ?>
                        <div class="col-xs-12 col-sm-4 col-md-2 text-center block-b at-blockb2">
                            <div class="col-xs-12 category-subcategory-post-image">
                                <a href="<?php echo esc_url(get_permalink());?>" class=" has-youtube-video-home-posts">
                                    <div class="four_by_six">
                                        <?php
                                            if(has_post_thumbnail()):
                                                the_post_thumbnail('post');
                                            endif
                                        ?>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 category-subcategory-post-content">
                                <a href="<?php echo esc_url(get_permalink());?>" class="photo-essay-article-content-title">
                                    <?php the_title();?>
                                </a>
                            </div>
                        </div>

                        <?php

                            endwhile;
                            endif;
                            wp_reset_postdata();
                        ?>
                    </div>

                </div>

        <?php
        return ob_get_clean();
    }

    add_shortcode('recommended-content','recommended_content');



function single_top_post_by_category($attr) {

    ob_start();
    extract(shortcode_atts(array(
        'category' => 'Category Title',
    ), $attr));

    $post = array(
            'numberposts' => '1',
            'category_name' => $category,
    );
    $recent_post = wp_get_recent_posts($post);
    ?>

    <div class="row photo-essay flex-row">
        <div class="container flex-container">
            <div class="col-sm-12 category-masthead-block-article photo-essay-block editable">
                <?php
                    foreach ($recent_post as $recent):
                ?>
                        <?php
                            if(has_post_thumbnail($recent['ID'])){
                                $img = get_the_post_thumbnail($recent['ID'],'thumbnail');
                            }
                        ?>
                <div class="col-xs-12 col-sm-9 photo-essay-article-image">
                    <a href="<?php echo get_permalink($recent["ID"])?>" class="">
                        <div class="four_by_six">
                            <?=$img?>
                        </div>
                    </a>
                </div>
                <?php
                    $category  = get_the_category($recent['ID']);
                ?>
                <div class="col-xs-12 col-sm-3 photo-essay-article-content vcenter">
                    <div class="photo-essay-article-content-container">
                        <a href="<?=$category[count($category)-1]->slug?>"
                           class="masthead-block-article-content-category category-title category-heading verticals-heading ">      <?php
                                echo $category[count($category)-1]->name;
                            ?>
                        </a>
                        <a href="<?php echo get_permalink($recent["ID"])?>" class="photo-essay-article-content-title">
                            <?=$recent['post_title'] ?>
                        </a>
                        <time class="photo-essay-article-content-time time-stamp-text text-timestamp"><?=time_ago()?>
                        </time>
                        <br/>
                        <span class="photo-essay-article-content-author author-name-text text-author">
                            <?php
                              echo the_author_meta('display_name', $recent['post_author']);
                            ?>
                        </span>
                    </div>
                </div>
                <?php
                    endforeach;
                ?>

            </div>
        </div>
    </div>


    <?php

    wp_reset_postdata();
    return ob_get_clean();
};
    add_shortcode('single_top_post_by_category','single_top_post_by_category');

    function five_sub_category_post($attr){

        ob_start();
        extract(shortcode_atts(array(
            'category' => 'Category Title',
        ), $attr));
    ?>
<div class="row category-subcategories flex-row">
        <div class="container flex-container">

    <?php
    global $wp;
    $current_link = home_url( $wp->request );
    $categories = get_category_by_slug($category);

    $sub_categories = get_categories(
                            array(
                                'parent' => $categories->term_id,
                                'order' => 'ASC',
                                'orderby' => 'term_id',
                            )
                        );

    foreach ($sub_categories as $sub_category):
        ?>
        <div class="category-subcategory-wrapper">
            <div class="col-xs-12 category-subcategory-title text-center">
                <h3 class="hidden-xs">
                    <a href="<?php echo $current_link.'/'.$sub_category->slug; ?>">
                        <span><?=$sub_category->name?></span>
                    </a>
                </h3>
                <h3 class="hidden-sm hidden-md hidden-lg">
                    <a href="<?php echo $current_link.'/'.$sub_category->slug; ?>">
                        <span><?=$sub_category->name?></span>
                    </a>
                </h3>
            </div>

            <div class="col-xs-12 category-subcategory-posts text-center">

                <?php

                $queryObject = new  Wp_Query( array(
                    'showposts' => 5,
                    'post_type' => array('post'),
                    'category_name' => $sub_category->slug,
//                    'meta_key' => 'post_views_count',
//                    'orderby' => 'meta_value_num',
                    'post_status' => 'publish',
//                    'date_query'    => array(
//                        'column'  => 'post_date',
//                        'after'   => '- 60 days'
//                    )
                ));

                if ( $queryObject->have_posts() ) :

                    while ( $queryObject->have_posts() ) :
                        $queryObject->the_post();

                        ?>
                        <div class="col-xs-12 col-sm-2 category-subcategory-post text-center">
                            <div class="col-xs-12 category-subcategory-post-image">
                                <a href="<?php echo esc_url( get_permalink() )?>" class="has-youtube-video-inner-categories">
                                    <div class="four_by_six">
                                        <?php
                                        if ( has_post_thumbnail() ) {
                                            the_post_thumbnail( 'post');
                                        }
                                        ?>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 category-subcategory-post-content">
                                <a href="<?php echo esc_url( get_permalink() )?>" class="photo-essay-article-content-title"> <?php the_title(); ?></a>
                            </div>
                        </div>

                        <?php
                    endwhile;
                endif;

                wp_reset_postdata();
                ?>


            </div>
        </div>


        <?php
    endforeach;
?>

</div>
</div>
<?php

        return ob_get_clean();
    }

add_shortcode('five_sub_category_post','five_sub_category_post');

function trending_post_show($attr){

    ob_start();
    extract(shortcode_atts(array(
        'category' => 'Category Title',
        'loop' => 1,
        'popular' => 'No',
        'days' => 30,

    ), $attr));



    if($popular == "Yes"):
        $filter = array(
            'meta_key' => 'post_views_count',
            'orderby' => 'meta_value_num',
            'post_status' => 'publish',
            'date_query'    => array(
                'column'  => 'post_date',
                'after'   => "'- '.$days.' days'",
            )
        );
    endif;

    $filter = array(
        'showposts' => (7*$loop),
        'post_type' => array('post'),
        'category_name' => $category,

    );

    $queryObject = new  Wp_Query($filter);
    if ( $queryObject->have_posts() ) :

        $i = 1;
        while ($queryObject->have_posts()) :
            $queryObject->the_post();
            ?>
            <div class="col-xs-12 <?php if ($i <= 3) echo "col-sm-4"; else echo "col-sm-3"; ?>  category-subcategory-post category-trending-post text-center">
                <div class="col-xs-12 col-sm-12 category-subcategory-post-image">
                    <a href="<?php echo esc_url(get_permalink()); ?>" class="has-youtube-video-masthead">
                        <div class="four_by_six">
                            <?php
                            if (has_post_thumbnail()):
                                the_post_thumbnail('post');
                            endif
                            ?>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-12 category-subcategory-post-content">
                    <a href="<?php echo esc_url(get_permalink()) ?>"
                       class="photo-essay-article-content-title"> <?php the_title(); ?></a>

                    <time class="photo-essay-article-content-time time-stamp-text text-timestamp">
                        <?php echo time_ago(); ?>
                    </time>
                    <span class="photo-essay-article-content-author author-name-text text-author">
                        <?php the_author(); ?>
                    </span>
                </div>
            </div>

            <?php
            if($i==7){
                $i=0;
            }
            $i++;
        endwhile;
    endif;
}


add_shortcode('trending_post_show','trending_post_show');
?>




