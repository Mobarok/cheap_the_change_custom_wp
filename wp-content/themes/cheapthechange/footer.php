<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/15/2018
 * Time: 11:36 PM
 */
?>

<div class="page-container footer-page-container">


    <div class="row footer-divider" style="padding-bottom: 20px">
        <hr class="subscribe-page-line">
    </div>


    <div class="row">
        <div class="container flex-container">


            <div class="bottom-nav flex-row">
                <div class="flex-row reference" style="text-align: left;">
                    <div class="col-md-2 col-sm-12 col-lg-2 col-xs-12">
                        <a href="https://www.huffingtonpost.com/" rel="nofollow" class="refer-link" TARGET="_blank"> Huffington Post </a>
                        <a href="https://www.bbc.com/" class="refer-link" rel="nofollow" TARGET="_blank"> BBC.com </a>
                    </div>
                    <div class="col-md-2 col-sm-12 col-lg-2 col-xs-12">
                        <a href="https://www.gty.org/" class="refer-link" rel="nofollow" TARGET="_blank"> gty.org </a>
                        <a href="https://www.investopedia.com/" rel="nofollow" class="refer-link" TARGET="_blank">investopedia.com  </a>
                    </div>
                    <div class="col-md-2  col-sm-12 col-lg-2 col-xs-12">
                        <a href="https://www.wsj.com/asia" class="refer-link" rel="nofollow" TARGET="_blank"> wsj.com </a>

                        <a href="https://www.cnet.com/" class="refer-link" rel="nofollow" TARGET="_blank"> cnet.com  </a>
                    </div>
                    <div class="col-md-2  col-sm-12 col-lg-2 col-xs-12">
                        <a href="https://www.dictionary.com/" class="refer-link" rel="nofollow" TARGET="_blank"> Dictionary.com </a>
                        <a href="https://www.thesaurus.com/" rel="nofollow" class="refer-link" TARGET="_blank"> thesaurus.com </a>
                    </div>
                    <div class="col-md-2  col-sm-12 col-lg-2 col-xs-12">
                        <a href="https://www.rhymezone.com/" rel="nofollow" class="refer-link" TARGET="_blank"> rhymezone.com  </a>
                        <a href="http://www.rimaritmo.com/" rel="nofollow" class="refer-link" TARGET="_blank"> Rimaritmo.com </a>
                        <a href="https://www.artofmanliness.com/category/relationships-family/" class="refer-link" TARGET="_blank" rel="nofollow"> artofmanliness.com </a>
                    </div>
                    <div class="col-md-2 col-sm-12 col-lg-2 col-xs-12">
                        <a href="http://www.datecount.com/" rel="nofollow" class="refer-link" TARGET="_blank"> datecount.com </a>
                        <a href="https://weather.com/weather/tenday/l/Las+Vegas+NV+USNV0049:1:US" class="refer-link" TARGET="_blank" rel="nofollow"> weather.com </a>
                        <a href="https://www.groupon.com/" rel="nofollow" class="refer-link" TARGET="_blank"> groupon.com </a>

                    </div>



                </div>
            </div>
        </div>
    </div>

    <div class="row footer-divider">
        <hr class="subscribe-page-line">
    </div>

    <div class="row subscribe-page-menu-social">
        <div class="container flex-container">

            <div class="bottom-nav flex-row">
                <div class="container flex-container">

                    <?php
                        if(function_exists('wp_nav_menu')){
                            wp_nav_menu( array(
                                    'theme_location' =>'footer-menu',
                                    'menu'           => 'footer_menu',
                                    'depth'             => 0,
                                    'container'         => 'div',
                                    'container_class'   => 'flex-container',
                                    'container_id'      => '',
//                                    'menu_class'        => 'nav navbar-nav',
                                    'menu_id'        => 'footerMenu',
                                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                    'walker'            => new wp_bootstrap_navwalker())
                            );
                        }
                    ?>
                </div>
            </div>

            <div class="social-nav-bottom flex-row">
                <div class="container flex-container">
                    <ul class="social-networks">
                        <li>
                            <a href="#" target="newSocial" onclick="parsely_click_tracking('twitter-footer')">
                                <img class="lazyload" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/Twitter-2x.png" alt="Twitter">
                                <span class="sr-only">Twitter</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="newSocial" onclick="parsely_click_tracking('facebook-footer')">
                                <img class="lazyload" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/Facebook-2x.png"  alt="Facebook">
                                <span class="sr-only">Facebook</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="newSocial" onclick="parsely_click_tracking('instagram-footer')">
                                <img class="lazyload" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/Instagram-2x.png" alt="Instagram">
                                <span class="sr-only">Instagram</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="newSocial" onclick="parsely_click_tracking('pinterest-footer')">
                                <img class="lazyload" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/Pinterest-2x.png" alt="Pinterest">
                                <span class="sr-only">Pinterest</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="parsely_click_tracking('youtube-footer')">
                                <img class="lazyload" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/YouTube-2x.png" alt="YouTube">
                                <span class="sr-only">YouTube</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="parsely_click_tracking('rss-footer')">
                                <img class="lazyload" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/RSS-2x.png"  alt="RSS Feed">
                                <span class="sr-only">RSS</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>

<footer id="footer">
    <div class="at-footer container">

        <div class="col-sm-6 col-sm-push-6 col-md-4 col-md-push-8">
            <div class="row bottom-form flex-row">
                <div class="container flex-container footer-form text-center">
                    <h3 style="display: none;">Subscribe to Our Newsletter</h3>
                    <form id="footerForm" accept-charset="utf-8" action="#" method="post" class="email-form iterable-form" form-location="footer">
                        <input type="hidden" name="name">
                        <label for="emailaddressbottom">Subscribe to Our Newsletter</label>

                    <div class="input-group">
                        <div class="email-box">
                            <?php

                            echo do_shortcode('[contact-form-7 id="12258" title="Newsletter"]');
                            ?>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-sm-pull-6 col-md-8 col-md-pull-4">

            <div class="col-xs-12 footer-copyright-text">
                <p class="copyright" style="line-height: 15px;">
                    Dosh Enterprises LLC<br/>
                    Dosh Publishing is the publisher. <br/>
                    Editor – Sean Christopher <BR/>
                     <BR/>

                    © 2019 Dosh Company LLC - All
                    rights reserved.   <span style="margin-left: 20px;">Developed by <a href="www.estheticsoft.com" target="_blank" nofollow>Esthetic Soft</a> </span><br>

                    Cheap The Change are service marks owned by the Dosh Company LLC. Other marks contained on this website are the property of their respective owners. Dosh Company LLC is not licensed by or affiliated with any third-party marks on its website and they do not endorse, authorize, or sponsor our content except as noted herein. Please read our <a href="<?php bloginfo('home')?>/privacy-policy" title="Privacy Policy">Privacy Policy</a> and <a href="<?php bloginfo('home')?>/terms-of-use/" title="Terms of Use">Terms of Use</a>.</p>
            </div>
        </div>


    </div>
</footer>

<script type='text/javascript' src='<?php echo esc_url(get_template_directory_uri()); ?>/assets/js/bootstrap.minea87.js'></script>
<script type='text/javascript' src='<?php echo esc_url(get_template_directory_uri()); ?>/assets/js/main8948.js'></script>
<script type='text/javascript' src='<?php echo esc_url(get_template_directory_uri()); ?>/assets/js/customJSeb65.js'></script>
<script type='text/javascript' src='<?php echo esc_url(get_template_directory_uri()); ?>/assets/js/wp-embed.min822f.js'></script>

<style type="text/css">
    #footer #footerForm>label{font-family:Roboto,sans-serif;font-size:16px;color:#fff;margin:0 0 5px 0;text-align:left;font-weight:400!important;text-indent:-5px;display:block}.home-page .single-posts-recommended{clear:both;background-color:#f5f5f5;margin-left:-5px;margin-right:-5px}.home-page .single-posts-recommended .post-recommended-title h3{margin-top:12px;margin-bottom:12px;color:#fff;padding:7px 10px;background-color:#0cbb93f0;font-size:15px;text-transform:uppercase;font-family:'Roboto',sans-serif;font-weight:400;display:inline-block;float:left;margin-left:10px}.home-page .single-posts-recommended .post-recommended-inner{padding:0;display:flex;flex-direction:row;flex-wrap:wrap}.home-page .single-posts-recommended a.photo-essay-article-content-title{font-family:'Crimson Text',serif;font-size:16px;color:#000;text-decoration:none}.home-page .single-posts-recommended .category-subcategory-post-content{padding-top:10px;padding-bottom:10px}.home-page .single-posts-recommended .post-recommended-inner>div>div.category-subcategory-post-content{background:#fff;padding-left:10px;padding-right:10px;margin-bottom:25px;flex-grow:2}.home-page .single-posts-recommended .post-recommended-inner>div>div.category-subcategory-post-content a{display:inline-block;font-family:'Roboto Condensed',sans-serif;font-size:16px;color:#222;text-align:left;font-weight:700}.home-page .single-posts-recommended .post-recommended-inner>div>div.category-subcategory-post-content a:hover{text-decoration:none;border:0;color:#2bbbb3}.home-page .single-posts-recommended .post-recommended-inner>div{padding-left:15px;padding-right:15px;display:flex;flex-direction:column}.home-page .single-posts-recommended .post-recommended-inner>div>div{padding-left:0;padding-right:0}body .sticky-social:not(.fixed) {bottom:0!important}.home-page .single-posts-recommended{margin-right:0!important;margin-left:0!important}
</style>






<!-- JS COde for Count -->
<script type="text/javascript">
    function social_box_dynamic_stats_update(type,value){
        if(type==='facebook_count')
        {jQuery('.at-newsletter-block .facebook_like_count').html(value);}
        else if(type==='pinterest_count')
        {jQuery('.at-newsletter-block .pinterest_follower_count').html(value);}
        else if(type==='email_count')
        {jQuery('.at-newsletter-block .email_subscriber_count').html(value);}
        else if(type==='total_subscribers_count')
        {jQuery('.at-newsletter-block .total_subscribers').html(value);}
    }

    jQuery(document).ready(function(){
        update_dynamic_stats('facebook_count',social_box_dynamic_stats_update);update_dynamic_stats('pinterest_count',social_box_dynamic_stats_update);update_dynamic_stats('email_count',social_box_dynamic_stats_update);
        update_dynamic_stats('total_subscribers_count',social_box_dynamic_stats_update);
    });
</script>

<script type="text/javascript">
    jQuery(window).load(function(){

        if(typeof tph!='undefined'&&typeof tph.utility!='undefined'){
            jQuery(document).on('scroll',function(){
                tph.utility.lazy_load_background('.verticals-bottom-vertical.lazy-load-background');
            })
            tph.utility.lazy_load_background('.verticals-bottom-vertical.lazy-load-background');}


    });
</script>



<span style='display:none' id='facebookCountCurrent'>0</span>
<span style='display:none' id='twitterCountCurrent'>0</span>
<span style='display:none' id='emailCountCurrent'>0</span>
<span style='display:none' id='pinterestCountCurrent'>0</span>
<span style='display:none' id='monthlyVisitorsCurrent'>0</span>
<span style='display:none' id='monthlyPageviewsCurrent'>0</span>
<span style='display:none' id='totalSubscribersCountCurrent'>0</span>


<!--<noscript class="psa_add_styles">-->

    <style type="text/css">img.wp-smiley,img.emoji{display:inline!important;border:none!important;box-shadow:none!important;height:1em!important;width:1em!important;margin:0 .07em!important;vertical-align:-.1em!important;background:none!important;padding:0!important}</style>

    <link rel='stylesheet' id='penny-ovo-font-css' href='https://fonts.googleapis.com/css?family=Shrikhand%7CMontserrat%7CArimo%7COvo%7CRoboto+Condensed%3A400%2C700%7CRoboto%3A200%2C300%2C400%2C700%7CCrimson+Text%3A400%2C400i%7CCoustard&amp;ver=c396be2c130d6542109fb75ca7788b0d' type='text/css' media='all'/>

    <link rel='stylesheet' id='jquery-ui-css' href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.min.css' type='text/css' media='all'/>


    <style id='tablepress-default-inline-css' type='text/css'>.tablepress,table{border-spacing:0;border-collapse:collapse}.tablepress thead th{text-align:left;background-color:#0cbb93f0;color:#fff;border-bottom:none;border:solid 1px #0cbb93f0}.tablepress tbody td,.tablepress thead th{vertical-align:middle;line-height:120%;text-align:left}.tablepress td,.tablepress th{padding:8px 8px 8px 12px;border:0;text-align:left;border-style:none}.tablepress thead th{height:75px;text-align:left}.tablepress tbody td{height:70px}.column-1,.column-2,.column-3,.column-4,.column-5{width:20%}.tablepress tbody td,.tablepress tfoot th{border-top:1px solid #ddd}.tablepress .odd td{background-color:#f9f9f9}</style>

    <style  type="text/css">.server_id_float{display:none!important}ul.single-social>li{margin-right:5px}ul.single-social>li:last-of-type{margin-right:0}.ask-penny-contact-page #penny-form-success{border-bottom:none}@media screen and (max-width:467px) and (min-width:334px){html body.one-time-fixed-body nav.navbar.navbar-fixed-top.one-time-fixed{top:60px!important}}.tph-wfh-class-short-header #wfh-search .tph-wfh-class-header-search-submit{height:58px!important}.force-hidden{display:none!important}@media (min-width:479px){.postid-100405 .post-page .single-post-content .single-post-content-inner ul:not(.single-social) li:before{display:none!important}}body.postid-98556 .read-time{display:none}.page-id-103279 .subscribe-page-line{margin-top:1px!important}</style>

    <style type="text/css">.single-post-content iframe{max-width:100%}#footer #footerForm>label{font-family:Roboto,sans-serif;font-size:16px;color:#fff;margin:0 0 5px 0;text-align:left;font-weight:400!important;text-indent:-5px;display:block}.author .masthead-block .flex-container{display:block!important}.home-page .single-posts-recommended{clear:both;background-color:#f5f5f5;margin-left:-5px;margin-right:-5px}.home-page .single-posts-recommended .post-recommended-title h3{margin-top:12px;margin-bottom:12px;color:#fff;padding:7px 10px;background-color:#0cbb93f0;font-size:15px;text-transform:uppercase;font-family:'Roboto',sans-serif;font-weight:400;display:inline-block;float:left;margin-left:10px}.home-page .single-posts-recommended .single-posts-recommended-sponsored{font-size:11px;font-family:"Roboto Condensed",sans-serif;color:#767676;text-align:right;padding-bottom:5px;padding-right:10px}.home-page .single-posts-recommended .post-recommended-inner{padding:0;display:flex;flex-direction:row;flex-wrap:wrap}.home-page .single-posts-recommended a.photo-essay-article-content-title{font-family:'Crimson Text',serif;font-size:16px;color:#000;text-decoration:none}.home-page .single-posts-recommended .category-subcategory-post-content{padding-top:10px;padding-bottom:10px}.home-page .single-posts-recommended .post-recommended-inner>div>div.category-subcategory-post-content{background:#fff;padding-left:10px;padding-right:10px;margin-bottom:25px;flex-grow:2}.home-page .single-posts-recommended .post-recommended-inner>div>div.category-subcategory-post-content a{display:inline-block;font-family:'Roboto Condensed',sans-serif;font-size:16px;color:#222;text-align:left;font-weight:700}.home-page .single-posts-recommended .post-recommended-inner>div>div.category-subcategory-post-content a:hover{text-decoration:none;border:0;color:#2bbbb3}.home-page .single-posts-recommended .post-recommended-inner>div{padding-left:15px;padding-right:15px;display:flex;flex-direction:column}.home-page .single-posts-recommended .post-recommended-inner>div>div{padding-left:0;padding-right:0}body .sticky-social:not(.fixed) {bottom:0!important}body .up-next-container{display:none}@media (max-width:991px){body .up-next-container{background:rgba(255,255,255,.95);display:block;bottom:-75px;height:75px;left:0;margin:0 auto;padding-top:10px;position:fixed;text-align:center;width:100%;z-index:999999;cursor:pointer}body .up-next-container:hover,body .up-next-container:focus,body .up-next-container:active{outline:0}body .up-next-container .up-next-image{display:inline-block;height:75px;left:0;position:absolute;top:0}body .up-next-container .up-next-text{color:#252525;display:inline-block;font-family:'Roboto Condensed',sans-serif;font-weight:bold;letter-spacing:0;position:absolute;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%);text-align:left;top:50%}}@media (min-width:320px){body .up-next-text{font-size:16px;left:66%;width:70%}}@media (min-width:375px){body .up-next-text{font-size:17px;left:62%;width:75%}}@media (min-width:425px){body .up-next-text{font-size:19px}}.postid-84082 .row.vertical-image-wrapper{display:flex;flex-wrap:wrap}.postid-84082 .row.vertical-image-wrapper .vertical-image-text{align-self:center}#debugData{display:none}.home-page .single-posts-recommended{margin-right:0!important;margin-left:0!important}.page-template-university-ecourse .subscribeForm img:first-of-type{max-width:100%}.page-template-university-ecourse-template .subscribeForm>.container{max-width:100%!important}#gform_9 div.gform_footer.top_label{text-align:center}#gform_8 div.gform_footer.top_label{text-align:center}</style>
<!--</noscript>-->

<script data-pagespeed-no-defer>(function(){function b(){var a=window,c=e;if(a.addEventListener)a.addEventListener("load",c,!1);else if(a.attachEvent)a.attachEvent("onload",c);else{var d=a.onload;a.onload=function(){c.call(this);d&&d.call(this)}}};var f=!1;function e(){if(!f){f=!0;for(var a=document.getElementsByClassName("psa_add_styles"),c=0,d;d=a[c];++c)if("NOSCRIPT"==d.nodeName){var k=document.createElement("div");k.innerHTML=d.textContent;document.body.appendChild(k)}}}function g(){var a=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||null;a?a(function(){window.setTimeout(e,0)}):b()}
        var h=["pagespeed","CriticalCssLoader","Run"],l=this;h[0]in l||!l.execScript||l.execScript("var "+h[0]);for(var m;h.length&&(m=h.shift());)h.length||void 0===g?l[m]?l=l[m]:l=l[m]={}:l[m]=g;})();
    pagespeed.CriticalCssLoader.Run();</script>


<?php wp_footer();?>
</body>

</html>

