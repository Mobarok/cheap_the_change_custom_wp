<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/16/2018
 * Time: 8:16 PM
 */

/*
 Template Name: Contact Us
 */

    // Do not allow directly accessing this file.
    if ( ! defined( 'ABSPATH' ) ) {
        exit( 'Direct script access denied.' );
    }

 get_header(); ?>

<div id="content" class="col-sm-12" style="margin-top:25px">
    <div class="contact-page subscribeForm">
        <div class="container">
            <h2 class="contact-page-head">Contact Us</h2>
            <div class="ad-subheading-separator"></div>


            <div class="row contact-page subscribeForm" id="success-form-wrapper" style="display:none">
                <div class='img-responsive center-block'>
                    <img id="contact-success-image" src="<?php echo esc_url(get_template_directory_uri());?>/assets/images/Success-Page.gif" class="img-responsive center-block"/>
                    <h2 class="contact-page-head">Success!</h2>
                    <div class="ad-subheading-separator"></div>
                    <p class="contact-success">Your message has been sent.</p>
                    <a href="<?php bloginfo('home');?>" class="btn btn-block btn-primary">Return to Homepage</a>
                </div>
            </div>


            <div class="row" id="contact-form">
                <p class="subscribe-description">
                    Please fill out the form below to get in touch with us.
                    You can also reach out to one of our specialists. Thanks!
                </p>
                <div class="col-xs-12 col-md-6 col-md-push-6 subscribe-page-main-form" style="overflow:hidden">
                    <div class="visible-inline-block out-contact">
                        <a class="out-contact-link" href="#">
                            <div>
                                <img src="<?php echo esc_url(get_template_directory_uri());?>/assets/images/advertise-icon.png" style="display: block !important;" class="img-responsive center-block out-contact-img"/>
                            </div>
                            <div class="out-contact-name">advertise</div>
                            <div class="hr-grey"></div>
                            <div class="out-contact-tag hidden-xs hidden-sm">Want to advertise with us?</div>
                            <div class="out-contact-btn">Click here</div>
                        </a>
                    </div>
                    <div class="visible-inline-block out-contact">
                        <a class="out-contact-link" href="#">
                            <div>
                                <img src="<?php echo esc_url(get_template_directory_uri());?>/assets/images/Contribute-Icon.png" class="img-responsive center-block out-contact-img"/>
                            </div>
                            <div class="out-contact-name">Contribute</div>
                            <div class="hr-grey"></div>
                            <div class="out-contact-tag hidden-xs hidden-sm">Want to write for us?</div>
                            <div class="out-contact-btn">click here</div>
                        </a>
                    </div>
                    <div class="visible-inline-block out-contact">
                        <a class="out-contact-link" href="#">
                            <div>
                                <img src="<?php echo esc_url(get_template_directory_uri());?>/assets/images/DearPenny-Icon.png" class="img-responsive center-block out-contact-img"/>
                            </div>
                            <div class="out-contact-name">Dear Sir</div>
                            <div class="hr-grey"></div>
                            <div class="out-contact-tag hidden-xs hidden-sm">Have a question for Dear Sir?</div>
                            <div class="out-contact-btn">click here</div></a>
                    </div>
                    <div class="visible-inline-block out-contact">
                        <a class="out-contact-link" href="https://www.thepennyhoarder.com/press/">
                            <div>
                                <img src="<?php echo esc_url(get_template_directory_uri());?>/assets/images/Press-Icon.png" class="img-responsive center-block out-contact-img"/>
                            </div>
                            <div class="out-contact-name">Press</div>
                            <div class="hr-grey"></div>
                            <div class="out-contact-tag hidden-xs hidden-sm">Need a quote or want to book us for an interview?</div>
                            <div class="out-contact-btn">click here</div></a>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6 col-md-pull-6 email-form">
                    <form class="form-horizontal" id="choose-contact" method="post" action="#"/>
                    <input type="hidden" id="submit_mail" name="submit_mail" value="b14b2eabdf"/>
                    <input type="hidden" name="_wp_http_referer" value=""/>
                    <fieldset>
                        <div class="form-group">
                            <div class="">
                                <select id="select-question" name="select-question" class="form-control contact-input">
                                    <option selected disabled hidden>Reason*</option>
                                    <option value="feedback">Have a question about something we wrote?</option>
                                    <option value="support">Having a problem with a link on our site?</option>
                                    <option value="product">Have feedback about our site?</option>
                                    <option value="feedback">Do you have other comments or concerns?</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <input id="textinput-name" name="textinput-name" type="text" placeholder="Name*" class="form-control input-md contact-input">
                        </div>
                        <div class="form-group">
                            <input id="textinput-email" name="textinput-email" type="text" placeholder="Email*" class="form-control input-md">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control input-area" id="textarea-mesg" name="textarea-mesg" maxlength="2000" rows="5" placeholder="Message*"></textarea>
                        </div>
                        <div class="form-group captcha-holder">
                            <input type="hidden" class="hiddenRecaptchaContact required" name="hiddenRecaptchaContact" id="hiddenRecaptchaContact">
                            <div class="g-recaptcha" data-sitekey="6Ldr84gUAAAAABT_ttFpwBm7GfulfGQ5cTA9GpyH" data-callback="removeGerror">

                            </div>
                        </div>
                        <div class="">
                            <div>
                                <button type="submit" class="btn btn-block btn-primary">Submit</button>
                                <p class="contact-required">*Required</p>
                            </div>
                        </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-container footer-page-container">

    <div class="row footer-divider">
        <hr class="subscribe-page-line">
    </div>

    <?php get_footer(); ?>
