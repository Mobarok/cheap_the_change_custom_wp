<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cheap_the_change');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']A1IzuN1Ssw(3udf~pON=g_N[K@<}D&vhBr[,<;RmA4Nodn~d2h1m^jB#t;PS-;]');
define('SECURE_AUTH_KEY',  '-1z9Y%0*/O*0o53K]R_Np+Rtof&8T;T[d-EyNKy0%*t7:e4r)xT_VR|/A.4i%[3`');
define('LOGGED_IN_KEY',    '+V W{^}_xd$I]MtzOuY+fvK/E->r6K(LoKdzS$LW)JQ;d&R]3]ur3$YDOt3f%eI7');
define('NONCE_KEY',        'q%xIh&25~gi *9!<p6 9>N3>7Y#0>X2qfL.vU>8cI;+$9K(LF>K&bYCq7K%SOb{&');
define('AUTH_SALT',        'dRC+{_XuzF.[!v&u>VQBhGJHAbH>}cE:uqv=sG;ELo7)/O@luKYQ<S<hTt&iQCK ');
define('SECURE_AUTH_SALT', 'E1v/Ts7FHbMr.^3P7<:m_K_BB@JG>}M!0H4 ]/tI@afDQ`z^y]GKtIN=ywno]EWO');
define('LOGGED_IN_SALT',   'B{A70/sqjBl|bsCoS%EHN+tpro(q^GqYeZ=y*GZ{n5xuQ&+ANvvH-<2V(8k<d7o#');
define('NONCE_SALT',       ',u/am@/-z q(a(.`J--7G!vt-RhdDpl*sS,0mGf3`CE87#Yk(C rpC:tXE]|EGZO');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ctc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
