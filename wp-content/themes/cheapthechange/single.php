<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/25/2018
 * Time: 12:34 AM
 *
 * Template Name:Single
 */

$GLOBAL['body_class'] = "post-template-default single single-post single-format-standard  prodServer non-touch-device non-ios-device";
get_header();

setPostViews(get_the_ID());


global $wp;

// get current url with query string.
$current_url =  home_url( $wp->request );
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>

<div class="page-container post-page" data-interaction="single-posts-post-wrapper">
    <div class="row photo-essay flex-row">
        <div class="container flex-container">
            <div class="col-xs-12 col-md-8 single-post-content" data-interaction="single-posts-content-wrapper">
                <div class="col-xs-12 single-post">


                    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                        <?php
                        $category_detail=get_the_category($post->ID);
?>
                        <span property="itemListElement" typeof="ListItem">
                            <a property="item" typeof="WebPage" title="Go to Cheap The Change." href="<?=bloginfo('home')?>" class="home"><span property="name">Home</span>
                            </a><meta property="position" content="1">
                        </span> <span>&gt;</span>
                        <span property="itemListElement" typeof="ListItem">
                            <a property="item" typeof="WebPage" title="Go to the <?=$category_detail[0]->cat_name?> category archives." href="<?php echo esc_url( get_permalink( get_page_by_title( $category_detail[0]->cat_name ) ) ); ?>" class="taxonomy category">
                                <span property="name"><?=$category_detail[0]->cat_name?></span>
                            </a>
                            <?php
                            if(isset($category_detail[1]->cat_name)):
                            ?>
                            <meta property="position" content="2"></span> <span>&gt;</span>
                        <span property="itemListElement" typeof="ListItem">
                            <a property="item" typeof="WebPage" title="Go to the <?=$category_detail[1]->cat_name?> category archives." href="<?php echo esc_url( get_permalink( get_page_by_title( $category_detail[1]->cat_name ) ) ); ?>" class="taxonomy category"><span property="name"><?=$category_detail[1]->cat_name?></span>
                            </a><meta property="position" content="3">
                        </span>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-12 read-time">
                        <p>
                            <?php
                            echo do_shortcode('[rt_reading_time  postfix="min read" postfix_singular="minute"]');
                            ?>
                        </p>
                    </div>

                    <?php
                            if (have_posts()) :
                                while (have_posts()) : the_post();
                    ?>

                    <div class="col-xs-12 single-post-title">
                        <h1><?php the_title(); ?></h1>
                    </div>

                    <div class="col-xs-12 single-post-social-top">
                        <div class="col-xs-12 sticky-social share-numbers">

                            <ul class="single-social">
                                <li class="at-top-fb-share single-social-facebook">
                                    <a onclick="window.open(this.href, 'facebookshare','left=20,top=20,width=600,height=300,toolbar=0,resizable=1'); return false;" href="https://www.facebook.com/share.php?u=<?=$current_url?>?utm_source=BottomFacebook_ShareButton_20181224" target="_blank">
                                        <i></i>
                                        <span class="sr-only">Share on Facebook</span>
                                    </a>
                                </li>
                                <li class="at-top-pin-share single-social-pinterest">
                                    <a onclick="window.open(this.href,'','status=no,resizable=yes,scrollbars=yes,personalbar=no,directories=no,location=no,toolbar=no,menubar=no,width=750,height=568,left=0,top=0');return false;" href="https://www.pinterest.com/pin/create/link/?url=<?=$current_url?>&amp;media=<?=$image[0]?>&amp;description=<?php the_title()?>&amp;utm_source=BottomPinterest_ShareButton_20181224" target="_blank">
                                        <i></i>
                                        <span class="sr-only">Share on Pinterest</span>
                                    </a>
                                </li>
                                <li class="at-top-tw-share single-social-twitter">
                                    <a onclick="window.open(this.href, 'twittershare','left=20,top=20,width=600,height=300,toolbar=0,resizable=1'); return false;" href="https://twitter.com/share?url=<?=$current_url?>/?utm_source=BottomTwitter_ShareButton_20181224&text=<?php the_title();?>" target="_blank">
                                        <i></i>
                                        <span class="sr-only">Share on Twitter</span>
                                    </a>
                                </li>
<!--                                <li class="at-top-sms-share single-social-text hidden-md hidden-lg">-->
<!--                                    <a href="" target="_blank">-->
<!--                                        <i></i>-->
<!--                                        <span class="sr-only">Share by SMS</span>-->
<!--                                    </a>-->
<!--                                </li>-->
                                <li class="at-top-email-share single-social-email">
                                    <a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?=the_title()?>  <?=get_permalink() ?>" target="_blank">
                                        <i></i>
                                        <span class="sr-only">Share by Email</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="share-numbers-spacer hidden-md hidden-lg"></div>


                            <div id="view-numbers" class="share-numbers-wrap with-border">
                                <span class="share-count">
                                    <?php echo getPostViews(get_the_ID());?>
                                </span>
                                <br/>
                                <span class="share-text">views</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 single-post-author">
                        <div class="col-xs-8 col-sm-8">
                            <?php $author_id=$post->post_author; ?>
                            <?php $avatar = get_avatar_url( $author_id, 32  ); ?>
                            <?php if(!empty($avatar)): ?>
                                <img src="<?php echo $avatar; ?>" class="author-thumbnail" alt=" <?php echo the_author_meta('display_name',$post->post_author);?>">
                            <?php else: ?>
                                <img src="<?php echo esc_url(get_template_directory_uri());?>/assets/images/avatar.png" class="author-thumbnail" >
                            <?php endif; ?>
                            <div class="author-container">
                                <p class="author-name">by
                                    <a href="#" class="single-post-author-name">
                                        <?php echo the_author_meta('display_name',$post->post_author);?>
                                    </a>
                                </p>
<!--                                <p class="author-title">Data Journalist</p>-->
                            </div>


                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <time><?php echo time_ago();?></time>
                        </div>
                    </div>

                    <div class="col-xs-12 single-post-image">
                        <a href="<?=get_permalink() ?>" class="">
                            <div class="four_by_six">
                                <?php
                                if(has_post_thumbnail()):
                                    the_post_thumbnail('post');
                                endif
                                ?>
                            </div>
                        </a>

                        <!-- SINGLE POST IMAGE BOTTOM TEXT

                        <div class="single-post-image-meta">
                            <div class="col-xs-12 single-post-image-meta-title">
                                    <span>                 </span>
                            </div>
                        </div>

                        -->
                    </div>

                    <div class="col-xs-12 single-post-content-inner">


                        <?php
                        /* Start the Loop */
                         the_content();

                        ?>

                        <div class="single-social-share-bottom text-center">

                            <ul class="single-social">
                                <li class="at-bottom-fb-share single-social-facebook">
                                    <a onclick="window.open(this.href, 'facebookshare','left=20,top=20,width=600,height=300,toolbar=0,resizable=1'); return false;" href="https://www.facebook.com/share.php?u=<?=$current_url?>?utm_source=BottomFacebook_ShareButton_20181224" target="_blank">
                                        <i></i>
                                        <span class="sr-only">Share on Facebook</span>
                                    </a>
                                </li>
                                <li class="at-bottom-pin-share single-social-pinterest">
                                    <a onclick="window.open(this.href,'','status=no,resizable=yes,scrollbars=yes,personalbar=no,directories=no,location=no,toolbar=no,menubar=no,width=750,height=568,left=0,top=0');return false;" href="https://www.pinterest.com/pin/create/link/?url=<?=$current_url?>&amp;media=<?=$image[0]?>&amp;description=<?php the_title()?>&amp;utm_source=BottomPinterest_ShareButton_20181224" target="_blank">
                                        <i></i>
                                        <span class="sr-only">Share on Pinterest</span>
                                    </a>
                                </li>
                                <li class="at-bottom-tw-share single-social-twitter">
                                    <a onclick="window.open(this.href, 'twittershare','left=20,top=20,width=600,height=300,toolbar=0,resizable=1'); return false;" href="https://twitter.com/share?url=<?=$current_url?>/?utm_source=BottomTwitter_ShareButton_20181224&text=<?php the_title();?>" target="_blank">
                                        <i></i>
                                        <span class="sr-only">Share on Twitter</span>
                                    </a>
                                </li>
<!--                                <li class="at-bottom-sms-share single-social-text hidden-md hidden-lg">-->
<!--                                    <a href="sms:?body=We%20Did%20Our%20Homework,%20and%20These%2011%20College%20Majors%20Can%20Boost%20Your%20Earnings https%3A%2F%2Fwww.thepennyhoarder.com%2Flife%2Fcollege%2Fbest-college-majors-earnings%2F" target="_blank">-->
<!--                                        <i></i>-->
<!--                                        <span class="sr-only">Share by SMS</span>-->
<!--                                    </a>-->
<!--                                </li>-->
                                <li class="at-bottom-email-share single-social-email">
                                    <a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?=the_title()?>  <?=get_permalink() ?>">
                                        <i></i>
                                        <span class="sr-only">Share by Email</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="newsletter-signup-wrapper-for-digioh">
                            <div class="col-xs-12 newsletter-wrap flex-row hidden-md hidden-lg">
                                <div class="container flex-container">
                                    <div class="col-xs-12 newsletter-form">
                                        <div class="col-xs-12 col-sm-6 newsletter-form-title">
                                            <div class="text-subheading">Ready to stop worrying about money?</div>
                                            <p>Get the Penny Hoarder Daily</p>
                                            <p class="email-privacy-policy-blurb-white"><a href="https://www.thepennyhoarder.com/privacy/?aff_sub2=homepage">Privacy Policy</a></p>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 newsletter-form-form">
                                            <form id="sidebarForm" accept-charset="utf-8" action="#" method="post" class="email-form iterable-form" form-location="mobile-post">
                                                <input type="hidden" name="name">
                                                <label for="33719fa28db7b74962cda1d55519390b" style="position: absolute;z-index: 1;pointer-events: none;color: transparent;">Email address</label>
                                                <?php echo do_shortcode('[contact-form-7 id="12264" title="Sign Up"]');?>
    </span>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
            <aside class="col-xs-12 col-md-4 content-sidebar">
                <div class="single-posts-recommended hidden-md hidden-lg">

                    <div class="container">

                        <div class="col-xs-12 post-recommended-title text-center">
                            <h3>Recommended Content</h3>
                        </div>

                        <div class="col-xs-12 post-recommended-inner text-center" data-interaction="single-posts-below-content-recommended-content-wrapper">
                            <?php
                            $queryObject = new  Wp_Query( array(
                                'showposts' => 6,
                                'meta_key' => 'post_views_count',
                                'orderby' => 'meta_value_num',
                                'post_status' => 'publish',
                                'date_query'    => array(
                                    'column'  => 'post_date',
                                    'after'   => '- 365 days'
                                )
                            ));

                            if ( $queryObject->have_posts() ) :
                            while ( $queryObject->have_posts() ) :
                            $queryObject->the_post();

                            ?>

                            <div class="col-xs-12 col-sm-4 col-md-2 text-center block-b at-blockb2" data-post-id="38620" data-interaction="single-posts-below-content-recommended-content-1">
                                <div class="col-xs-12 category-subcategory-post-image">
                                    <a href="<?php echo esc_url( get_permalink() )?>" class=" has-youtube-video-home-posts" data-interaction="single-posts-below-content-recommended-content-1-thumbnail">
                                        <div class="four_by_six">
                                            <?php
                                            if ( has_post_thumbnail() ) {
                                                the_post_thumbnail( 'post');
                                            }
                                            ?>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xs-12 category-subcategory-post-content">
                                    <a href="<?php echo esc_url( get_permalink() )?>" class="photo-essay-article-content-title" data-post-id="38620" data-interaction="single-posts-below-content-recommended-content-1-title">
                                        <?php the_title();?>
                                    </a>
                                </div>
                            </div>
                                <?php

                                endwhile;
                                endif;
                                wp_reset_postdata();
                            ?>

                        </div>
                    </div>
                </div>

                <div class="row newsletter-wrap flex-row hidden-xs hidden-sm">
                    <div class="container flex-container">
                        <div class="col-xs-12 newsletter-form">
                            <div class="col-xs-12 newsletter-form-title">
                                <div class="text-subheading">Ready to stop worrying about money? </div>
                                <p>Get the Penny Hoarder Daily</p>
                            </div>
                            <div class="col-xs-12 newsletter-form-form">
                                <form id="sidebarForm" accept-charset="utf-8" action="#" method="post" class="email-form iterable-form" form-location="post-sidebar">
                                    <input type="hidden" name="name">
                                    <label for="13282f151d4dc06aea28cbc167059ecb" style="position: absolute;z-index: 1;pointer-events: none;color: transparent;">Email address</label>
                                    <?php echo do_shortcode('[contact-form-7 id="12264" title="Sign Up"]');?>
                                </form>
                            </div>
                            <div class="col-xs-12 newsletter-form-blurb">
                                <p class="email-privacy-policy-blurb-white"><a href="https://www.thepennyhoarder.com/privacy/?aff_sub2=homepage">Privacy Policy</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row category-subcategory flex-row">
                    <div class="container flex-container">
                        <div class="col-xs-12">

                            <div class="single-posts-trending" >
                                <div class="col-xs-12 text-center">
                                    <?php
                                        $category_detail=get_the_category($post->ID);//$post->ID

                                    ?>
                                    <h3>Trending in <?=$category_detail[0]->name?></h3>
                                </div>

                                <div class="col-xs-12 single-posts-trending-inner text-center">

                                    <?php

                                    $queryObject = new  Wp_Query( array(
                                        'showposts' => 3,
                                        'post_type' => array('post'),
                                        'category_name' => $category_detail[0]->slug,
                                        'post_status' => 'publish',
                                    ));

                                    if ( $queryObject->have_posts() ) :

                                    while ( $queryObject->have_posts() ) :
                                    $queryObject->the_post();

                                    ?>


                                    <div class="col-xs-12 col-sm-4 col-md-12 text-center" data-interaction="single-posts-sidebar-trending-1">
                                        <div class="col-xs-12 category-subcategory-post-image">
                                            <a href="<?php echo esc_url( get_permalink() )?>" class="" data-interaction="single-posts-sidebar-trending-1-thumbnail">
                                                <div class="four_by_six">
                                                    <?php
                                                    if ( has_post_thumbnail() ) {
                                                        the_post_thumbnail( 'post');
                                                    }
                                                    ?>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 single-posts-trending-content">
                                            <time class="posts-block-datetime time-stamp-text text-timestamp"><?=time_ago();?></time>

                                            <a href="<?php echo esc_url( get_permalink() )?>" class="photo-essay-article-content-title" data-interaction="single-posts-sidebar-trending-1-title">
                                                <?php the_title();?>
                                            </a>


                                            <span class="posts-block-author author-name-text text-author">
                                                 <?php
                                                 echo the_author_meta('display_name', $recent['post_author']);
                                                 ?>
                                            </span>
                                        </div>
                                    </div>
                                    <?php
                                            endwhile;
                                        endif;
                                    wp_reset_postdata();
                                    ?>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <h3>RECOMMENDED CONTENT</h3>
                                </div>

                                <div class="col-xs-12 trending-block-popular single-posts-trending-inner text-center" data-interaction="single-posts-sidebar-recommended-content-wrapper">

                                    <?php
                                    $queryObject = new  Wp_Query( array(
                                        'showposts' => 3,
                                        'meta_key' => 'post_views_count',
                                        'orderby' => 'meta_value_num',
                                        'post_status' => 'publish',
                                        'date_query'    => array(
                                            'column'  => 'post_date',
                                            'after'   => '- 365 days'
                                        )
                                    ));

                                    if ( $queryObject->have_posts() ) :
                                        while ( $queryObject->have_posts() ) :
                                        $queryObject->the_post();

                                    ?>
                                    <div class="col-xs-12 col-sm-4 col-md-12 text-center trending-block-popular block-b at-blockb1" data-post-id="21662" data-interaction="single-posts-sidebar-recommended-content-1">
                                        <div class="col-xs-12 category-subcategory-post-image">
                                            <a href="<?php echo esc_url(get_permalink());?>" class="" data-interaction="single-posts-sidebar-recommended-content-1-thumbnail">
                                                <div class="four_by_six">
                                                    <?php
                                                        if(has_post_thumbnail()):
                                                            the_post_thumbnail('post');
                                                        endif;
                                                    ?>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-xs-12 single-posts-trending-content">
                                            <time class="posts-block-datetime time-stamp-text text-timestamp"><?php echo time_ago();?></time>

                                            <a href="<?php echo esc_url(get_permalink());?>" data-post-id="21662" class="photo-essay-article-content-title" data-interaction="single-posts-sidebar-recommended-content-1-title">
                                                <?php the_title(); ?>
                                            </a>


                                            <span class="posts-block-author author-name-text text-author">
                                                <?php
                                                echo the_author_meta('display_name', $recent['post_author']);
                                                ?>
                                            </span>
                                        </div>
                                    </div>
                                    <?php

                                        endwhile;
                                    endif;
                                    wp_reset_postdata();
                                    ?>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </div>

    <div class="single-posts-recommended hidden-xs hidden-sm">

        <div class="container">

            <?php echo do_shortcode("[recommended-content]"); ?>

        </div>
    </div>

<?php

//echo do_shortcode("[footer_image_navigation]");

get_footer();
?>


    <!-- I'm not Robot txt please include this into Form for Robot-->
<!--    <div class="g-recaptcha" data-sitekey="6LcT1oQUAAAAABBe-jz9dSYaozecwM6LfsuRzDjA"></div>-->

</div>