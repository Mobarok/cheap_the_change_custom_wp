<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/19/2018
 * Time: 10:00 PM
 *
 * Template Name:Privacy Policy
 */

    get_header();
    
?>
<div class="page-container default-page">

    <div class="row flex-row">
        <div class="container flex-container">
            <p><span style="font-weight: 400;">Effective Date: December 7, 2018</span></p>
            <p style="text-align: center;"><b>Privacy Policy</b></p>
            <p><span style="font-weight: 400;">This website is owned and operated by CHEAP THE CHANGE D/B/A CHEAP THE CHANGE. References to “</span><b>us</b><span style="font-weight: 400;">,” </span><b> </b><span style="font-weight: 400;">“</span><b>we,</b><span style="font-weight: 400;">” “</span><b>our</b><span style="font-weight: 400;">,” or “</span><b>Cheap The Chnage</b><span style="font-weight: 400;">” refer to Cheap The Chnage and its parents, subsidiaries, divisions, successors, and assigns, if any.  This Privacy Policy applies to all information collected by us via any method or platform. This Privacy Policy also applies to all information collected by us and by third-parties and applications you interact with on the website </span><a href="http://www.cheapthechange.com/"><span style="font-weight: 400;">www.cheapthechange.com</span></a><span style="font-weight: 400;"> (“Site”), and includes collection of information on, by or through all other services we provide, and all of the associated content, functionalities and advertising, on all platforms, whether via computer, mobile, e-reader, tablet or other device used to access the Site or to participate in any short message service (“SMS”) program or other program, contest, sweepstakes, campaign, or service hosted by us (“Campaign”) (collectively, all of the foregoing, including the Site, are the “Services”).*</span> <b>Regardless of how you access or log-in to the Services, </b><b>by accessing, logging-in or using Cheap The Chnage, utilizing, participating in, or opting in to any Campaign, you agree to this Privacy Policy and our </b><a href="https://www.cheapthechange.com/terms-of-use/"><b>Terms of Service</b></a><b>. </b></p>
            <p><span style="font-weight: 400;">We take your privacy seriously and we are committed to protecting it through our compliance with this policy. </span></p>
            <p><span style="font-weight: 400;">Do not provide information about others unless you are authorized to do so and consent to have all information used, disclosed, and transferred in accordance with this Privacy Policy.</span></p>
            <p style="text-align: center;"><b>INFORMATION WE COLLECT FROM YOU</b></p>
            <p><span style="font-weight: 400;">We collect information from you when you use the Services. The information we collect from you includes: (1) information you provide directly to us; (2) information we may collect automatically, such as through cookies; and (3) other information, such as information about your device, location information, information from social networking services, and information from other sources. The information we collect may include information that personally identifies you or can be used alone or combined with other information to identify you as an individual (</span><b>“personal information”</b><span style="font-weight: 400;">). Examples of personal information include, but are not limited to, your name, your physical or email address, and your date of birth. Please note that this policy may still apply if you elect private browsing.</span></p>
            <p style="text-align: left;"><b>Information You Provide Directly to Us</b></p>
            <p><span style="font-weight: 400;">We may collect and store information you provide to us directly, including information you provide to us when you register for the Services, like your name, username, and password; records and copies of your correspondence to us (including email addresses); your responses to surveys that we might ask you to complete; your search queries on the Site; SMS or text messages you send as a part of a Campaign; information you post to message boards, in chat areas; and information as otherwise described to you at the point of collection or pursuant to your consent.</span></p>
            <p><b>Information Collected Through Automatic Data Collection Technologies</b><span style="font-weight: 400;">  </span></p>
            <p><span style="font-weight: 400;">As you navigate through and interact with our Services, we, and our third-party service providers,vendors, and advertisers may use automatic data collection technologies to collect:</span></p>
            <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Details of your visits to our Site, including traffic pattern data, location data, logs, and other communication data and the resources that you access and use with our Services.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Information about your computer and internet connection, including your Internet Protocol (IP) address, operating system, and browser type.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Geolocation information.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Information about your interactions with email messages, such as the links clicked on and whether the messages are opened or forwarded.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Demographic information that is about you individually.</span></li>
            </ul>
            <p><span style="font-weight: 400;">We also may use these technologies to collect information about your online activities over time and across third-party websites or other online services (behavioral tracking). For more information on how you can opt out of behavioral tracking, please see </span><span style="font-weight: 400;"><a href="#YOURCHOICESTARGET"> Your Choices About Your Information</a></span><span style="font-weight: 400;">. </span></p>
            <p><span style="font-weight: 400;">We may aggregate automatically collected information with personal information we collect in other ways or receive from third parties. This information helps us to improve our Services and to deliver a better and more personalized service, including by enabling us to:</span></p>
            <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Estimate our audience size and usage patterns.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Customize our Services according to your individual interests.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Speed up your searches.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Recognize you when you use our Services.</span></li>
            </ul>
            <p><span style="font-weight: 400;">The technologies we use for this automatic data collection may include, but are not limited to:</span></p>
            <ul>
                <li style="font-weight: 400;">
                    <b>Cookies (or browser cookies)</b><span style="font-weight: 400;">. A cookie is a small file placed on the hard drive of your computer. You may refuse to accept browser cookies by activating the appropriate setting on your browser. However, if you select this setting you may be unable to access certain parts of our Services. Unless you have adjusted your browser setting so that it will refuse cookies, our system may issue cookies when you direct your browser to our Site and to certain other platforms for our Services. </span>
                </li>
                <li style="font-weight: 400;">
                    <b>Flash Cookies</b><span style="font-weight: 400;">. Certain features of our Services may use local stored objects (or Flash cookies) to collect and store information about your preferences and navigation to, from, and on our Site and to certain other platforms for our Services. Flash cookies are not managed by the same browser settings as are used for browser cookies. </span>
                </li>
                <li style="font-weight: 400;">
                    <b>Web Beacons</b><span style="font-weight: 400;">. The Services, pages of our Site and our emails may contain small electronic files known as web beacons (also referred to as clear gifs, pixel tags, and single-pixel gifs) that permit us, for example, to count users who have visited those pages or opened an email and for other related Services statistics (for example, recording the popularity of certain content and verifying system and server integrity). </span>
                </li>
                <li style="font-weight: 400;">
                    <b>Google Analytics</b><span style="font-weight: 400;">. We use third-party service providers for web analytic services, such as Google Analytics, with our Services, to provide us with statistics and other information about the Services and visitors to the Site and to certain other platforms for our Services. </span>
                </li>
                <li style="font-weight: 400;">
                    <b>Embedded Scripts</b><span style="font-weight: 400;">. An embedded script is programming code designed to collect information about your interactions with the Services. It is temporarily downloaded on your device from our web server or a third-party service provider with whom we work, is active only while you are connected to the Services, and deleted or deactivated thereafter.</span>
                </li>
                <li style="font-weight: 400;">
                    <b>Location</b><span style="font-weight: 400;">–</span><b>identifying Technologies</b><span style="font-weight: 400;">. We use GPS (global positioning systems) software, geo-filtering, and other location-aware technologies for purposes such as verifying your location and delivering or restricting relevant content based on your location. </span>
                </li>
                <li style="font-weight: 400;">
                    <b>Device Fingerprinting</b><span style="font-weight: 400;">. Device fingerprinting is the process of analyzing and combining sets of information elements from your device’s browser, such as JavaScript objects and installed fonts, in order to create a “fingerprint” of your device and uniquely identify your device and applications.</span>
                </li>
            </ul>
            <p><span style="font-weight: 400;">For further information on tracking technologies and your choices regarding them, please see </span><span style="font-weight: 400;"><a href="#YOURCHOICESTARGET">Your Choices About Your Information</a></span><span style="font-weight: 400;">.</span></p>
            <p style="text-align: center;"><b>HOW WE SECURE YOUR INFORMATION</b></p>
            <p><span style="font-weight: 400;">We have implemented measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration, and disclosure. All non-public information you provide to us that we store is stored on secure servers behind firewalls and encrypted at-rest, as well as encrypted in transit via SSL. However, the transmission of information via the internet is not completely secure. Although we do our best to protect your personal information, we cannot guarantee the security of your personal information transmitted to our Site or through the Services or as part of our Campaigns. Any transmission of personal information, or otherwise, is at your own risk. We are not responsible for circumvention of any privacy settings or security measures contained on the Site or within the Services.  </span></p>
            <p style="text-align: center;"><b>YOUR INFORMATION AND THIRD-PARTIES</b></p>
            <p><span style="font-weight: 400;">We enable third parties to collect personal information, user information, de-identified user information, or encrypted, non-human readable user information (such as, but not limited to hashed email addresses, desktop or mobile device identifiers, network cache information, network connection information, network session information, browser-level user agent information, device IP information, and/or other data) collected from devices of end users of the Services, for targeted advertising purposes through a variety of technologies and methods.</span></p>
            <p><b>Third-Party Use of Cookies and Other Tracking Technologies</b></p>
            <p><span style="font-weight: 400;">Some Services, including advertisements, and some Campaigns, are served by third parties, including advertisers, ad networks and servers, content providers, and application providers. Please be aware that we are not responsible for the privacy practices of other websites or third parties, and we do not control these third parties’ tracking technologies or how they may be used. By linking to such websites or using such third parties’ services, we are not endorsing their content or practices. We encourage you to be aware when you access any such third-party links and to read the privacy policies of each website you access. If you have any questions about an advertisement or other targeted content, you should contact the responsible provider directly. For information about how you can opt out of receiving targeted advertising from many providers, see </span><span style="font-weight: 400;"><a href="#YOURCHOICESTARGET">Your Choices About Your Information</a></span><span style="font-weight: 400;">. </span></p>
            <p><b>Information We Collect from Others</b></p>
            <p><span style="font-weight: 400;">We may also collect personal information about you, your online activities, and your interactions with us, our affiliates, or others. We may receive additional information about you, from sources such as advertisers who advertise on our site, our affiliates and vendors, and other commercial sources and third parties. We may also receive information from third parties that identifies your interactions with our Services across multiple devices.</span></p>
            <p><span style="font-weight: 400;">We may combine all of the personal information and non-personal information we collect or receive about you and use or disclose it in the manner described in this Privacy Policy, including to help us better tailor content, advertising, and offers to your interests and needs.</span></p>
            <p><b>Social Networking Services</b></p>
            <p><span style="font-weight: 400;">We work with third-party social media providers to offer you their social networking services through the Services. For example, you can use third-party social networking services to share our content with your friends and followers on those social networking services. These social networking services may be able to collect information about you, while using our Services. These third-party social networking services also may notify your friends about your use of the Services, in accordance with applicable law and their own privacy policies. If you choose to make use of third-party social networking services, we may receive information about you that you have made available to those social networking service, including information about your contacts on those social networking services. </span></p>
            <p style="text-align: center;"><b>HOW WE USE YOUR INFORMATION</b></p>
            <p><span style="font-weight: 400;">We use information that we collect about you or that you provide to us, including any personal information:</span></p>
            <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">To present our Services and to otherwise carry out, evaluate, market, analyze, personalize and improve our Services, which may include storing your information and providing access and sharing of your information with our agents, vendors, suppliers, affiliates, third-party service providers, advertisers and other third parties.</span></li>
                <li style="font-weight: 400;">
                    <span style="font-weight: 400;">To administer Campaigns that utilize automatic</span> <span style="font-weight: 400;">telephone dialing systems, but only if you’ve opted in. When you sign up to participate in a Campaign, the information you provide may also be provided to our Campaign advertisers, operators, and sponsors, and the use of that information will be governed by those advertisers’, operators’, and sponsors’ privacy policies or practices.</span>
                </li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">To contact you via email and otherwise about the Services, your account, events, services or products that we think might be relevant or of interest to you.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">In any other way we may describe when you provide the information.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">For any other purpose for which you provided it.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">For any other purpose stated in this Privacy Policy. </span></li>
            </ul>
            <p><span style="font-weight: 400;">We may also use your information to contact you about our own and third-parties’ goods and services that may be of interest to you. If you do not want us to use your information in this way, you can opt out via the manner specified in the correspondence. For more information, see </span><span style="font-weight: 400;"><a href="#YOURCHOICESTARGET">Your Choices About Your Information</a></span><span style="font-weight: 400;">.</span></p>
            <p><span style="font-weight: 400;">We may disclose non-personal information about your use of the Services, and information that does not identify any individual, without restriction. Such information may include aggregate or anonymized website usage data or demographic reports.</span></p>
            <p><span style="font-weight: 400;">We may disclose personal information and any other information that we collect or you provide as described in this privacy policy:</span></p>
            <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">To our subsidiaries and affiliates.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">To contractors, agents, vendors, suppliers, advertisers, service providers, third-party data providers, second-party consortiums, and other third parties we do business with, or which we use to support our business, to use in performing certain functions or services on our behalf, conducting analyses, sending communications from us, including as a part of a Campaign, or marketing directly to you. </span></li>
                <li style="font-weight: 400;">
                    <span style="font-weight: 400;">To third parties to market their products or services to you if you have not opted out of these marketing messages. Please note that messages delivered from a third party will subject you to the third party’s privacy policy. For more information, see </span><span style="font-weight: 400;"><a href="#YOURCHOICESTARGET">Your Choices About Your Information</a></span><span style="font-weight: 400;">.</span>
                </li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all of our assets, whether as a going concern or as part of bankruptcy, liquidation, or similar proceeding, in which personal information about our Services is among the assets transferred.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">To fulfill the purpose for which you provide it. For example, if you give us an email address to use the “email a friend” feature of our Services, we will transmit the contents of that email and your email address to the recipients.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">For any other purpose disclosed by us when you provide the information.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">With your consent.</span></li>
            </ul>
            <p><span style="font-weight: 400;">We reserve the right, and you hereby expressly authorize us, to disclose your personal information:</span></p>
            <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">To comply with any subpoena, court order, or legal process, including to respond to any government or regulatory request.</span></li>
                <li style="font-weight: 400;">
                    <span style="font-weight: 400;">To enforce or apply our </span><a href="https://www.cheapthechange.com/terms-of-use/"><span style="font-weight: 400;">Terms of Service</span></a><span style="font-weight: 400;">.</span>
                </li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">If we believe disclosure is necessary or appropriate to protect our rights and property or to prevent physical harm or financial loss, or in connection with an investigation of suspected or actual illegal activity.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">To a parent company, subsidiaries, joint ventures, or other companies under common control with us (in which case we will require such entities to honor this Privacy Policy).</span></li>
            </ul>
            <p id="YOURCHOICESTARGET" style="text-align: center;"><b>YOUR CHOICES ABOUT YOUR INFORMATION</b></p>
            <p><span style="font-weight: 400;">We strive to provide you with choices regarding the personal information you provide to us. We have created mechanisms to provide you control over your information: </span></p>
            <ul>
                <li style="font-weight: 400;">
                    <b>Your Information</b><span style="font-weight: 400;">. You may review and update certain member profile information by logging in to the relevant portion of the Services where such information may be updated. </span>
                </li>
                <li style="font-weight: 400;">
                    <b>Unsubscribe from Emails</b><span style="font-weight: 400;">. To unsubscribe from a particular newsletter, click the “unsubscribe” link at the bottom of that email newsletter. When we send newsletters to subscribers, we may allow advertisers or partners to include messages in those newsletters, or we may send sponsored or dedicated messages on behalf of those advertisers or partners. We may disclose your opt-out choices to third parties so that they can honor your preferences in accordance with applicable laws.</span>
                </li>
                <li style="font-weight: 400;">
                    <b>Opt-out from SMS messages. </b><span style="font-weight: 400;">To stop receiving SMS messages after you’ve signed up for a Campaign, just reply “</span><b>STOP</b><span style="font-weight: 400;">” to opt out and cancel the service.  After you send the message “</span><b>STOP</b><span style="font-weight: 400;">” to us, we will send you a reply message to confirm that you have been unsubscribed. After this, you will no longer receive messages from us. If you want to join again, just sign up as you did the first time and we will start sending messages to you again.</span>
                </li>
                <li style="font-weight: 400;">
                    <b>Tracking Technologies and Advertising</b><span style="font-weight: 400;">. You can set your browser to refuse all or some browser cookies, or to alert you when cookies are being sent. To learn how you can manage your Flash cookie settings, visit the Flash player settings page on Adobe’s website. If you disable or refuse cookies, please note that some parts of the Services may then be inaccessible or not function properly.</span>
                </li>
                <li style="font-weight: 400;">
                    <b>Disclosure of Your Information for Third-Party Advertising</b><span style="font-weight: 400;">. If you do not want us to share your personal information with unaffiliated or non-agent third parties for promotional purposes, you can opt out in the manner specified in the correspondence.</span>
                </li>
            </ul>
            <p><b>Internet Based  Advertising (IBA)</b><span style="font-weight: 400;">.  You can exercise your online advertising choices at </span><a href="http://optout.aboutads.info"><span style="font-weight: 400;">http://optout.aboutads.info</span></a><span style="font-weight: 400;"> and </span><a href="http://www.networkadvertising.org"><span style="font-weight: 400;">www.networkadvertising.org</span></a><span style="font-weight: 400;"> You may also opt out of receiving IBA from many sites through the Network Advertising Initiative (“NAI”) at the </span><a href="https://www.networkadvertising.org/"><span style="font-weight: 400;">NAI Service</span></a><span style="font-weight: 400;">, and the Digital Advertising Alliance (“</span><b>DAA</b><span style="font-weight: 400;">”) at the </span><a href="http://www.aboutads.info/"><span style="font-weight: 400;">DAA consumer choice service</span></a><span style="font-weight: 400;">.  The tools provided on the </span><a href="http://optout.aboutads.info/#!/"><span style="font-weight: 400;">DAA opt-out page</span></a><span style="font-weight: 400;"> and  </span><a href="http://optout.networkadvertising.org/#!/"><span style="font-weight: 400;">NAI opt-out page</span></a><span style="font-weight: 400;"> are provided by third parties, and not us. We do not control or operate these tools or the choices that advertisers and others provide through these tools. Some of our third-party service providers require us to specifically list their opt-out links, and those links, as well as as opt-out links for other providers we may use, are listed below. When you opt out, you may receive an “opt-out” cookie so that the network will know not to assign you new cookies in the future. You will continue to receive ads, but not behaviorally targeted ads. If you erase your browser’s cookies, you may need to perform this process again. We are not responsible for the effectiveness of, or compliance with, any third parties’ opt-out options or programs or the accuracy of their statements regarding their programs.  </span></p>
            <p><span style="font-weight: 400;">Alexa</span> <a href="https://support.alexa.com/hc/en-us/articles/200685410-Opting-Out-of-Alexa-Measurement-Pixel"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Double Click</span> <a href="http://www.doubleclick.com/privacy/faq.aspx"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Facebook</span> <a href="https://www.facebook.com/help/568137493302217"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Google</span> <a href="http://www.google.com/privacy_ads.html"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Google Analytics </span> <a href="https://tools.google.com/dlpage/gaoptou"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Google Retargeting</span> <a href="http://www.google.com/settings/ads"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Parsley</span> <a href="http://srv.config.parsely.com/optout"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Piwik</span> <a href="https://piwik.org/privacy/"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Scorecardresearch </span> <a href="http://www.scorecardresearch.com/preferences.aspx"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Taboola</span> <a href="https://www.taboola.com/privacy-policy"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Quantcast </span> <a href="https://www.quantcast.com/opt-out/"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p><span style="font-weight: 400;">Yahoo</span> <a href="https://aim.yahoo.com/aim/us/en/optout/index.htm"><span style="font-weight: 400;">Opt-Out Link</span></a></p>
            <p style="text-align: center;"><b>NOTICES, DISCLOSURES, AND ADDITIONAL INFORMATION</b></p>
            <p><b>Your California Privacy Rights</b></p>
            <p><span style="font-weight: 400;">Under California’s “Shine the Light” law, California residents are entitled to request and obtain from us once a calendar year information about the customer information we shared, if any, with other businesses for their own direct marketing uses. If applicable, this information would include the categories of customer information and the names and addresses of those businesses with which we shared customer information for the immediately prior calendar year.</span></p>
            <p><span style="font-weight: 400;">To obtain this information from us, please contact us at </span><a href="/cdn-cgi/l/email-protection#ed81888a8c81838299848e889ead8184889e889f9e868c8b8bc38e8280"><span style="font-weight: 400;"><span class="__cf_email__" data-cfemail="48242d2f292426273c212b2d0824212d3b2d3a3b23292e2e662b2725">[email&#160;protected]</span></span></a><span style="font-weight: 400;">. You must put “Your California Privacy Rights” in the body or in the subject line of your message. We will provide the requested information to you at your email address in response. Please note that we will not accept inquiries via the telephone, email, or by facsimile, and we are not responsible for notices that are not labeled or sent properly, or that do not have complete information. </span></p>
            <p><span style="font-weight: 400;">Please be aware that not all information sharing is covered by the “Shine the Light” requirements and only information on covered sharing will be included in our response.</span></p>
            <p><b>How We Respond to “Do Not Track” Signals</b></p>
            <p><span style="font-weight: 400;">Some web browsers may transmit “do not track” signals to online services you visit. California Business and Professions Code Section 22575(b) (as amended effective January 1, 2014) provides that California residents are entitled to know how we respond to “do not track” browser settings. Because there currently is no industry standard concerning what, if anything, websites should do when they receive such signals, we currently do not take action in response to these signals. For more information on “do not track,” please visit </span><a href="http://www.allaboutdnt.com"><span style="font-weight: 400;">http://www.allaboutdnt.com</span></a><span style="font-weight: 400;">. </span></p>
            <p><b>International Users</b></p>
            <p><span style="font-weight: 400;">Please do not visit Cheap The Chnage or use the Services from outside North America; the Services are not intended to be accessed from outside the North America and we may restrict access to the Services from other countries or regions. Please be aware that the information we collect through the Services will be transferred to and stored on our servers in the North America. By using the Services, you acknowledge and consent to the transfer and processing of your personal information in the United States as described in this Privacy Policy. Please be aware that the data protection laws and regulations applicable to your personal information transferred to the United States may be different from the laws in your country of residence.</span></p>
            <p><b>Children Under the Age of 13</b></p>
            <p><span style="font-weight: 400;">The Services are not intended for children under 13 years of age and we do not knowingly collect personal information from children under 13. If we learn we have collected or received personal information from a child under 13 without verification of parental consent, we will delete that information. If you believe we might have any information from or about a child under 13, please contact us at </span><a href="/cdn-cgi/l/email-protection#53353636373132303813273b3623363d3d2a3b3c32213736217d303c3e"><span style="font-weight: 400;"><span class="__cf_email__" data-cfemail="20464545444241434b6054484550454e4e59484f41524445520e434f4d">[email&#160;protected]</span></span></a><span style="font-weight: 400;">.</span></p>
            <p><b>Modifications to This Privacy Policy </b></p>
            <p><span style="font-weight: 400;">We reserve the right to change this Privacy Policy at any time by posting revisions on Cheap The Chnage. Such changes will be effective upon posting. Your continued use of the Services after we make changes is deemed to be acceptance of those changes, so please check the policy periodically for updates.</span></p>
            <p><b>Contact Information</b></p>
            <p><span style="font-weight: 400;">To ask questions or comment about this privacy policy and our privacy practices, contact us at </span><a href="/cdn-cgi/l/email-protection#4d2b2828292f2c2e260d3925283d2823233425222c3f29283f632e2220"><span style="font-weight: 400;"><span class="__cf_email__" data-cfemail="8fe9eaeaebedeeece4cffbe7eaffeae1e1f6e7e0eefdebeafda1ece0e2">[email&#160;protected]</span></span></a><span style="font-weight: 400;">.</span></p>
            <p>*SMS messaging is also commonly referred to as “text messaging” or “texting.”</p>
        </div>
    </div>
</div>


<?php

    get_footer();

?>

