<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/11/2019
 * Time: 2:46 PM
 *
 * Template Name:Amazon Jacket/Spine Calculation
 */


get_header();
?>
<style>
   input[type=text] { height: 30px !important; line-height: 28px !important; padding: 4px; border: 1px solid #707070; font-size: 14px; color: #707070; text-align: center; display: inline-block !important;}
    input[type=submit] { width: 70px; }

    /*hr { background: rgba(255,255,255,0.3); height: 1px; border: none; margin: 20px 0; }*/

    #stats {
        width: 60%;
        float: left;
        padding: 50px 25px;
        box-shadow: 0 0 7px 0 rgba(0,0,0,.2);
        min-height: 430px;
    }

    .book_details {
        width: 36%;
        float: left;
        padding: 20px;
        border-left: solid 2px #fff;
        box-shadow: 0 0 7px 0 rgba(0,0,0,.2);
        margin-left: 4%;
        min-height: 430px;
    }
    @media only screen and (max-width: 600px) {

        #stats,
        .book_details { width: 100%; float: none; margin: 0 0 10px 0; border: none;}
    }

</style>

<div class="page-container category-page">

    <div class="row breadcrumbs flex-row">
        <div class="container flex-container">
            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">

                <span property="itemListElement" typeof="ListItem">
                    <a property="item" typeof="WebPage" title="Go to Cheap The Change" href="<?php bloginfo('home')?>" class="home">
                        <span property="name">Home</span>
                    </a>
                    <meta property="position" content="1"></span>
                <span>&gt;</span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name">
                        <?php
                        echo get_the_title($post->ID);
                        ?></span>
                    <meta property="position" content="2"></span>
            </div>
        </div>
    </div>

    <div class="row category-trending  net-worth-page flex-row">
        <div class="container flex-container">
            <div id="fbuilder">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 " id="fbuilder_1" data-original-title="" title="">
                    <div id="formheader_1"><div class="fform" id="field">
                            <h1>Amazon CreateSpace.com Jacket/Spine Calculator</h1>
                            <span class="cff-form-description">Use the following form to get pixel dimensions for PhotoShop and other design programs. Simply put in your page count, book interior trim and paper color and your jacket cover size will instantly appear at the bottom with all the details.</span>
                        </div>

                        <hr class="main-vertical-block-separator hidden-sm">
                    </div>
                    <div class="col-md-12 col-lg-12">

<div style="margin: 40px auto 0 auto; max-width: 100%; ">

    <div id="stats">

        <p style="padding-bottom: 20px;">
            <span style="white-space:nowrap;"><b>Page Count: </b> <input type="text"  class="form-control page_count" name="page_count" style="width: 100px;" autofocus="autofocus"></span>
        </p>

        <p class="sizes" style="line-height: 1.7em;">
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size" checked data-width="5.5" data-height="8.5" id="size_5.5x8.5"> <label for="size_5.5x8.5">5.5x8.5</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="5.06" data-height="7.81" id="size_5.06x7.81"> <label for="size_5.06x7.81">5.06x7.81</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="5" data-height="8" id="size_5x8"> <label for="size_5x8">5x8</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="5.25" data-height="8" id="size_5.25x8"> <label for="size_5.25x8">5.25x8</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="6" data-height="9" id="size_6x9"> <label for="size_6x9">6x9</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="6.14" data-height="9.21" id="size_6.14x9.21"> <label for="size_6.14x9.21">6.14x9.21</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="6.69" data-height="9.61" id="size_6.69x9.61"> <label for="size_6.69x9.61">6.69x9.61</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="7" data-height="10" id="size_7x10"> <label for="size_7x10">7x10</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="7.44" data-height="9.69" id="size_7.44x9.69"> <label for="size_7.44x9.69">7.44x9.69</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="7.5" data-height="9.25" id="size_7.5x9.25"> <label for="size_7.5x9.25">7.5x9.25</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="8" data-height="10" id="size_8x10"> <label for="size_8x10">8x10</label>
			</span>
            <span style="white-space:nowrap; padding: 10px;">
			<input type="radio" name="size"data-width="8.5" data-height="11" id="size_8.5x11"> <label for="size_8.5x11">8.5x11</label>
			</span>
        </p>
        <p style="padding-bottom: 20px;">
			<span style="white-space:nowrap;">
			Width:
			<input type="text" class="form-control width" name="width" value="5.5" style="width: 100px;">
			</span>
            &nbsp; &nbsp;
            <span style="white-space:nowrap;">
			Height:
			<input type="text" class="form-control height" name="height" value="8.5"  style="width: 100px;"></span>
        </p>

        <p class="type" style="line-height: 1.7em;">
            <b>Paper Color: </b>
            <span style="white-space:nowrap; padding: 5px;"><input type="radio" name="type" data-inches=".0025" id="type_cream" checked> <label for="type_cream">Cream</label></span>
            <span style="white-space:nowrap; padding: 5px;"><input type="radio" name="type" data-inches=".002252" id="type_white"> <label for="type_white">White</label></span>
            <span style="white-space:nowrap; padding: 5px;"><input type="radio" name="type" data-inches=".002347" id="type_color"> <label for="type_color">Full-Color</label></span>
        </p>

        <p style="padding-bottom: 30px;">
            <b>DPI: </b> <input type="text" class="form-control dpi" name="dpi" style="width: 100px;" value="300" autofocus="autofocus">
        </p>

    </div>

    <div class="book_details">
        <b> File Details </b><br><br>

        File Width: <span class="file_width"></span><br>
        File Height: <span class="file_height"></span><br>
        <hr>
        Spine Left: <span class="spine_left"></span><br>
        Spine Right: <span class="spine_right"></span><br>
        <hr>
        Bleed Left: <span class="bleed_left"></span><br>
        Bleed Right: <span class="bleed_right"></span><br>
        Bleed Top: <span class="bleed_top"></span><br>
        Bleed Bottom: <span class="bleed_bottom"></span>
    </div>

    <div class="clr"></div>
</div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
<script type="text/javascript">

    var dpi = 300;
    var page_count = 0;
    var bleed = .125;
    var book_width = 5.5;
    var book_height = 8.5;
    var spine_inches = .0025;


    function createspace_calc()
    {
        if( !page_count )
        {
            $(".book_details span").html('');
            return;
        }

        dpi = parseInt($(".dpi").val());

        var full_width = Math.round(((bleed + book_width + ( page_count * spine_inches ) + book_width + bleed) * dpi).toFixed(0));
        var full_height = Math.round(((bleed + book_height + bleed) * dpi).toFixed(0));

        // FILE
        $(".file_width").html( full_width + "px" );
        $(".file_height").html( full_height + "px" );

        // SPINE
        $(".spine_left").html( Math.round(((bleed + book_width) * dpi).toFixed(0)) + "px" );
        $(".spine_right").html( Math.round(((bleed + book_width + ( page_count * spine_inches )) * dpi).toFixed(0)) + "px" );

        // BLEED
        $(".bleed_top").html( Math.round((bleed * dpi).toFixed(0)) + "px" );
        $(".bleed_right").html( Math.round((full_width - (bleed * dpi)).toFixed(0)) + "px" );
        $(".bleed_bottom").html( Math.round((full_height - (bleed * dpi)).toFixed(0)) + "px" );
        $(".bleed_left").html( Math.round((bleed * dpi).toFixed(0)) + "px" );
    }

    $(document).ready(function() {

        $("body").delegate('.page_count', "keyup change", function() { page_count = $(this).val(); createspace_calc(); });
        $("p.type input").change(function() { spine_inches = $(this).data('inches'); createspace_calc(); });

        $(".dpi").change(createspace_calc);
        $(".dpi").keyup(createspace_calc);

        $("p.sizes input[type=radio]").change(function() {

            $(".width").val( $(this).data('width') );
            $(".height").val( $(this).data('height') );

            book_width = $(this).data('width');
            book_height = $(this).data('height');
            createspace_calc();

        });

        $(".width, .height").keyup(function() {
            $("p.sizes input[type=radio]").prop('checked', false);

            book_width = parseInt($(".width").val());
            book_height = parseInt($(".height").val());

            createspace_calc();
        });

    });


</script>

<?php
//echo do_shortcode("[footer_image_navigation]");
?>
</div>

<?php
get_footer();
?>

