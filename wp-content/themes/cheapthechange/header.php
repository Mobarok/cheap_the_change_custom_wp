<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/15/2018
 * Time: 11:36 PM
 */
?>
<!DOCTYPE html>
<html lang="<?php language_attributes(); ?>">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/png" href="<?php echo esc_url(get_template_directory_uri()); ?>/favicon.PNG"/>

    <script src="https://cdn.optimizely.com/js/8202772301.js"></script>


	<meta name="description" content="<?php bloginfo('description'); ?>"/>

	<link rel='stylesheet' id='cheap-ovo-font-css' href='https://fonts.googleapis.com/css?family=Shrikhand%7CMontserrat%7CArimo%7COvo%7CRoboto+Condensed%3A400%2C700%7CRoboto%3A200%2C300%2C400%2C700%7CCrimson+Text%3A400%2C400i%7CCoustard&amp;ver=c396be2c130d6542109fb75ca7788b0d' type='text/css' media='all'/>
	<link rel='stylesheet' id='cheap-fontawesome-style-css' href='<?php echo esc_url(get_template_directory_uri()); ?>/assets/css/font-awesome.minbb49.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='cheap-bootstrap-style-css' href='<?php echo esc_url(get_template_directory_uri()); ?>/assets/css/bootstrap.min8baf.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='cheap-style-css' href='<?php bloginfo('stylesheet_url');?>' type='text/css' media='all'/>
	<link rel='stylesheet' id='cheap-style-child-css' href='<?php echo esc_url(get_template_directory_uri()); ?>/style-child.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='jquery-ui-css' href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.min.css' type='text/css' media='all'/>

	<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>
	<script type='text/javascript' src='<?php echo esc_url(get_template_directory_uri()); ?>/assets/js/jquery/jquery-migrate.min330a.js'></script>
	<script type='text/javascript' src='<?php echo esc_url(get_template_directory_uri()); ?>/assets/js/tph-utilities8448.js'></script>

	<style data-rel="dev_custom_css_head">@media (min-width:479px){.postid-100405 .post-page .single-post-content .single-post-content-inner ul:not(.single-social) li:before{display:none!important}}</style>


	<script data-rel="dev_custom_js_head"></script>
	<script>
jQuery(document).ready(function($){var searchParams=new URLSearchParams(window.location.search)
	if(searchParams.has('s')&&jQuery('body').hasClass('error404')){var search_term=encodeURIComponent(searchParams.get('s'));var newURL=window.location.href.replace('?s='+search_term,'?')
	newURL=newURL.replace('&s='+search_term,'')
	newURL=newURL.replace('?&','?')
	window.location=newURL;}});
	</script>
    <!-- JS Code for On Load Add Class -->
    <script type="text/javascript">
        jQuery(function() {
            jQuery('.navbar-nav>li>a').addClass("text-menu menu-items-text");
        });
    </script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php wp_head();?>
</head>


<body  id="body" data-interaction="all-pages-page-body" <?php body_class(); ?>>

<nav class="navbar navbar-default true-white-color navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-navigation" aria-expanded="false">
                <div id="navbar-hamburger">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </div>
                <div id="navbar-close" class="hidden">
                    &nbsp;
                </div>
            </button>
            <a class="at-brand navbar-brand" href="<?php bloginfo('home');?>">
                <img alt="Cheap The Change" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/logo.png">
            </a>
        </div>




        <div class="collapse navbar-collapse navbar-absolute" id="mobile-navigation">
            <ul id="menu-navigation" class="nav navbar-nav">
                <li class="hidden-md hidden-lg mobile-search-form">
                    <form action="#" method="get" id="mobile-search" target="_top">
                        <div class="input-group input-group-sm">
                            <label for="mobile-search-term" cheight: 100vh;lass="sr-only">Email address</label>
                            <input type="text" name="s" id="mobile-search-term" class="form-control" placeholder="Search" aria-describedby="mobile-search-icon">
                            <span class="input-group-addon fa fa-search" id="mobile-search-icon"></span>
                            <input type="hidden" name="aff_id" value=/>
                            <input type="hidden" name="aff_sub2" value=homepage /></div>
                    </form>
                </li>

            </ul>

            <?php
            if(function_exists('wp_nav_menu')){
                wp_nav_menu( array(
                        'theme_location' =>'main-menu',
                        'menu'           => 'header_menu',
                        'depth'             => 2,
//                        'container'         => 'div',
//                        'container_class'   => 'collapse navbar-collapse navbar-absolute',
//                        'container_id'      => 'mobile-navigation',
                        'menu_class'        => 'nav navbar-nav',
                        'menu_id'        => 'menu-navigation',
                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                        'walker'            => new wp_bootstrap_navwalker())
                );
            }
            ?>

            <ul class="nav navbar-nav navbar-right">
                <li class="hidden-xs hidden-sm"><a class="social-icons" href="#">
                        <i class="at-navbar-search-icon fa fa-search fa-lg"></i>
                        <div class="desktop-search-form hidden-xs hidden-sm">
                            <form id="desktop-search" action="#" method="get" onclick="parsely_click_tracking('search');">
                                <div class="input-group">
                                    <label for="searchtop" class="sr-only">Search</label>
                                    <input id="searchtop" name="s" type="text" class="at-navbar-search-field form-control" placeholder="what are you searching for?">
                                    <input type="hidden" name="aff_id" value=/><input type="hidden" name="aff_sub2" value=homepage />
                                    <span class="input-group-btn">
                                        <button class="at-navbar-search-button btn btn-default" type="submit" onclick="parsely_click_tracking('perform-search');">Search</button>
                                   </span>
                                </div>
                            </form>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
