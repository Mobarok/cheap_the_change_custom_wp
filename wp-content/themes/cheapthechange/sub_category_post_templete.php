<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 12:13 AM
 *
 * Template Name:Sub Category Post Template
 */

    get_header();

?>
<div class="page-container category-page subcategory-page">


    <div class="row breadcrumbs flex-row">
        <div class="container flex-container">
            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">

                <span property="itemListElement" typeof="ListItem">
                    <a property="item" typeof="WebPage" title="Go to Cheap the Change" href="<?php bloginfo('home')?>" class="home">
                        <span property="name">Home</span>
                    </a>
                    <meta property="position" content="1">
                </span> <span>&gt;</span>
                <span property="itemListElement" typeof="ListItem">
                    <a property="item" typeof="WebPage" title="" href="
                    <?php
                        $parent_slug = get_post($post->post_parent);
                        $parent_slug = $parent_slug->post_name;
                        bloginfo('home');echo '/'.$parent_slug;
                    ?>" class="taxonomy category">
                        <span property="name">
                            <?php
                            // get the page name
                                $parent_title = get_the_title($post->post_parent);
                                echo $parent_title;

                            ?>
                        </span>
                    </a>
                    <meta property="position" content="2">
                </span> <span>&gt;</span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name">
                        <?php
                            echo get_the_title($post->ID);
                        ?>
                    </span>
                    <meta property="position" content="3">
                </span>
            </div>
        </div>
    </div>

            <?php
                echo do_shortcode("[single_top_post_by_category category='".$post->post_name."']");
            ?>


    <div class="row category-trending flex-row">
        <div class="container flex-container">
            <div class="col-xs-12 category-subcategory-posts category-trending-posts text-center">
                <?php
                    echo do_shortcode("[trending_post_show loop='2' category='".$post->post_name."']");

                ?>
            </div>
        </div>
    </div>



    <div class="row category-trending flex-row">
        <div class="container flex-container">
            <?php
                echo do_shortcode("[trending_post_show loop='7' class='1' category='".$post->post_name."']");

            ?>
        </div>
    </div>

    <?php
        echo do_shortcode("[footer_sign_up]");
//        echo do_shortcode("[footer_image_navigation]");

    ?>


    <?php
    get_footer();
?>
