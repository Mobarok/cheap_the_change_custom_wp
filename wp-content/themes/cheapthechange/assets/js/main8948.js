(function ($) {
    $(document).ready(function () {

        $('#mobile-navigation')
            .on('show.bs.collapse', function () {
                $('#navbar-hamburger').addClass('hidden');
                $('#navbar-close').removeClass('hidden');
                $('body').addClass('menu-open');

                if ($('li.current-menu-item').length) {
                    $('li.current-menu-item').addClass('open');
                }
                if ($('li.current-category-parent').not('.current-menu-item').length) {
                    $('li.current-category-parent').not('.current-menu-item').addClass('open');
                }
            })
            .on('hide.bs.collapse', function () {
                $('#navbar-hamburger').removeClass('hidden');
                $('#navbar-close').addClass('hidden');
                $('body').removeClass('menu-open');
                if ($('li.menu-item').not('.current-menu-item').not('.current-category-parent').length) {
                    $('li.menu-item').not('.current-menu-item').not('.current-category-parent').removeClass('open');
                }

                if ($('li.current-category-parent.current-menu-item').length) {
                    $('li.current-category-parent.current-menu-item').addClass('open');
                }
            });

        $('i.dropdown-activator').click(function (e) {
            e.preventDefault();
            $(this).parent().parent().toggleClass('open');
            //$(this).parent().parent().siblings().removeClass('open');

            if ($(window).width() < 992) {
                var offset = $(this).parent().parent().offset().top + $(this).parent().parent().height();
                if( offset > $(window).height() ) {
                    $('.navbar-collapse').animate({scrollTop: offset - $(window).height()}, 1000);
                }
            }
        })

        $('.navbar-default .navbar-nav.navbar-right a.social-icons i.fa-search').click(function (e) {
            e.preventDefault();
            if ($('.desktop-search-form').length) {
                $('.desktop-search-form').fadeToggle("fast", function () {
                    $(this).toggleClass('activeform');
                    $('.desktop-search-form input').focus();
                });
            }
        })

        if ($('.mobile-search-form').length) {
            $('.mobile-search-form').on('click', '#mobile-search-icon', function (e) {
                e.preventDefault();
                if ($("#mobile-search-term").val() != '') {
                    $('form#mobile-search').submit();
                } else {
                    $("#mobile-search-term").focus();
                }
            })
        }

        if ($('.row.main-vertical-block div .panel-heading a').length) {
            $('.row.main-vertical-block div .panel-heading a').hover(
                function () {
                    $(this).parent().addClass("active").siblings('.panel-body').addClass('active');
                }, function () {
                    $(this).parent().removeClass("active").siblings('.panel-body').removeClass('active');
                }
            );
        }

        var cnt = 2;
        // Load More button
        $('.btn-load-more').on('click', function (e) {
            e.preventDefault();
            if( cnt < 6 ) {
               $('div[data-placement-id="posts-block-' + cnt + '"]').toggle().fadeIn();
            }

            if( cnt >= 5 ) {
                $(this).attr('disabled', 'disabled').html('More Below');
            }

            cnt++;
        })

        var cnt_sub = 1;

        if( $( 'div.page-container' ).length ) {
            if ($('div.page-container').hasClass('subcategory-page')) {
                if (!$('.subcategory-load-1').length) {
                    $('.btn-sub-load-more').attr('disabled', 'disabled').html('More Below');
                }
            }
        }

        $('.btn-sub-load-more').on('click', function (e) {
            e.preventDefault();

            if( $( '.subcategory-load-' + cnt_sub ).length ) {
                $( '.subcategory-load-' + cnt_sub ).toggle().fadeIn();
            }

            var new_cnt = cnt_sub + 1;

            if( ! $( '.subcategory-load-' + new_cnt ).length ) {
                $(this).attr('disabled', 'disabled').html('More Below');
            }

            cnt_sub++;

        })


        var cnt_main = 1;

        if( $( 'div.page-container' ).length ) {
            if ($('div.page-container').hasClass('category-page')) {
                if (!$('.category-main-load-more-1').length) {
                    $('.btn-main-load-more').attr('disabled', 'disabled').html('More Below');
                }
            }
        }

        $('.btn-main-load-more').on('click', function (e) {
            e.preventDefault();

            if( $( '.category-main-load-more-' + cnt_main ).length ) {
                $( '.category-main-load-more-' + cnt_main ).toggle().fadeIn();
            }

            var new_main_cnt = cnt_main + 1;

            if( ! $( '.category-main-load-more-' + new_main_cnt ).length ) {
                $(this).attr('disabled', 'disabled').html('More Below');
            }

            cnt_main++;

        })

        $(window).scroll(function() {

            var scroll = $(window).scrollTop();

            var distanceFromTop = $(this).scrollTop();

            // if single-post-social-top exists AND window width is less than 992px must match upnext banner
            if( $('.single-post-social-top').length && $(window).width() < 992) {

                // if distance from top is greater than or equal to single-post-social-top offset
                // AND
                // if body does NOT have 'up-next-showing' class
                // add 'fixed' class to sticky social icons
                if (distanceFromTop >= $('.single-post-social-top').offset().top && !jQuery('body').hasClass('up-next-showing')) {

                    // add 'fixed' class to sticky-social element
                    $('.sticky-social').addClass('fixed');

                    //Modify UTM Source
                    var anchors = $('.sticky-social').find('a[href*="utm_source="]');
                    $.each(anchors, function() {
                        var href = $(this).attr('href');
                        if(href.indexOf('utm_source=Top') != -1) {
                            jQuery(this).attr('href', href.replace("utm_source=Top", "utm_source=Sticky"));
                        }
                        if(href.indexOf('utm_source=Facebook_Top') != -1) {
                            jQuery(this).attr('href', href.replace("utm_source=Facebook_Top", "utm_source=Facebook_Sticky"));
                        }
                    });
                    
                    
                } else {
                    // remove 'fixed' class of sticky-social element
                    $('.sticky-social').removeClass('fixed');

                    //Modify UTM Source
                    var anchors = $('.sticky-social').find('a[href*="utm_source="]');
                    $.each(anchors, function() {
                        var href = $(this).attr('href');
                        if(href.indexOf('utm_source=Sticky') != -1) {
                            jQuery(this).attr('href', href.replace("utm_source=Sticky", "utm_source=Top"));
                        }
                        if(href.indexOf('utm_source=Facebook_Sticky') != -1) {
                            jQuery(this).attr('href', href.replace("utm_source=Facebook_Sticky", "utm_source=Facebook_Top"));
                        }
                    });
                }

            }

        });

        $(window).on('resize', function () {
            if( $('#wpadminbar').length ) {
                $(".navbar-collapse").css({maxHeight: $(window).height() - $(".navbar-header").height() - $("#wpadminbar").height() + "px"});
            } else {
                $(".navbar-collapse").css({maxHeight: $(window).height() - $(".navbar-header").height() + "px"});
            }
        });

        $(window).resize();

        /**
         * Determine the mobile operating system.
         * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
         *
         * @returns {String}
         */
        function getMobileOperatingSystem() {
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;

            if (/windows phone/i.test(userAgent)) {
                return "Windows Phone";
            }

            if (/android/i.test(userAgent)) {
                return "Android";
            }

            if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                return "iOS";
            }

            return "unknown";
        }

         if( $('body').hasClass('single-post')) {
            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                if (typeof $( 'a[href^="sms:?"]' ).attr( 'href' ) != "undefined") {
                    var new_url = $( 'a[href^="sms:?"]' ).attr( 'href' ).replace( 'sms:?', 'sms:&' );
                    $( 'a[href^="sms:?"]' ).attr( 'href', new_url );
                }
            }

            if( getMobileOperatingSystem() === 'unknown' ) {
                if(  $( 'a[href^="sms:"]' ).length ) {
                    $( 'a[href^="sms:"]' ).parent().remove();
                }
            }
        }

        // if we are on university template, edit SMS links to only have page url
        if($('body').hasClass('page-template-university-ecourse-template')){
            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                if (typeof $('.at-bottom-sms-share  a').attr( 'href' ) != "undefined") {
                    var url = $('.at-bottom-sms-share  a').attr( 'href' ).replace( 'sms:?', 'sms:&' );
                    var new_url = url.replace(/body=.* /, 'body=');
                    $( '.at-bottom-sms-share a' ).attr( 'href', new_url );
                }
            }

            if( getMobileOperatingSystem() === 'unknown' ) {
                if(  $( 'a[href^="sms:"]' ).length ) {
                    $( 'a[href^="sms:"]' ).parent().remove();
                }
            }
        }


        /*** Advertise page ***/

        if( $('div.page-container.advertise-page').length ) {

            /**
             * Copyright 2012, Digital Fusion
             * Licensed under the MIT license.
             * http://teamdf.com/jquery-plugins/license/
             *
             * @author Sam Sehnert
             * @desc A small plugin that checks whether elements are within
             *         the user visible viewport of a web browser.
             *         only accounts for vertical position, not horizontal.
             */
            $.fn.visible = function (partial) {

                var $t = $(this),
                    $w = $(window),
                    viewTop = $w.scrollTop(),
                    viewBottom = viewTop + $w.height(),
                    _top = $t.offset().top,
                    _bottom = _top + $t.height(),
                    compareTop = partial === true ? _bottom : _top,
                    compareBottom = partial === true ? _top : _bottom;

                return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
            };

            function growBars() {

                $('.demographics .verticalCenter > span').each(function () {
                    thisHeight = ($(this).attr('rel') * 2.5 + 60) / 2;
                    topHeight = thisHeight;
                    if ($(window).width() < 767) {
                        topHeight = thisHeight + 30;
                    }
                    $(this).animate({paddingTop: topHeight, paddingBottom: thisHeight}, 2 * 1000).addClass('grownUp');
                    $(this).promise().done(function () {
                        // $(this).addClass('grownUp');
                    });
                });

            }

            function resizeTheStuff(element) {
                windowWidth = $(window).width();
                if (windowWidth < 767) {
                    $('.rotate').each(function () {
                        oldText = $(this).html();
                        oldText = oldText.replace('<br>', ' — ');
                        $(this).html(oldText);
                    });
                } else {
                    $('.rotate').each(function () {
                        newText = $(this).html();
                        newText = newText.replace(" — ", "<br>");
                        $(this).html(newText);
                    });
                }

                maxHeight = 0;
                $('.purpleBoxEqual').height('initial');
                $('.purpleBoxEqual').each(function () {
                    theHeight = $(this).height();
                    if (theHeight > maxHeight) {
                        maxHeight = theHeight;
                    }
                });
                $('.purpleBoxEqual').height(maxHeight);
                // alert('.purpleBoxEqual'+' '+maxHeight);

                maxHeight = 0;

                originalWidth = 100 / 6;
                width = 0;
                $('.wide1667').each(function () {
                    $(this).css('right', width + '%');
                    width = originalWidth + width;
                });

                originalWidth = 100 / 3;
                width = 0;
                $('.wide33').each(function () {
                    $(this).css('right', width + '%');
                    width = originalWidth + width;
                });

                originalWidth = 100 / 2;
                width = 0;
                $('.gender .wide50').each(function () {
                    $(this).css('right', width + '%');
                    width = originalWidth + width;
                });
                originalWidth = 100 / 2;
                width = 0;
                $('.children .wide50').each(function () {
                    $(this).css('right', width + '%');
                    width = originalWidth + width;
                });


            }


            $(window).on("load resize", function (e) {
                element = '';
                resizeTheStuff(element);
            });

            $(window).on("load resize scroll", function (e) {

                if ($('.animateTrigger').visible() || $('.demographicTitle').visible()) {
                    growBars();
                }

            });

        }

        /*** END - Advertise page ***/

        /*** Vertical image in posts ***/

        /* if( $('.row.vertical-image-wrapper').length ) {
            var vert_img_wrapper_row = $('.row.vertical-image-wrapper');
            var vert_img_wrapper = $('.row.vertical-image-wrapper .vertical-image-center');
            var vert_img = $('.row.vertical-image-wrapper img');
            var vert_caption = $('.row.vertical-image-wrapper .vertical-image-caption');
            var vert_text = $('.row.vertical-image-wrapper .vertical-image-text');

            var containerWidth = $('.single-post-content-inner').width();
            var viewportWidth = $(window).width();
            var imageWidth = parseInt(vert_img.attr('width'));
            var imageHeight = parseInt(vert_img.attr('height'));
            console.log(imageHeight + vert_caption.outerHeight());

            vert_img_wrapper.css('width', imageWidth + 'px');
            vert_img_wrapper.css('height', (imageHeight + vert_caption.outerHeight()) + 'px');
            vert_text.css('height', imageHeight + 'px');
            vert_text.css('width', (containerWidth - imageWidth ) + 'px');


            if( viewportWidth > 767 ) {
                vert_img_wrapper_row.removeClass('vert-mobile');
                vert_caption.css('margin-left', '0').css('margin-right', '0' );
            } else {
                vert_img_wrapper_row.addClass('vert-mobile');
                vert_caption.css('margin-left', ( (containerWidth - imageWidth ) / 2) ).css('margin-right', ( (containerWidth - imageWidth ) / 2) );
            }

            $(window).on('resize', function() {
                containerWidth = $('.single-post-content-inner').width();
                viewportWidth = $(window).width();
                vert_img_wrapper.css('width', vert_img.width() + 'px');
                vert_img_wrapper.css('height', (imageHeight + vert_caption.outerHeight()) + 'px');
                vert_text.css('height', imageHeight + 'px');
                vert_text.css('width', ( containerWidth - vert_img.width() ) + 'px');

                if( viewportWidth > 767 ) {
                    vert_img_wrapper_row.removeClass('vert-mobile');
                    vert_caption.css('margin-left', '0').css('margin-right', '0' );
                } else {
                    vert_img_wrapper_row.addClass('vert-mobile');
                    vert_caption.css('margin-left', ( (containerWidth - imageWidth ) / 2) ).css('margin-right', ( (containerWidth - imageWidth ) / 2) );
                }

            });


        } */

        /*** End of Vertical image in posts ***/

        /*** Social icons bouncing features ***/

        var utmSource = document.referrer;
        if (utmSource.length && utmSource.length > 0) {
            if (utmSource.toLowerCase().indexOf('pinterest.') >= 0) {
                if( $('.single-social-pinterest a i').length ) {
                    $('.single-social-pinterest').addClass('animated infinite bounce');
                }
            }
            else {
                if( $('.single-social-facebook a i').length ) {
                    $('.single-social-facebook').addClass('animated infinite bounce');
                }
            }
        }
        else {
            if( $('.single-social-facebook a i').length ) {
                $('.single-social-facebook').addClass('animated infinite bounce');
            }
        }

        /* End Social icons bouncing features */

        /* CTA Banner Fix */
        var navbar        = $('html body.one-time-fixed-body nav.navbar.navbar-fixed-top.one-time-fixed'),
            pageContainer = $('.page-container'),
            hasAdminBar   = $('body').hasClass('admin-bar'),
            navbarHeight  = navbar.outerHeight();

        function cta_banner_fix() {
            // Do not continue if height is less than 60
            var oneTimeHeight = $('#one-time-fixed').outerHeight();
            if ( 40 > oneTimeHeight ) {
                return;
            }

            // Set variable
            var newHeight = parseInt( ( oneTimeHeight + navbarHeight ) - 55 );

            // Fix navbar position
            navbar.each(function () {
                var adminBarHeight = ( $(window).width() <= 782 ) ? 46 : 32;
                     oneTimeHeight = ( true === hasAdminBar ) ? parseInt( oneTimeHeight + adminBarHeight ) : oneTimeHeight;
                this.style.setProperty( 'top', oneTimeHeight + 'px', 'important' );
            });

            // Fix page container position
            pageContainer.each(function () {
                if( ! $(this).hasClass('housing-project-page') ) {
                    this.style.setProperty( 'margin-top', newHeight + 'px', 'important' );
                }
            });
        }


        // Fix Banner Position
        cta_banner_fix();

        // Bind Banner Fix To Window Resize
        $(window).resize(function() {
            if ( !$('body').hasClass('one-time-closed') && $('body').hasClass('one-time-fixed-body') ) {
                cta_banner_fix();
            }
        });

        // Bind close button to fix banner position
        jQuery(document).on("click", "#one-time-fixed-close, #cta-banner-inner a", function (e) {
            jQuery("#cta-banner-inner").hide();
            jQuery("#one-time-fixed").hide();

            // Set positioning variables
            if ( hasAdminBar ) {
                var adminBarHeight = ( $(window).width() <= 782 ) ? 46 : 32;
            }
            else {
                var adminBarHeight = 0;
            }

            // Fix navbar position
            navbar.each(function () {
                this.style.setProperty( 'top', adminBarHeight + 'px', 'important' );
            });

            // Fix page container position
            pageContainer.each(function () {
                if( ! $(this).hasClass('housing-project-page') ) {
                    this.style.setProperty( 'margin-top', 0, 'important' );
                }
            });
        });
        /* End CTA Banner Fix */

        /* Play Video Inline */
        
        if ( $('.has-youtube-video').length ) {
            // Load the YouTube iFrame Player API code asynchronously.
            var tag = document.createElement('script');
            tag.setAttribute("async", "");
            tag.src = "//youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('footer')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

            // YouTube Video Click
            jQuery( 'body' ).on( 'click', '.has-youtube-video .youtube-play-icon', function(event) {
                // Prevent default action
                event.preventDefault();

                // Determine if activated
                if ( jQuery(this).parent().hasClass( 'yt-video-activated' ) ) {
                    return false;
                }


                // Get YouTube Video ID
                var yt_video_id = $(this).parent().attr( 'data-yt-video-id' );

                // Hide picture
                $(this).hide()
                    .parent()
                        .find( '.four_by_six' )
                            .hide();

                // Create YT iFrame
                createYTiFrame( $(this).parent().find( 'div[data-iframe-id="iframe-video-' + yt_video_id + '"]' ), yt_video_id );

                // Show Video Container
                $(this).parent().find( 'div[data-video-id="video-' + yt_video_id + '"]' ).show();

                // Add activated class
                $(this).parent().addClass( 'yt-video-activated' );
            } );

            // On YouTube Iframe Ready
            function createYTiFrame( element, yt_video_id ) {
                // Create unique youtube iframe id
                var iframeID = 'yt-iframe-' + yt_video_id + Math.floor( Math.random() * 20000 );

                // Add ID to element
                element.attr( 'id', iframeID );

                // Create YouTube iFrame
                var player = new YT.Player( iframeID, {
                    height: '100%',
                    width: '100%',
                    videoId: yt_video_id,
                    controls: 0,
                    showinfo: 0,
                    autohide: 1,
                    modestbranding: 1,
                    events: {
                        'onReady': onInlineYTPlayerReady
                    }
                });
            }

            // Play Video On Ready
            function onInlineYTPlayerReady(event) {
                event.target.playVideo();
            }
        }
        /* End Play Video Inline */


        /* Consent Form Date Fix */
        if ( jQuery('.tph-text-to-date').length ) {
            var today = new Date();

            jQuery('.tph-text-to-date').find('input[type="text"]').attr('type', 'date')
                .attr('disabled', 'disabled');

            document.querySelector('.tph-text-to-date input[type="date"]').value = today.getFullYear() + '-0' + (today.getMonth() + 1) + '-' + today.getDate();
        }
        /* End Consent Form Date Fix */

        if ( jQuery('.tph-single-post-inner-related-block a').length ) {
            var eventLabel = current_post_id;
            var main_url   = 'https://www.thepennyhoarder.com' + location.pathname;
            var tph_uuids  = tph.utility.uuid.get();

          $('.tph-single-post-inner-related-block a').each( function( index, value ) {
            var url = $(this).attr( 'href' );
            if ( 'undefined' !== url ) {
              var url_obj = new URL( url );
              url_obj.searchParams.set( 'aff_sub', 'trk-seo-inline-' + parseInt( index + 1 ) + '-' + $('.tph-single-post-inner-related-block').first().data( 'post-id' ) );
              var modifiedUrl = url_obj.toString().replace( '%27', '' );
              $(this).attr( 'href', modifiedUrl );
            }
          } );

            $('.tph-single-post-inner-related-block a').on('click', function (event) {
                var eventCategory = "RelatedInnerPostClick";
                var eventAction = jQuery(this).attr('data-post-id');
                var eventPosition = jQuery(this).attr('data-position');

                // GA GTM Tracking
                dataLayer.push({
                    'currentPostID': eventLabel,
                    'clickPostID': eventAction,
                    'clickedPosition': eventPosition,
                    'event': eventCategory
                });

                // Parsely Redshift Tracking
                fire_parsely_event({
                    url: main_url,
                    action: '_' + eventCategory,
                    data: {
                        _high_seo_cluster_block_click_current_post_id: eventLabel,
                        _high_seo_cluster_block_click_clicked_post_id: eventAction,
                        _high_seo_cluster_block_click_clicked_position: eventPosition,
                        _tph_custom_uuids: tph_uuids
                    }
                });
            });
        }


        /* High SEO Cluster Link Click */
        if ( $('.high-seo-cluster__list li a').length ) {
            var eventLabel = current_post_id;
            var main_url   = 'https://www.thepennyhoarder.com' + location.pathname;
            var tph_uuids  = tph.utility.uuid.get();

            $('.high-seo-cluster__list li a').each( function( index, value ) {
                var url = $(this).attr( 'href' );
                if ( 'undefined' !== url ) {
                    var url_obj = new URL( url );
                    url_obj.searchParams.set( 'aff_sub', 'trk-seo-bottom-' + parseInt( index + 1 ) + '-' + $('#high-seo-cluster').data( 'post-id' ) );
                    var modifiedUrl = url_obj.toString().replace( '%27', '' );
                    $(this).attr( 'href', modifiedUrl );
                }
            } );

            $('.high-seo-cluster__list li a').on('click', function (event) {
                var eventCategory = "HighSEOClusterBlockClick";
                var eventAction = jQuery(this).attr('data-post-id');
                var eventPosition = jQuery(this).attr('data-position');

                // GA GTM Tracking
                dataLayer.push({
                    'currentPostID': eventLabel,
                    'clickPostID': eventAction,
                    'clickedPosition': eventPosition,
                    'event': eventCategory
                });

                // Parsely Redshift Tracking
                fire_parsely_event({
                    url: main_url,
                    action: '_' + eventCategory,
                    data: {
                        _high_seo_cluster_block_click_current_post_id: eventLabel,
                        _high_seo_cluster_block_click_clicked_post_id: eventAction,
                        _high_seo_cluster_block_click_clicked_position: eventPosition,
                        _tph_custom_uuids: tph_uuids
                    }
                });
            });
        }
        /* End High SEO Cluster Link Click */

    });
})(jQuery);
