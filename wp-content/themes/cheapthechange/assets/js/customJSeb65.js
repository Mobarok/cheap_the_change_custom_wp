//##########################
// TPH setting get variables
//##########################
// these functions live in the theme root/tracking-scripts.php
// on a rare occasion, that file doesn't load and it causes a console error 
// which results in this file not loading.
// these are here to prevent that error. If we update the code in the other file
// we should copy the changes to the functions below

if (typeof get_browser_query_parameter != 'function') {
	// get variables 
	var get_browser_query_parameter = function get_browser_query_parameter(target_get_variable) {
		var page_query_string = decodeURIComponent(window.location.search.substring(1)),
			get_variables = page_query_string.split('&'),
			query_variable,
			i;
		for (i = 0; i < get_variables.length; i++) {
			query_variable = get_variables[i].split('=');
			if (query_variable[0] === target_get_variable) {
				return query_variable[1] === undefined ? true : query_variable[1];
			}
		}
		return false;
	};
}

if (typeof logThis != 'function') {

	// TPH logging method
	function logThis() {
		if (typeof arguments != 'undefined' && get_browser_query_parameter('debug')) {
			for (var i = 0; i < arguments.length; i++) {
				console.log("DEBUG [" + typeof arguments[i] + "]", arguments[i]);
			}
		}
	}
}

// set up get variables to use later
window.$_GET = {};
function set_global_get_variables() {
	var page_query_string = decodeURIComponent(window.location.search.substring(1));
	var get_variables = page_query_string.split('&');
	var query_variable, i;

	for (i = 0; i < get_variables.length; i++) {
		query_variable = get_variables[i].split('=');
		window.$_GET[query_variable[0]] = query_variable[1];
	}
}
set_global_get_variables();

function run_after_parsely_loads() {
	if (jQuery('.single-posts-recommended').length >= 1) {
		rec_content_block_c();
		logThis('Ran rec_content_block_c');
		rec_content_frontend();
		logThis('Ran rec_content_frontend');
	}
}

//########################
// Section Title Here
//########################

jQuery(document).ready(function ($) {

	//################################
	// Add the class .ios-device to the 
	// body tag for ease of use in other
	// js and css files.
	//################################
	var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
	if (iOS) {
		$('body').addClass('ios-device');
	}

	//####################################################################################
	// Check and see if Google Tag Manager is enabled, don't fire local FB pixel if true..
	//####################################################################################
	if (use_google_tag_manager == false) {

		//################################
		// Tracking pixel for signup forms
		//################################
		jQuery('.iterable-form').on('submit', function () {
			var form_location = jQuery(this).attr('form-location');
			if (form_location == '' || form_location == null) {
				form_location = 'not_set';
			}

			jQuery(document).on('DOMNodeInserted', function (e) {
				if (jQuery(e.target).hasClass('error')) {
					fbq('trackCustom', 'emailSignup', {
						custom_param1: form_location,
						custom_param2: 0
					});
				} else if (jQuery(e.target).hasClass('success')) {
					fbq('trackCustom', 'emailSignup', {
						custom_param1: form_location,
						custom_param2: 1
					});
				}
			});
		});

		//#########################################################################
		// Google Adwords + Taboola onclick= attribute addition to affiliate links
		//#########################################################################

		jQuery('a[href*="go2cloud.org"],a[href*="//t.thepennyhoarder.com"]').each(function () {
			jQuery(this).attr('onclick', 'goog_report_conversion(); taboola_clickout();');
		})

		//########################################
		// Facebook init to start Facebook scripts
		//########################################

		!function (f, b, e, v, n, t, s) {
			if (f.fbq) return; n = f.fbq = function () {
				n.callMethod ?
				n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			}; if (!f._fbq) f._fbq = n;
			n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
			t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
		}(window,
			document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '263664193816679');
		fbq('track', 'PageView');
		fbq('track', 'ViewContent');

		//#############################################
		// ClickOut to Facebook on specific pages only.
		//#############################################

		jQuery("body").on('click', 'a[href*="thepennyhoarder.go2cloud.org"],a[href*="//t.thepennyhoarder.com"]', function (event) {
			if (get_browser_query_parameter('aff_id') == '4' || get_browser_query_parameter('aff_id') == '44' || get_browser_query_parameter('aff_id') == '70' || get_browser_query_parameter('aff_id') == '84') {
				fbq('track', 'ClickOut');
			}
		})

	}


	//################################
	// HasOffers Popular URL Parameters
	//################################
	// var append_this = '?aff_id=56&utm_source=most_popular&utm_medium=organic';
	// jQuery('.trending-block-popular .trending-block-popular a').each(function(element){
	//     var new_link = jQuery(this).attr('href') + append_this;
	//     jQuery(this).attr('href', new_link);
	// });


	//##################################
	// Open external links in a new tab.
	//##################################

	$("a[href^='http']").filter(function () {
		return this.hostname && this.hostname !== location.hostname;
	}).attr('target', '_blank');
	$("a[href^=http]").each(function () {
		if (this.href.indexOf('//t.thepennyhoarder.com') > -1) {
			$(this).attr('target', '_blank');
		} else if (this.href.indexOf('//thepennyhoarder.go2cloud.org') > -1) {
			$(this).attr('target', '_blank');
		}

	});

	//############################################
	// Modify WP caption to use custom TPH caption
	//############################################

	var caption = jQuery('meta[itemprop=caption]').attr("content");
	if (caption === undefined || caption === null || caption == 'undefined') {
		// do nothing if it's undefined
	} else {
		jQuery(".single-post-image-meta-copyright span").text(caption);
	}

	// Has offers modifications of links on page and tracking urls  

	// updating aff_id and aff_sub3 to values in the URL if they exist
	function replaceQueryParam(param, newval, search) {
		var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
		var query = search.replace(regex, "$1").replace(/&$/, '');
		return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
	}

    // standardize affiliate links on the page
    // the PHP implementation of this script is located in tph-class.php
    tph.utility.append_tracking_to_affiliate_links();
    

	//############################################
	// Bank Rate Test JS
	//############################################

	host = window.location.protocol + "//" + document.domain;
	pageTitle = document.title;
	pageURL = window.location.href.replace(host, "");

	window.addEventListener('blur', function () {
		if (document.activeElement.id == 'placement_180826_0_iframe') {

			// alert("Clicked in the iframe "+pageTitle+" - "+pageURL);

			if (document.domain.indexOf("staging") > -1 || document.domain.indexOf("local") > -1) {
				logThis('Clicked Bank Rate iFrame Test:', pageURL, pageTitle);
				analytics.track('Clicked Bank Rate iFrame', {
					'Page URL': pageURL,
					'Page Title': pageTitle
				}, function () {
					logThis("segment tracked successfully iframe click for bank rate: ");
				});
			} else {
				analytics.track('Clicked Bank Rate iFrame', {
					'Page URL': pageURL,
					'Page Title': pageTitle
				});

			}

		}
	});

	//##############################################################
	// CT FaceBook HasOffers
	//##############################################################
	// temp scipt to help find differences between fb and ho reported clicks
	jQuery("body").on('click', 'a[href*="thepennyhoarder.go2cloud.org"],a[href*="//t.thepennyhoarder.com"]', function (event) {

		var click_info = {};
		click_info.browser_url = location.href;
		click_info.user_agent = navigator.userAgent;
		click_info.ip = jQuery('.clicker_number').attr('data-count');
		click_info.clicked_url = this.href;
		click_info.referrer = document.referrer;
		click_info = encodeURIComponent(JSON.stringify(click_info));

		var this_domain = jQuery('meta[rel="currentDomain"]').attr('content');

		var post_to_url = this_domain + '/scripts/hoc.php?data=' + click_info;

		jQuery.getJSON(post_to_url, function (data_returned) {
		});
	});

	//##########################################
	// Iterable API
	//##########################################

	// Get the date
	today = $("meta[rel='date:today']").attr('content');


	if ($('meta[rel="email:yesmail"]').attr('content') == 'true') {
		$('#sidebar .clearfix.email-form,#footerForm').addClass('iterable-form');
		$('#sidebar .clearfix.email-form').attr('form-location', 'sidebar');
		$('#footerForm').attr('form-location', 'footer');
	}

	// When the iterable email input is selected, remove the error message
$('body').on('focus', '.iterable-form input[name="email"], .iterable-form input[name="zip"], .iterable-form input[name="firstName"]', function (event) {
    $(this).closest('form').find('input').removeClass('has-error').tooltip('hide'); // remove the error class 
    $(this).closest('form').find('.help-block').remove(); // remove the error text 
	$(this).closest('.bottom-form').removeClass('bottom-form-error').removeClass('bottom-form-error-subscribed'); // remove the error class
    $(this).closest('.subscribe-page-main-form').removeClass('subscribe-form-error').removeClass('subscribe-form-error-subscribed'); // remove the error class
    $(this).closest('.subscribe-page-main-form').find('.error-spacing').removeClass('error-spacing').css('margin-top', '0');
    $(this).closest('.subscribe-page-main-form').find('.has-error').removeClass('has-error');
  });

	function scrollToTopFunction() {
		document.body.scrollTop = 0; // For Safari
		document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
	}

	$('body').on('submit', '.iterable-form', function (event) {

		// Stop the form from submitting the normal way and refreshing the page
		event.preventDefault();

		// When iterable submit is pressed, remove the error message
		$(this).find('input').removeClass('has-error'); // remove the error class
		$(this).find('.help-block').remove(); // remove the error text

		capturelocation = $(this).attr('form-location');
		captureurl = $('meta[rel="url:full"]').attr('content');
		$('form[form-location="' + capturelocation + '"] input[type="submit"]').attr('value', '...');
		$('form[form-location="' + capturelocation + '"] input[type="submit"]').prop('disabled', true);
		// Get the form data
		// There are many ways to get this data using jQuery (you can use the class or id also)
		var formData = {
			'email': $(this).find('input[name="email"]').val(),
			'firstName': $(this).find('input[name="firstName"]').val(),
			'lastName': $(this).find('input[name="lastName"]').val(),
			'templateID': $(this).find('input[name="templateID"]').val(),
			'targetList': $(this).find('input[name="targetList"]').val(),
			'listType': $(this).find('input[name="listType"]').val(),
			'zip': $(this).find('input[name="zip"]').val(),
			'captureurl': captureurl,
			'capturelocation': capturelocation
		};

		if(get_browser_query_parameter('iterable_environment')){
			formData['environment'] = get_browser_query_parameter('iterable_environment');
		}

		logThis("Form data");
		logThis(formData);

		// Process the form
		// To test locally, or on staging, uncomment below and comment production url. 
		process_subscribe_request_url = $('meta[rel="currentDomain"]').attr('content') + '/api/process-iterable-api-request.php';
		// process_subscribe_request_url = 'https://www.thepennyhoarder.com/api/process-yesmail-api-request.php'; 
		// process_subscribe_request_url = 'http://local.thepennyhoarder.com/api/process-yesmail-api-request.php'; 

		logThis(process_subscribe_request_url);
		$.ajax({
			type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url: process_subscribe_request_url, // the url where we want to POST
			data: formData, // our data object
			dataType: 'json', // what type of data do we expect back from the server
			encode: true,
			success: function (dataReturned) {
				logThis("Successful Call");
				logThis(dataReturned);
				logThis(capturelocation);

				// Here we will handle errors and validation messages
				if (typeof dataReturned == 'undefined' || dataReturned == null) {
					//$('form[form-location="' + capturelocation + '"] input[type="email"]').addClass('has-error'); // Red input box on error
					if (capturelocation == 'footer') {
						$('form[form-location="' + capturelocation + '"] .input-group').after('<div class="help-block error">There was an issue, please try again.</div>'); // Generic error message return
					} else {
						$('form[form-location="' + capturelocation + '"] .input-group-btn').after('<div class="help-block error">There was an issue, please try again.</div>'); // Generic error message return
					}

				} else if (!dataReturned.success) {
					// Handle errors for email
					if (capturelocation == 'subscribe-page' || capturelocation == 'referral') {
						// if email required error AND first name required error, show message on both inputs

                        if ( dataReturned.errors.email ) {
                            if( dataReturned.errors.error_type == 'already_subscribed' ) {
                                $('.subscribe-page-main-form').addClass('subscribe-form-error-subscribed');
                            } else {
                                $('.subscribe-page-main-form').addClass('subscribe-form-error');
                            }
                            $('input#subscribe-email-field').closest('.subscribe-email-box').addClass('error-spacing');
                            $('input#subscribe-email-field').attr('title', dataReturned.errors.email).attr('data-original-title', dataReturned.errors.email).addClass('has-error').tooltip({trigger: 'manual'}).tooltip('show').val('');
							$('#subscribe-email-field:-webkit-autofill').css('-webkit-box-shadow', '0 0 0px 1000px white inset'); //removes webkit yellow when we blank the field
							$('#subscribe-email-field-label').addClass('has-error');
                        }

                        if ( dataReturned.errors.zip ) {
                            if( dataReturned.errors.error_type == 'already_subscribed' ) {
                                $('.subscribe-page-main-form').addClass('subscribe-form-error-subscribed');
                            } else {
                                $('.subscribe-page-main-form').addClass('subscribe-form-error');
                            }
                            $('input#subscribe-zip-field').closest('.subscribe-email-box').addClass('error-spacing');
                            $('input#subscribe-zip-field').attr('title', dataReturned.errors.zip).attr('data-original-title', dataReturned.errors.zip).addClass('has-error').tooltip({trigger: 'manual'}).tooltip('show').val('');
							$('#subscribe-zip-field-label').addClass('has-error');
                        }

						if ( dataReturned.errors.name ) {
							if( dataReturned.errors.error_type == 'already_subscribed' ) {
								$('.subscribe-page-main-form').addClass('subscribe-form-error-subscribed');
							} else {
								$('.subscribe-page-main-form').addClass('subscribe-form-error');
							}
							$('input#subscribe-name-field').closest('.subscribe-email-box').addClass('error-spacing');
							$('input#subscribe-name-field').attr('title', dataReturned.errors.name).attr('data-original-title', dataReturned.errors.name).addClass('has-error').tooltip({trigger: 'manual'}).tooltip('show').val('');
						}

						$('input.has-error').each(function( key, val ) {
							var tooltip_height = $(this).siblings('.tooltip').height();
							var margin_top = tooltip_height + 25;
                            var tooltip_top = tooltip_height + 20;
                            $(this).siblings('.tooltip').css('top', '-' + tooltip_top + 'px');
                            $(this).closest('.error-spacing').css('margin-top', margin_top);
						});

						// else if capture location is university landing page
					} else if (dataReturned.errors.email != "undefined") {
						$('form[form-location="' + capturelocation + '"] input[name="email"]').addClass('has-error'); // Red input box on error

						if (capturelocation == 'footer') {
							if( dataReturned.errors.error_type == 'already_subscribed' ) {
								$('.bottom-form').addClass('bottom-form-error-subscribed');
							} else {
								$('.bottom-form').addClass('bottom-form-error');
							}

							$('input#emailaddressbottom').attr('title', dataReturned.errors.email).attr('data-original-title', dataReturned.errors.email).tooltip({trigger: 'manual'}).tooltip('show').val('').blur();
							var tt = $('.bottom-form .email-box').find('.tooltip');
							var tt_height = $('.bottom-form .email-box').find('.tooltip').height();
							var top_position = -(tt_height + 14);
							$(tt).css('top', top_position);

							// if capture location is subsribe page
						} else if (capturelocation == 'university-ecourse-page') {
							// if email required error AND first name required error, show message on both inputs
							if (dataReturned.errors.email && dataReturned.errors.name) {
								$('form[form-location="' + capturelocation + '"] input[name="email"]').attr('placeholder', dataReturned.errors.email).val(''); // Error message return
								$('form[form-location="' + capturelocation + '"] input[name="firstName"]').addClass('has-error'); // Red input box on error
								$('form[form-location="' + capturelocation + '"] input[name="firstName"]').attr('placeholder', dataReturned.errors.name).val(''); // Error message return
								// else if only email error, show message on email input
							} else if (dataReturned.errors.email) {
								$('form[form-location="' + capturelocation + '"] input[name="email"]').attr('placeholder', dataReturned.errors.email).val(''); // Error message return
								// else if only first name error, show message on first name input
							} else if (dataReturned.errors.name) {
								$('form[form-location="' + capturelocation + '"] input[name="email"]').removeClass('has-error')
								$('form[form-location="' + capturelocation + '"] input[name="firstName"]').addClass('has-error'); // Red input box on error
								$('form[form-location="' + capturelocation + '"] input[name="firstName"]').attr('placeholder', dataReturned.errors.name).val(''); // Error message return
								// else, show error message on email input
							} else {
								$('form[form-location="' + capturelocation + '"] input[name="email"]').attr('placeholder', dataReturned.errors.email).val(''); // Error message return
							}
						} else if (capturelocation == 'money-in-your-pocket') {
							$('#money-in-your-pocket-email').hide();
							$('#money-in-your-pocket').append('<div class="money-in-your-pocket-already-subscribed"><img class="thumbs-up-error" src="/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-up-selected.png" /> <span class="money-in-your-pocket-blurb">Looks like you’re already a faithful Penny Hoarder! <span class="money-in-your-pocket-blurb-second-line">We hope you’re enjoying the newsletter.</span></span></div>');
						} else {
							$('form[form-location="' + capturelocation + '"] .input-group-btn').after('<div class="help-block error">' + dataReturned.errors.email + '</div>'); // Error message return
						}

					} else {
						$('form[form-location="' + capturelocation + '"] input[name="email"]').addClass('has-error'); // Red input box on error
						if (capturelocation == 'footer') {
							$('form[form-location="' + capturelocation + '"] .input-group').after('<div class="help-block error">There was an issue, please try again...</div>'); // Error message return
						} else {
							$('form[form-location="' + capturelocation + '"] .input-group-btn').after('<div class="help-block error">There was an issue, please try again...</div>'); // Error message return
						}
					}

				} else {
					// ALL GOOD! just show the success message!

					// Fire success to dataLayer for analytics
						window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						event: 'formSubmissionSuccess',
						formId: capturelocation
						});
						
					// $('form[form-location="'+capturelocation+'"] input[type="submit"]').attr('value','Success! You Rock!');
					// $('form[form-location="'+capturelocation+'"] input[type="submit"]').prop('disabled', 'true');
					if (capturelocation == 'footer') {
		   //$('form[form-location="' + capturelocation + '"] .input-group').after('<div class="help-block success" hidden>Success! You Rock!</div>'); 
						$('.bottom-form').addClass('bottom-form-success').addClass('text-center').removeClass('bottom-form-error').removeClass('bottom-form-error-subscribed'); 
			$('.bottom-form').children().remove(); 
						$('.bottom-form').append('<h4 class="footer-success-heading">' + dataReturned.message + '</h4>');
					} else if (capturelocation == 'subscribe-page' || capturelocation == 'referral') {
						scrollToTopFunction();
						
						$('.subscribe-page-main-form').addClass('hidden');
						$('.subscribe-page.subscribeForm').append('<div class="success-animation"><img class="hidden-xs" src="/wp-content/themes/pennyhoarder/assets/images/PaperAirplanes_Success.gif" alt="Success!"><img class="hidden-sm hidden-md hidden-lg" src="/wp-content/themes/pennyhoarder/assets/images/480px_PaperAirplanes_Success_new.gif" alt="Success!"></div><div class="subscribe-page-success-animation"><div class="container"><h2>Nice!</h2><p>' + dataReturned.message + '</p><a href="/" class="btn btn-primary btn-block btn-subscribe-page btn-subscribe-page-success">Return to Homepage</a></div></div>');
					
						if(capturelocation == 'referral') {
							campaign.identify({
								firstname: "",
								lastname: "",
								email: formData['email']
							});
						}
						
					} else if (capturelocation == 'university-ecourse-page') {
						$('form[form-location="' + capturelocation + '"]').parent().hide();
						$('.subscribe-description').parent().append('<h3 class="form-success-message">' + dataReturned.message + '</h3>');
						$('.subscribe-description').parent().append('<div class="form-success-btn"><img src="/wp-content/themes/pennyhoarder/assets/images/success-checkmark.png"> SUCCESS</div>');
					} else if (capturelocation == 'money-in-your-pocket') {
						$('#money-in-your-pocket-email').hide();
						$('#money-in-your-pocket').append('<div class="money-in-your-pocket-new-subscriber"><img class="thumbs-up-success" src="/wp-content/themes/pennyhoarder/assets/images/miyp-check-mark.png" /> <span class="money-in-your-pocket-blurb">Thank you!</span></div>');
					} else {
						$('form[form-location="' + capturelocation + '"] .input-group-btn').after('<div class="help-block success" hidden>' + dataReturned.message + '</div>');
					}

					$(' form[form-location="' + capturelocation + '"] .help-block').slideDown();
					// $('form[form-location="'+capturelocation+'"] input[type="submit"]').after('<div class="help-block">' + dataReturned.message + '</div>');
				}

				$('form[form-location="' + capturelocation + '"] input[type="submit"]').prop('disabled', false);
				$('form[form-location="' + capturelocation + '"] input[type="submit"]').attr('value', 'SUBSCRIBE');
			},
			error: function (xhr, textStatus, error) {
				logThis("CALL FAILED");
				logThis(dataReturned);
				logThis('textStatus: ' + textStatus);
				logThis('error: ' + error);
				logThis('responseText: ' + xhr.responseText);
			}

		});

		// $(this).find('input[type="submit"]').prop('disabled',false);
		// $(this).find('input[type="submit"]').attr('value','SUBMIT');

	});

	// Let's wait a bit longer to run these.
	$(window).load(function () {

		//########################
		// Get the Parsely UUID
		//########################

		var loop_parsely_uuid = 0;
		var parsely_uuid_exists = setInterval(function () {

			if (typeof PARSELY != 'undefined' && typeof PARSELY.config != 'undefined' && typeof PARSELY.config.parsely_site_uuid != 'undefined' && PARSELY.config.parsely_site_uuid != '') {

				// Get the unique user identifier from Parsely
				parsely_uuid = PARSELY.config.parsely_site_uuid;

				if (window.location.hostname == 'www.thepennyhoarder.com') {
					current_page_url = location.href;
				} else {
					current_page_url = location.href.replace('http://local', 'https://www');
				}
				current_page_url = current_page_url.split('?')[0];

				// parsely api endpoint to track page the user is on
				var parsely_track_profile_url = 'https://api.parsely.com/v2/profile?apikey=thepennyhoarder.com&uuid=' + parsely_uuid + '&url=' + current_page_url;
				// send data to parsely to track in users profile
				jQuery.get(parsely_track_profile_url, function (response, status) {
					// logThis(response);
				});

				// logThis( parsely_uuid );
				clearInterval(parsely_uuid_exists);
			} else {
				loop_parsely_uuid++;

				if (loop_parsely_uuid >= 15) {
					// it has been 3 seconds, stop trying
					logThis('Parsely not ready, cannot get uuid.');
					clearInterval(parsely_uuid_exists);
				}
			}

		}, 200);



		if ($('meta[rel="email:yesmail"]').attr('content') == 'true') {
				$('#sidebar .clearfix.email-form,#footerForm').addClass('iterable-form');
				$('#sidebar .clearfix.email-form').attr('form-location','sidebar');
				$('#footerForm').attr('form-location','footer');
		}

		//###############################
		// TPH Fingerprinting to Redshift
		//###############################
		// If a user comes with an aff_unique2 set, split it into an object and cookies.
		// Get the URL parameters, split them into an object, check the object against
		var tph_uuids_cookie = tph.utility.uuid.get();

		//########################
		// Parsely Beacon for UUID
		//########################
		var url = 'https://www.thepennyhoarder.com' + location.pathname;

		var beacon_data = {
			url: url,
			action: '_tph_uuid',
			data: {
				_tph_custom_uuids: tph.utility.uuid.get()
			}
		};

		tph.utility.parsely.track.event( beacon_data );

		//#####################
		// Money In Your Pocket
		//#####################

		// hide MiYP on non-posts 
		jQuery(".page #money-in-your-pocket").hide();

		if ($('body').hasClass('single-post')) {
			var post_id = jQuery('#post_id').val();
			jQuery("#money-in-your-pocket-thumbs img.money-in-your-pocket.thumbs-up").bind("mouseenter", function () {
				if (window.innerWidth > 1199 && !jQuery("#money-in-your-pocket-thumbs img.thumbs-down").hasClass("thumbs-down-selected")) {
					jQuery(this).attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-up-selected.png");
				}
			});
			jQuery("#money-in-your-pocket-thumbs img.money-in-your-pocket.thumbs-up").bind("mouseleave", function () {
				if (window.innerWidth > 1199 && !jQuery("#money-in-your-pocket-thumbs img.thumbs-down").hasClass("thumbs-down-selected")) {
					jQuery(this).attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-up.png");
				}
			});
			jQuery("#money-in-your-pocket-thumbs img.money-in-your-pocket.thumbs-down").bind("mouseenter", function () {
				if (window.innerWidth > 1199) {
					jQuery(this).attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-down-selected.png");
				}
			});
			jQuery("#money-in-your-pocket-thumbs img.money-in-your-pocket.thumbs-down").bind("mouseleave", function () {
				if (window.innerWidth > 1199) {
					jQuery(this).attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-down.png");
				}
			});

			//#####################################################################################
			// Check the thumb cookie status for Money In Your Pocket, set one if it doesn't exist.
			//#####################################################################################


			if (tph.utility.cookie.read('tph_thumbs_up') == false) {
				tph.utility.cookie.create('tph_thumbs_up', '[0]', 3650);
			}

			if (tph.utility.cookie.read('tph_thumbs_down') == false) {
				tph.utility.cookie.create('tph_thumbs_down', '[0]', 3650);
			}

			if (tph.utility.cookie.read('user_email') == false) {
				tph.utility.cookie.create('user_email', '0', 3650);
			}

			//#####################################
			// Check for thumb status on page load.
			//#####################################

			parsely_thumb_list(post_id);

		}

		//################################################ 
		// Certain way to play vimeo videos if on firefox 
		//################################################ 

		var FF = !(window.mozInnerScreenX == null);
		if (FF) {
			jQuery(".embed-container video.vimeo_video").attr("onclick", "play()");
		} else {
			// not firefox  
		}

		//###################################################################################### 
		// Add tracking to posts in recommended content block (bottom rail) if not already there 
		//###################################################################################### 

		jQuery('.single-posts-recommended .post-recommended-inner a').each(function (value, index) {
			var this_link = jQuery(this).attr('href');

			var aff_id = tph.page_data.aff_id;
	
			var utm = '';
	
			if (get_browser_query_parameter('utm_source') != '' && get_browser_query_parameter('utm_source') != false) {
				utm = utm + '&utm_source=' + get_browser_query_parameter('utm_source');
			}
	
			if (get_browser_query_parameter('utm_medium') != '' && get_browser_query_parameter('utm_medium') != false) {
				utm = utm + '&utm_medium=' + get_browser_query_parameter('utm_medium');
			}

			if (this_link.indexOf('?') == -1) {
				this_link = this_link + '?aff_id='+ aff_id + utm;
				jQuery(this).attr('href', this_link);
			}
		})

		//############################################################# 
		// BLACKLIST USER AGENTS FROM BEING ABLE TO CLICK HASOFFERS ADS 
		//############################################################# 

		var user_agent = navigator.userAgent.toLowerCase();
		if (user_agent.indexOf('wordpress') != -1 || user_agent.indexOf('minigun') != -1) {
			jQuery('a[href*="go2cloud.org"]').each(function (content, index) {
				jQuery(this).attr('href', jQuery(this).attr('href').replace('go2cloud', 'disabled'));
			})
		}

		//############################################################################ 
		// Add class `disclaimer` to h3 if it contains "SPONSORED ADVERTISING CONTENT" 
		//############################################################################ 

		if (jQuery('h3:contains("SPONSORED ADVERTISING CONTENT")').length) {
			jQuery('h3:contains("SPONSORED ADVERTISING CONTENT")').removeAttr('style').addClass('disclaimer');
		}

		//############################################################ 
		// Remove any `p` elements if they contain `match_this` string 
		//############################################################ 

		var match_this = 'Click <a href="https://cdn.thepennyhoarder.com/documents/The_Penny_Hoarder_Media_One_Pager.pdf" target="_blank"><b>here</b></a> to download our overview page.';
		jQuery('p').each(function () {
			if (jQuery(this).html() == match_this) {
				jQuery(this).remove();
			}
		});

		//############################################ 
		// Remove server_id and main post share number 
		//############################################ 

		jQuery(".server_id_float, .main-post .share-box .number").remove();

		//###################################################### 
		// If recommended post does not have `hidden-md` class, 
		// add `hidden-xs`, `hidden-sm` classes 
		//###################################################### 

		if (jQuery('.post-page .single-posts-recommended').not('.hidden-md').length != 0) {
			jQuery('.single-posts-recommended').not('.hidden-md').addClass('hidden-xs').addClass('hidden-sm');
			var recommended_block = jQuery('.single-posts-recommended').not('.hidden-md').clone();
			jQuery('.single-posts-recommended').not('.hidden-md').remove();
			jQuery('div.post-page > .row.category-subcategory.flex-row').before(recommended_block);
		}

		//################################ 
		// Homepage stats counts fallbacks 
		//################################ 

		// UPDATE THE PINTEREST COUNT FOR THE DYNAMIC DISPLAYS 
		pinterestCountTemp = jQuery('#pinterestCountCurrent').text();
		if (pinterestCountTemp) {
			pinterestCountTemp = pinterestCountTemp.toString().replace(/,/g, "");

			jQuery('.pinterestFollowerCount').each(function () {
				oldVal = jQuery(this).text();
				oldVal = oldVal.toString().replace(/,/g, ""); // 1500000 

				newVal = Math.floor(pinterestCountTemp / 1000) * 1000;
				if (newVal > oldVal) {
					newVal = newVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					jQuery(this).text(newVal);
				}
			});

		}

		// UPDATE THE email COUNT FOR THE DYNAMIC DISPLAYS 
		emailCountCurrent = jQuery('#emailCountCurrent').text();
		if (emailCountCurrent) {
			emailCountCurrent = emailCountCurrent.toString().replace(/,/g, "");

			jQuery('.emailSubscriberCount').each(function () {
				oldVal = jQuery(this).text();
				oldVal = oldVal.toString().replace(/,/g, ""); // 1500000 

				newVal = Math.floor(emailCountCurrent / 1000) * 1000;
				if (newVal > oldVal) {
					newVal = newVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					jQuery(this).text(newVal);
				}
			});

		}

		facebookLikeCount = jQuery('.facebookLikeCount').text();
		twitterFollowerCount = jQuery('.pinterestFollowerCount').text();
		emailSubscriberCount = jQuery('.emailSubscriberCount').text();

		if (facebookLikeCount != 0 && twitterFollowerCount != 0 && emailSubscriberCount != 0) {
			facebookLikeCount = facebookLikeCount.toString().replace(/,/g, "");
			twitterFollowerCount = twitterFollowerCount.toString().replace(/,/g, "");
			emailSubscriberCount = emailSubscriberCount.toString().replace(/,/g, "");

			totalSubscribers = parseInt(facebookLikeCount) + parseInt(twitterFollowerCount) + parseInt(emailSubscriberCount);
			totalSubscribers = totalSubscribers.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

			jQuery('.totalSubscribers').text(totalSubscribers);
		}

	}) // Window Load 
}); // Document Ready

//########################
// Money In Your Pocket
//########################

// AMP slated for phase 2 if viable.  CSS/cookie/origin/session limitations.

if (typeof tph_thumbs_down == 'undefined') {
	var tph_thumbs_down = [];
}
if (typeof tph_thumbs_up == 'undefined') {
	var tph_thumbs_up = [];
}
if (typeof tph_user_email == 'undefined') {
	var tph_user_email = [];
}

function parsely_thumb_list(post_id) {
	// Parse the cookie into an array
	tph_thumbs_down = JSON.parse(tph.utility.cookie.read("tph_thumbs_down"));
	tph_thumbs_up = JSON.parse(tph.utility.cookie.read("tph_thumbs_up"));
	tph_user_email = JSON.parse(tph.utility.cookie.read("user_email"));

	// Is this post thumbed up already? 
	if (jQuery.inArray(post_id, tph_thumbs_up) >= 0) {
		// Don't display any Money in Your Pocket.
		jQuery("#money-in-your-pocket").hide();
		// Is email set in the cookie?
		// if ( tph_user_email.length > 1 ) {
		// }
	}

	// If the cookie is already set to thumbs down on this post, select it for this user.
	if (jQuery.inArray(post_id, tph_thumbs_down) >= 0) {
		jQuery(".money-in-your-pocket.thumbs-down").addClass("thumbs-down-selected").attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-down-selected.png");
		jQuery(".money-in-your-pocket.thumbs-up").attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-up-deselected.png");

	}
}

// Store users email in the cookie.
function parsely_user_email() {
	var user_email = jQuery('.at-newsletter-email-field.form-control').val();
	tph.utility.cookie.create('tph_user_email', user_email, 3650);
}

// User selects 'X'.
function money_in_your_pocket_hide() {

	jQuery("#money-in-your-pocket").hide();
}

function parsely_thumbs_up(post_id) {
	if (typeof tph_thumbs_up == 'undefined') {
		return; // not fully loaded yet
	}

	tph_thumbs_up.push(post_id);
	tph.utility.cookie.create("tph_thumbs_up", JSON.stringify(tph_thumbs_up), 3650);

	// Switch to selected image on thumbs up and disable further button pushing.
	jQuery(".money-in-your-pocket.thumbs-up").addClass("thumbs-up-selected").attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-up-selected.png")
	jQuery("#money-in-your-pocket-thumbs").attr("disabled", true);
	jQuery(".money-in-your-pocket.thumbs-down").attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-down-deselected.png")

	// Wait just half a second now.
	setTimeout(function () {
		jQuery("#money-in-your-pocket").addClass("flipped");
	}, 500);

	// Post ID, URL, Thumbs Up.
	parsely_submit_thumb(post_id, window.location.href, 1);
}

function parsely_thumbs_down(post_id) {
	if (typeof tph_thumbs_down == 'undefined') {
		return; // not fully loaded yet
	}

	// If it's already selected, deselect it, and continue.
	if (jQuery("#money-in-your-pocket-thumbs .thumbs-down").hasClass("thumbs-down-selected")) {
		jQuery(".money-in-your-pocket.thumbs-down-selected").removeClass("thumbs-down-selected").attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-down.png");
		// Switch to normal image on thumbs up.
		jQuery(".money-in-your-pocket.thumbs-up").attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-up.png");

		// Submit the post ID, URL, Thumbs Down Deselected.
		parsely_submit_thumb(post_id, window.location.href, 2);

		// Splice out that ID safely from the array
		var thumbs_index = jQuery.inArray(post_id, tph_thumbs_down);
		if (thumbs_index >= 0) {
			tph_thumbs_down.splice(thumbs_index, 1);
		}
		// Recreate the cookie without that ID.
		tph.utility.cookie.create("tph_thumbs_down", JSON.stringify(tph_thumbs_down), 3650);
	} else {

		jQuery(".money-in-your-pocket.thumbs-down").addClass("thumbs-down-selected").attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-down-selected.png");
		// Switch to deselected image for thumbs up.
		jQuery(".money-in-your-pocket.thumbs-up").attr("src", "/wp-content/themes/pennyhoarder/assets/images/miyp-thumbs-up-deselected.png");
		tph_thumbs_down.push(post_id);
		tph.utility.cookie.create("tph_thumbs_down", JSON.stringify(tph_thumbs_down), 3650);
		// Post ID, URL, and Thumbs Up/Down
		parsely_submit_thumb(post_id, window.location.href, 0);
	}

}

// Submit the data to the data pipeline.
// https://www.parse.ly/help/integration/dynamic/
// https://www.parse.ly/help/integration/validate/




function parsely_submit_thumb(post_id, url, thumb_status) {
	tph.utility.cookie.create('tph_parsely_UUID', parsely_uuid, 3650);
	// get post url
	var url = 'https://www.thepennyhoarder.com' + location.pathname;
	// get full host url
	// var urlref = 'https://www.thepennyhoarder.com';
	// and Date/Time in "YYYY-MM-DD HH:MM:SS
	var currentdate = new Date();
	var parsely_datetime = currentdate.getUTCFullYear() + "-" + (currentdate.getUTCMonth() + 1) + "-" + currentdate.getUTCDate() + " " + currentdate.getUTCHours() + ":" + currentdate.getUTCMinutes() + ":" + currentdate.getUTCSeconds();
	// logThis(document.cookie);
	// logThis( parsely_datetime );
	// Example result to the pipeline: 
	//  "extra_data": {"_thumb_status": 1, "_thumb_parsely_uuid": "########-####-####-####-############", "_thumb_post_id": "58971", "_thumb_timestamp": "2017-9-20 20:0:39", "_thumb_url": "https://www.thepennyhoarder.com/life/college/millennials-scared-to-invest/"}

	var beacon_data = {
		url: url,
		// urlref: urlref,
		// surl: urlref,
		action: '_miyp_submit_thumb',
		data: {
			_thumb_parsely_uuid: parsely_uuid,
			_thumb_post_id: post_id,
			_thumb_url: url,
			_thumb_status: thumb_status,
			_thumb_timestamp: parsely_datetime
		}
	};
	tph.utility.parsely.track.event(beacon_data);

	return true;

}


//##########################################################
// Add Parsely tracking to all header social icons
//##########################################################

function parsely_click_tracking(parsely_track) {
	if (typeof parsely_track == 'undefined'){
		logThis('Parsely click track passed with no data');
		return;
	}
	var url = 'https://www.thepennyhoarder.com' + location.pathname;
	var beacon_data = {
		url: url,
		// urlref: urlref,
		// surl: urlref,
		action: '_social_header_click',
		data: {
			_social_header_platform: parsely_track,
		}
	};
	tph.utility.parsely.track.event(beacon_data);
}

//##########################################################
// Add Parsely tracking to author page
//##########################################################

function parsely_author_name_click_tracking(parsely_track) {
	if (typeof parsely_track == 'undefined'){
		logThis('Parsely click track passed with no data');
		return;
	}
	var url = 'https://www.thepennyhoarder.com' + location.pathname;
	var beacon_data = {
		url: url,
		// urlref: urlref,
		// surl: urlref,
		action: '_author_name_click',
		data: {
			_author_name: parsely_track,
		}
	};
	tph.utility.parsely.track.event(beacon_data);
}

function parsely_author_page_post_click_tracking(parsely_track) {
	if (typeof parsely_track == 'undefined'){
		logThis('Parsely click track passed with no data');
		return;
	}
	var url = 'https://www.thepennyhoarder.com' + location.pathname;
	var beacon_data = {
		url: url,
		// urlref: urlref,
		// surl: urlref,
		action: '_author_page_post_click',
		data: {
			_author_page_post_url: parsely_track,
		}
	};
	tph.utility.parsely.track.event(beacon_data);
}

function parsely_author_page_featured_post_click(parsely_track) {
	if (typeof parsely_track == 'undefined'){
		logThis('Parsely click track passed with no data');
		return;
	}
	var url = 'https://www.thepennyhoarder.com' + location.pathname;
	var beacon_data = {
		url: url,
		// urlref: urlref,
		// surl: urlref,
		action: '_author_page_featured_post_click',
		data: {
			_author_page_featured_post_url: parsely_track,
		}
	};
	tph.utility.parsely.track.event(beacon_data);
}

//##########################################################
// Staff Page Tracking
//##########################################################

function parsely_staff_page_read_more_click_tracking(parsely_track) {
	if (typeof parsely_track == 'undefined'){
		logThis('Parsely click track passed with no data');
		return;
	}
	var url = 'https://www.thepennyhoarder.com' + location.pathname;
	var beacon_data = {
		url: url,
		// urlref: urlref,
		// surl: urlref,
		action: '_staff_read_more_click',
		data: {
			_author_name: parsely_track,
		}
	};
	fire_parsely_event(beacon_data);
}

//##########################################################
// Add Parsely tracking to all social share links on posts
//##########################################################

jQuery(".single-social li a, #parsely-track").on('click', function () {
	// if (typeof PARSELY != 'undefined' && typeof PARSELY.beacon != 'undefined') {
	// get post url
	var url = 'https://www.thepennyhoarder.com' + location.pathname;
	// get full host url
	// var urlref = 'https://www.thepennyhoarder.com';
	// get button classes from parent of this link so we know what platform we are sharing to
	var button_classes = jQuery(this).parent().attr('class');
	// only grab the first class (ie: at-top-fb-share)
	var button_location = button_classes.split(' ')[0];
	// check if clicked button is on sticky menu or not
	// if it is sticky, replace 'top' in class with 'sticky'
	if (jQuery(this).parents('.sticky-social').hasClass('fixed')) {
		var action_name = button_location.replace('top', 'sticky');
	} else {
		var action_name = button_location;
	}
	// append '_' to beginning of action name because that's how parsely wants the action value to start
	var action = '_' + action_name;
	// send data to parse.ly

	var beacon_data = {
		url: url,
		// urlref: urlref,
		// surl: urlref,
		action: '_social_share_click',
		data: {
			_share_icon_click: action,
		}
	};
	tph.utility.parsely.track.event(beacon_data);

	// Fire the data to GA as well
		var eventLabel = current_post_id;
		var eventCategory = "SocialShareClick";
		dataLayer.push({
			'socialMediaMedium': button_location,
			'clickPostID': eventLabel,
			'event': eventCategory
		});


	return true;

	// } else {
	// logThis('Parsely not ready, will not submit data.');
	// return false;
	// }


});

//##########################################################
// Add Parsely tracking to all header social icons on Housing project page
//##########################################################

function parsely_housing_project_click_tracking(parsely_track) {
	if (typeof parsely_track == 'undefined'){
		logThis('Parsely click track passed with no data');
		return;
	}
	var url = 'https://www.thepennyhoarder.com' + location.pathname;
	var beacon_data = {
		url: url,
		// urlref: urlref,
		// surl: urlref,
		action: '_housing_project_social_share',
		data: {
			_housing_project_social_share_platform: parsely_track,
		}
	};
	tph.utility.parsely.track.event(beacon_data);
}

//##########################################################
// Add Parsely tracking to all chapter links on Housing project page
//##########################################################

function parsely_housing_project_chapter_tracking(parsely_track) {
	if (typeof parsely_track == 'undefined'){
		logThis('Parsely click track passed with no data');
		return;
	}
	var url = 'https://www.thepennyhoarder.com' + location.pathname;
	var beacon_data = {
		url: url,
		// urlref: urlref,
		// surl: urlref,
		action: '_housing_project_chapter_nav_click',
		data: {
			_housing_project_chapter_nav_label: parsely_track,
		}
	};
	tph.utility.parsely.track.event(beacon_data);
}


//##########################################################
// Recommended Content
//##########################################################

function rec_content_block_c() {

	// get unique user identifier from parsely
	var uuid = PARSELY.config.parsely_site_uuid;

	var parsely_related_content_url = 'https://api.parsely.com/v2/related?apikey=thepennyhoarder.com&uuid=' + uuid + '&limit=25';

	var posts = [];

	var post_titles = [];

	var debug = get_browser_query_parameter('tph_debug_block_c');

	// logThis('UUID: ' + uuid);
	// logThis('PRS URL: ' + parsely_related_content_url);

	// api call to parsely to get all recommended posts based on uuid. 
	// we are getting 11 posts. 10 to display, and 1 for back up in case current post is a post in the returned list
	jQuery.get(parsely_related_content_url, function (response, status) {

		if (debug == true) {

			jQuery('.debug-c').append("<pre>Parsely UUID: " + PARSELY.config.parsely_site_uuid + "</pre>");

			jQuery('.debug-c').append("<pre>Parsely Get Request Status: " + status + "</pre>");

			jQuery('.debug-c').append("<pre>Parsely Response: " + JSON.stringify(response) + "</pre>");

		}

		// remove what was already in subcategory element to make way for unique posts
		// jQuery('.category-subcategory-post').remove();

		var count = 0;

		// lets loop through each returned post
		jQuery.each(response.data, function (i, post) {

			// get location path from returned url
			response_path = post.url.replace('https://www.thepennyhoarder.com', '');

			// make sure we don't show any posts from staging
			// also make sure we don't show a post if we are currently on it
			// finally, make sure we have no posts with duplicate titles in block c
			// also make sure we have no posts with duplicate titles in block b
			if (response_path != location.pathname && post.url.indexOf('https://www.thepennyhoarder.com') != -1 && post_titles.indexOf(post.title) == -1 && jQuery('.block-b').text().indexOf(post.title) == -1) {

				count++;

				// if we have shown less than 10 posts, keep going until we reach the max
				if (count <= 10) {

					// add post title to array so we can check to make sure there are no duplicates
					post_titles.push(post.title);

					// add post title, url and image to array to send through ajax
					posts.push({ url: post.url, image: post.image_url, title: post.title });

				}

			}

		});

		// array of all post data
		var postData = {
			posts: posts,
		}

		var expected_posts_returned = '11';

		jQuery('.debug-c').append("<pre>Expected Unique Posts Returned: " + expected_posts_returned + "</pre>");
		jQuery('.debug-c').append("<pre>Actual Unique Posts Returned: " + count + "</pre>");

		if (count >= expected_posts_returned) {
			jQuery('.debug-c').append("<pre>Passed. Expected amount or more posts have been returned by Parsely.</pre>");
		} else {
			jQuery('.debug-c').append("<pre>Failed. Not enough posts returned from Parsely.</pre>");
		}

	});
}

function rec_content_frontend() {

	if(jQuery('.category-subcategory-title').hasClass('recommended-on')){

		var aff_id = tph.page_data.aff_id;

		// get aff_sub2 (path basename) by splitting pathname and returning last value in array
		if (get_browser_query_parameter('aff_sub2') != '' && get_browser_query_parameter('aff_sub2') != false) {
			var aff_sub2 = get_browser_query_parameter('aff_sub2');
		} else {
			var aff_sub2 = window.location.pathname.split('/').filter(function (el) { return !!el; }).pop();
		}

		var utm = '';

		if (get_browser_query_parameter('utm_source') != '' && get_browser_query_parameter('utm_source') != false) {
			utm = utm + '&utm_source=' + get_browser_query_parameter('utm_source');
		}

		if (get_browser_query_parameter('utm_medium') != '' && get_browser_query_parameter('utm_medium') != false) {
			utm = utm + '&utm_medium=' + get_browser_query_parameter('utm_medium');
		}

		// get aff_sub3 from url parameter
		if (get_browser_query_parameter('aff_sub3') != '' && get_browser_query_parameter('aff_sub3') != false) {
			var aff_sub3 = get_browser_query_parameter('aff_sub3');
			// else, if aff_id is empty, set it to the default of 76
		} else {
			var aff_sub3 = '';
		}

		if (get_browser_query_parameter('aff_unique3') != '' && get_browser_query_parameter('aff_unique3') != false) {
			var aff_unique3 = get_browser_query_parameter('aff_unique3');
		} else {
			var aff_unique3 = '';
		}

		if (get_browser_query_parameter('aff_unique4') != '' && get_browser_query_parameter('aff_unique4') != false) {
			var aff_unique4 = get_browser_query_parameter('aff_unique4');
		} else {
			var aff_unique4 = '';
		}

		// var aff_id,aff_sub2,utm,aff_sub3,aff_sub,aff_unique3,aff_unique4;

		// get unique user identifier from parsely
		var uuid = PARSELY.config.parsely_site_uuid;

		var parsely_related_content_url = 'https://api.parsely.com/v2/related?apikey=thepennyhoarder.com&uuid=' + uuid + '&limit=25';

		var post_titles = [];
		var post_urls = [];
		var count_accepted = 0;
		var we_are_done_inserting_posts = false;
		
		var $dom_target = jQuery('.category-subcategory-title.recommended-on').closest('.category-subcategory-wrapper').find('.category-subcategory-posts');
		var $tmp_dom = jQuery('<div><span class="category-subcategory-posts" data-interaction="single-posts-below-content-suggested-for-you"></span></div>');

        jQuery('.category-subcategory-title.recommended-on').closest('.category-subcategory-wrapper').find('.category-subcategory-posts').addClass('suggested-for-you-target');
		jQuery('.category-subcategory-title.recommended-on').closest('.category-subcategory-wrapper').find('.category-subcategory-posts').attr('data-interaction', 'single-posts-below-content-suggested-for-you');
		logThis('Suggested for you '+'PRS URL: ' + parsely_related_content_url);

		// api call to parsely to get all recommended posts based on uuid. 
		// we are getting 11 posts. 10 to display, and 1 for back up in case current post is a post in the returned list
		var live_posts = [];
		jQuery.get(parsely_related_content_url, function (response, status) {
			if (status == 'success') {

				var processed_count = 0;
				var original_count = response.data.length;

				function check_if_post_is_published_promise(post_url, post_data){
					return new Promise(function(resolve, reject) {
						if(we_are_done_inserting_posts) {
							return resolve(true);
						}
						try {
							jQuery.get(post_url, function(response){
								// logThis('Suggested for you '+ 'status for ' + post_url, response );
								if ( response == 'publish' ) { 
									insert_suggested_for_you_article(post_data);
									// live_posts.push(post_data);
									resolve(live_posts);
								} else { 
									resolve('Not Published '+post_url);
								}
							})
							
						} catch(err_caught){
							resolve('Not Published '+post_url);
						}
							
							
					});
				}

				response.data.forEach(function(post_data){

					if (post_data['url'].indexOf('www.thepennyhoarder.com/') != -1) {
						
						var post_identifier = 'https://'+window.location.hostname+'/?tph_run_service=get_post_status&post_url=' + encodeURIComponent(post_data['url'].replace('www.thepennyhoarder.com', window.location.hostname).replace(/\/$/, ""));
						check_if_post_is_published_promise(post_identifier, post_data).then(function(live_posts){
						}).catch(function(status_error){
						});
					} 
				});

				function insert_suggested_for_you_article(post) {
					if (count_accepted == 10) {
						if (jQuery('.suggested-for-you-target').length == 1) {
							we_are_done_inserting_posts = true;
							jQuery('.suggested-for-you-target').removeClass('suggested-for-you-target')
							var content_to_insert = jQuery($tmp_dom).html();
							$dom_target.html( content_to_insert );
						} 
						return;
					} 
					logThis('Suggested for you '+ 'published post', post.title );

					// get location path from returned url
					response_path = post.url.replace('https://www.thepennyhoarder.com', '');

					current_post_title = jQuery('.single-post-title').text();

					// make sure we don't show any posts from anywhere but production
					// also make sure we don't show a post if we are currently on it by checking current path vs post path and current title vs post title
					// finally, make sure we have no posts with duplicate titles in block c
					// also make sure we have no posts with duplicate titles in block b
					// make sure no posts without an image show up
					// also make sure no posts with duplicate urls appear
					var $check_1 = (response_path != window.location.pathname  );
					var $check_2 = (post.title != current_post_title  );
					var $check_3 = (post.url.includes('//www.thepennyhoarder.com') == true  );
					var $check_4 = (!post_titles.includes(post.title));
					if ($check_4 == false) {
					}
					var $check_5 = (jQuery('.block-b').text().indexOf(post.title) == -1 ); 
					var $check_6 = (post.image_url != ''  );
					var $check_7 = (!post_urls.includes(post.url));
					if ($check_7 == false) {
					}
					// logThis('Suggested for you '+'1', $check_1);
					// logThis('Suggested for you '+'2', $check_2);
					// logThis('Suggested for you '+'3', $check_3);
					// logThis('Suggested for you '+'4', $check_4);
					// logThis('Suggested for you '+'5', $check_5);
					// logThis('Suggested for you '+'6', $check_6);
					// logThis('Suggested for you '+'7', $check_7);
					if ($check_1 && $check_2 && $check_3 && $check_4 && $check_5 && $check_6 && $check_7) {

						// if we have shown less than 10 posts, keep going until we reach the max
						if (count_accepted < 10) {
							count_accepted++;

							// add post title to array so we can check to make sure there are no duplicates
							post_titles.push(post.title);

							// add post url to array so we can check to make sure there are no duplicates
							post_urls.push(post.url);

							// format url to include block, block position, post id and current post id
							// ie: rc=c-2-8292-3733
							// if aff_sub3 is not empty, add it to url along with dynamic aff_id and aff_sub2
							if (aff_sub3 != '') {
								url = post.url + '?aff_id=' + aff_id + utm + '&aff_sub2=' + aff_sub2 + '&aff_sub3=' + aff_sub3 + '&rc=on-c-' + count_accepted + '-' + current_post_id;
								// else, only add dynamic aff_id and aff_sub2 to url
							} else {
								url = post.url + '?aff_id=' + aff_id + utm + '&aff_sub2=' + aff_sub2 + '&rc=on-c-' + count_accepted + '-' + current_post_id;
							}

							url = url + '&aff_sub=rc-on-c-' + count_accepted + '-' + current_post_id;

							if (aff_unique3 != '') {
								url = url + '&aff_unique3='+aff_unique3;
							}

							if (aff_unique4 != '') {
								url = url + '&aff_unique4='+aff_unique4;
							}

							url = url.replace('http://', 'https://');
							
							// classes for mobile sizes
							// if first post, make full width
							if (count_accepted <= 1) {
								var mobile_class = 'col-xs-12';
								// else, if post is after #5, hide on sm and xs
							} else if (count_accepted > 5) {
								var mobile_class = 'col-xs-6 hidden-xs hidden-sm';
								// else, just make half width
							} else {
								var mobile_class = 'col-xs-6';
							}

							// append post image, title, and link inside subcategory-post element
							jQuery('.category-subcategory-posts', $tmp_dom).append('<div data-interaction="single-posts-below-content-suggested-for-you-' + count_accepted + '" class="' + mobile_class + ' col-md-2 category-subcategory-post text-center block-c"><div class="col-xs-12 category-subcategory-post-image"><a data-interaction="single-posts-below-content-suggested-for-you-' + count_accepted + '-thumbnail" class="recommended-' + count_accepted + '" href=' + url + '></a></div><div class="col-xs-12 category-subcategory-post-content"><a data-interaction="single-posts-below-content-suggested-for-you-' + count_accepted + '-title"  href=' + url + ' class="photo-essay-article-content-title">' + post.title + '</a></div></div>');

							// append picture inside of link so it does not turn out empty
							jQuery('.recommended-' + count_accepted, $tmp_dom).append('<picture class="four_by_six"><img class="lazyload" src=' + post.image_url + ' alt=' + post.title + '></picture>');
						} 

					} else {
						logThis('Suggested for you '+ 'rejected post', post.title );
					}


				}

			}
		});
	}
}



//##########################################################
// Add dynamic stats functions
//##########################################################

/***
* Helper function. Formats the numeric values in a nicer way
* @param val - numeric value
* @returns {string} - string with thousands operator added (comma)
*/
function add_commas_to_dynamic_stats(val) {
	var rx = /(\d+)(\d{3})/;
	return String(val).replace(/^\d+/, function (w) {
		while (rx.test(w)) {
			w = w.replace(rx, '$1,$2');
		}
		return w;
	});
}

/***
* This function is able to get stats at runtime (dynamic stats)
* @param {string} type - facebook_count, total_subscribers, etc
* @param {function} callback_function - function to call when stats are available; (type, value) parameters are provided to the callback function
* @param {int} decimals - number of decimal places to use
* @param {string} format - special formatting option . i.e. M for Millions, K for thousands
* @returns {string} - obtained stat value
*/
function update_dynamic_stats(type, callback_function, decimals, format) {
	if (!decimals) {
		decimals = 0
	}
	var allowed_types = ['facebook_count', 'twitter_count', 'pinterest_count', 'email_count', 'total_subscribers_count', 'monthly_uniques', 'monthly_uniques_and_sessions'];
	if (allowed_types.indexOf(type) < 0) {
		// invalid type provided
		return;
	}

	var val = '';

	if (type === 'facebook_count') {
		val = jQuery('#facebookCountCurrent').html();
	} else if (type === 'twitter_count') {
		val = jQuery('#twitterCountCurrent').html();
	} else if (type === 'pinterest_count') {
		val = jQuery('#pinterestCountCurrent').html();
	} else if (type === 'email_count') {
		val = jQuery('#emailCountCurrent').html();
	} else if (type === 'total_subscribers_count') {
		val = jQuery('#totalSubscribersCountCurrent').html();
	} else if (type === 'monthly_uniques') {
		val = jQuery('#monthlyVisitorsCurrent').html();
	} else if (type === 'monthly_uniques_and_sessions') {
		val = jQuery('#monthlyPageviewsCurrent').html();
	}

	// remove thousands operators (if any):
	var re = new RegExp(',', 'g');
	val = val.replace(re, '');

	val = parseFloat(val);

	if (format && format.toUpperCase() === 'M') {
		val = val * 1.0;
		val /= 1000000.0;
	} else if (format && format.toUpperCase() === 'K') {
		val = val * 1.0;
		val /= 1000.0;
	}

	var ret_val = val.toFixed(decimals);
	ret_val = add_commas_to_dynamic_stats(ret_val);

	if (callback_function) {
		callback_function(type, ret_val);
	}
}

//##########################################################
// Replace logout URLs on frontend so they don't throw 404
//##########################################################

/*jQuery(document).ready(function ($) {
	// if wp-admin-bar-logout exists (user is logged in)
	if ($('#wp-admin-bar-logout').length) {
		logout_element = $('#wp-admin-bar-logout').find('a');
		logout_link = logout_element.attr('href');

		// if logoout_link contains 'action=logout', fix url then replace it
		if (logout_link.indexOf("action=logout") >= 0) {
			new_url = logout_link.replace('login', 'wp-login.php');
			$(logout_element).attr('href', new_url);
		}
	}
});*/

//##########################################################
// Check if element is visible in the viewport
//##########################################################

jQuery.fn.is_element_in_viewport = function() {

	// get top position of the element
	var element_top = jQuery(this).offset().top;

	// get bottom position of element = height of element + element top position
	var element_bottom = element_top + jQuery(this).outerHeight();

	// get top of viewport position
	var viewport_top = jQuery(window).scrollTop();

	// get bottom position of viewport = height of window + viewport top position
	var viewport_bottom = viewport_top + jQuery(window).height();

	// check if element bottom position is higher than top of viewport position
	// and
	// element top position is less than viewport bottom position
	return element_bottom > viewport_top && element_top < viewport_bottom;

};
//##########################################################
// Track clicks on Author names and posts on author's page
//##########################################################

jQuery(document).ready(function ($) {
	$('.single-post-author a').on('click', function() {
		parsely_author_name_click_tracking( $(this).text() );
	});

	$('.author-page .author-row a').on('click', function() {
		parsely_author_page_post_click_tracking( $(this).attr('href') );
	});

	$('.author-page .author-featured-block a').on('click', function() {
		parsely_author_page_featured_post_click( $(this).attr('href') );
	});

});
jQuery(document).ready(function($){
	jQuery(".four_by_six img").on("error", function() {
		if (typeof jQuery(this).attr('data-srcset') != 'undefined') {
			$(this).removeClass().removeAttr('data-src data-srcset srcset').attr('src', 'https://cdn.thepennyhoarder.com/wp-content/themes/pennyhoarder/assets/images/placeholder.jpg');
		}
	});
});

// jQuery(document).ready(function($){
//     jQuery("img").on("error", function(){
//         if (typeof jQuery(this).attr('data-srcset') != 'undefined' && !jQuery(this).hasClass('fixed-bad-load')) {
//             jQuery(this).addClass('fixed-bad-load')
//             var newHTML = jQuery(this).closest('div').html().replace(/staging\.thepennyhoarder\.com/g, 'cdn.thepennyhoarder.com');
//             jQuery(this).closest('div').html(newHTML);
//         }
//     });
// });
//##########################################################
// Track clicks on Read more buttons on Staff page
//##########################################################

jQuery(document).ready(function ($) {

	$('.staff-page .btn-staff-read-more').on('click', function() {
		parsely_staff_page_read_more_click_tracking( $.trim( $(this).parent().siblings('.staff-author-name').text() ) );
	});

});

