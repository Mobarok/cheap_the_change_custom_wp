<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/4/2019
 * Time: 9:24 PM
 *
 * Template Name: Currency Converter
 */
get_header();
?>


<div class="page-container category-page">

    <div class="row breadcrumbs flex-row">
        <div class="container flex-container">
            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">

                <span property="itemListElement" typeof="ListItem">
                    <a property="item" typeof="WebPage" title="Go to Cheap The Change" href="<?php bloginfo('home')?>" class="home">
                        <span property="name">Home</span>
                    </a>
                    <meta property="position" content="1"></span>
                <span>&gt;</span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name">
                        <?php
                        echo get_the_title($post->ID);
                        ?></span>
                    <meta property="position" content="2"></span>
            </div>
        </div>
    </div>

    <div class="row category-trending  currency-page flex-row">
        <div class="container flex-container">
    <?php
    echo do_shortcode("[currency_bcc w=\"300\" h=\"300\" s=\"0cbb93\" fc=\"FFFFFF\" f=\"USD\" t=\"EUR\" type=\"custom\"]");

    ?>
        </div>
    </div>
            <?php


//    echo do_shortcode("[footer_image_navigation]");

    ?>
</div>
<script type="text/javascript">

    jQuery(document).ready(function(){
        jQuery(".i.c > label").css("font-size", "17px !important");
        jQuery(".c > p").css("font-size", "17px !important");
    });

</script>

<?php
get_footer();
?>

