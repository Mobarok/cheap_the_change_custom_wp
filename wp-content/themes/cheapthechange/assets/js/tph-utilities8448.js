'use strict';

if (typeof tph != 'undefined' && typeof tph.utility == 'undefined') {

    tph.utility = {};
    tph.temp_data = {};
    // POLYFILL FOR OLDER BROWSERS
    // enables newer JS functionality for older browsers
    // Source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes
    if (!Array.prototype.includes) {
        Object.defineProperty(Array.prototype, 'includes', {
            value: function(searchElement, fromIndex) {

                if (this == null) {
                    throw new TypeError('"this" is null or not defined');
                }

                // 1. Let O be ? ToObject(this value).
                var o = Object(this);

                // 2. Let len be ? ToLength(? Get(O, "length")).
                var len = o.length >>> 0;

                // 3. If len is 0, return false.
                if (len === 0) {
                    return false;
                }

                // 4. Let n be ? ToInteger(fromIndex).
                //    (If fromIndex is undefined, this step produces the value 0.)
                var n = fromIndex | 0;

                // 5. If n <= 0, then
                //  a. Let k be n.
                // 6. Else n < 0,
                //  a. Let k be len + n.
                //  b. If k < 0, let k be 0.
                var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

                function sameValueZero(x, y) {
                    return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
                }

                // 7. Repeat, while k < len
                while (k < len) {
                    // a. Let elementK be the result of ? Get(O, ! ToString(k)).
                    // b. If SameValueZero(searchElement, elementK) is true, return true.
                    if (sameValueZero(o[k], searchElement)) {
                        return true;
                    }
                    // c. Increase k by 1. 
                    k++;
                }

                // 8. Return false
                return false;
            }
        });
    };

    jQuery(document).ready(function($) {

        // strings
        tph.utility.get_param_from_string = function(key, source_string) {
            var match = RegExp('[?&]' + key + '=([^&]*)').exec(source_string);
            var got_value = match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            if (got_value) {
                return got_value;
            } else {
                return false;
            }
        };
        tph.utility.remove_param_from_string = function(key, source_string) {
            if (source_string.indexOf('?') > 0) {
                var url = new URL(source_string);
                var params = url.searchParams;
                var connector = '?';
            } else if (source_string.indexOf('?') == 0) {
                source_string = 'https://www.placeholder.com/' + source_string;
                var url = new URL(source_string);
                var params = url.searchParams;
                var connector = '?';
            } else {
                source_string = 'https://www.placeholder.com/?' + source_string;
                var url = new URL(source_string);
                var params = url.searchParams;
                var connector = '';
            }

            if (params.has(key)) {
                params.delete(key);
                params = params.toString();
                url = url.href.split('?')[0];
                if (url.indexOf('placeholder') >= 0) {
                    url = '';
                }
                return url + connector + params;
            } else {
                return source_string;
            }

        };
        tph.utility.add_param_to_string = function(key, value, source_string) {
            if (source_string.indexOf('?') > 0) {
                var url = new URL(source_string);
                var params = url.searchParams;
                var connector = '?';
            } else if (source_string.indexOf('?') == 0) {
                source_string = 'https://www.placeholder.com/' + source_string;
                var url = new URL(source_string);
                var params = url.searchParams;
                var connector = '?';
            } else {
                source_string = 'https://www.placeholder.com/?' + source_string;
                var url = new URL(source_string);
                var params = url.searchParams;
                var connector = '';
            }

            params.append(key, value);
            params = params.toString();
            url = url.href.split('?')[0];
            if (url.indexOf('placeholder') >= 0) {
                url = '';
            }
            return url + connector + params;
        };

        tph.utility.update_param_in_string = function(key, value, source_string) {
            source_string = tph.utility.remove_param_from_string(key, source_string);
            source_string = tph.utility.add_param_to_string(key, value, source_string);
            return source_string;

        };


        // browser
        tph.utility.get_browser_query_parameter = function(key) {
            const searchParams = new URLSearchParams(window.location.search)
            if (searchParams.has(key)) {
                return String(searchParams.get(key));
            } else {
                return false;
            }
        };

        tph.utility.update_page_title = function(new_title, target_element) {
            if (document.title != new_title) {
                if ($(target_element).length >= 1) {
                    $(target_element).html(new_title);
                }
                document.title = new_title;
            }
        };

        tph.utility.update_browser_url = function(new_url_relative, new_page_title) {

            // get the current path, everything after the domain. 
            // example: /wfh/?keyword=qa
            var current_path = window.location.href.replace(window.location.origin, '');

            if (new_url_relative == current_path) {
                // trying to load the current page! Do nothing
                // console.log('trying to load the current page! Do nothing');

            } else {

                history.pushState({}, null, new_url_relative);
            }

            if (typeof new_page_title != 'undefined') {
                // no new title, just change the URL
                tph.utility.update_page_title(new_page_title);
            }
        };

        tph.utility.update_browser_url_with_reload = function(new_url_relative, new_page_title) {
            tph.utility.update_browser_url(new_url_relative, new_page_title);
            window.location.reload();
        };

        tph.utility.update_browser_query_parameter = function(key, value) {
            var new_query = tph.utility.update_param_in_string(key, value, window.location.search);
            var browser_url_no_query_vars = window.location.href.split('?')[0];
            var new_url = browser_url_no_query_vars + new_query;
            tph.utility.update_browser_url(new_url);
        };

        tph.utility.remove_browser_query_parameter = function(key) {
            if (tph.utility.get_browser_query_parameter(key)) {
                var new_browser_query = tph.utility.remove_param_from_string(key, window.location.search);
                if (new_browser_query != '?' && new_browser_query.indexOf('?') != -1) {
                    var browser_url_no_query_vars = window.location.href.split('?')[0];
                    var new_url = browser_url_no_query_vars + '?' + new_browser_query;
                } else {
                    var new_url = browser_url_no_query_vars;

                }
                tph.utility.update_browser_url(new_url);

            } else {
                // if no value matching the requested parameter is found do nothing
            }
        };

        tph.utility.get_params_from_url_as_object = function(urlWithParams, uriEncodedResult) {
            // set up get variables to use later
            if (urlWithParams.indexOf('?') === 0) {
                // query string only passed
                var queryString = urlWithParams.substring(1);
            } else if (urlWithParams.indexOf('?') != -1) {
                var queryString = urlWithParams.substr((urlWithParams.indexOf('?') + 1));
            } else if (urlWithParams.indexOf('&') != -1) {
                var queryString = urlWithParams;
            }

            $_GET = {};
            var page_query_string = decodeURIComponent(queryString);
            var get_variables = page_query_string.split('&');
            var query_variable, i;

            for (i = 0; i < get_variables.length; i++) {
                query_variable = get_variables[i].split('=');
                if (uriEncodedResult) {
                    $_GET[query_variable[0]] = encodeURIComponent(query_variable[1]);
                } else {
                    $_GET[query_variable[0]] = query_variable[1];
                }
            }

            return $_GET;
        };

        tph.utility.get_browser_query_variables_as_object = function() {
            return tph.utility.get_params_from_url_as_object(window.location.search);
        };

        // misc
        tph.utility.sleep = function(ms) {

            /*
            	Example
                tph.utility.sleep(1000).then(function(){ 
                	console.log(response); 
                });

            */

            return new Promise(function(resolve) {
                setTimeout(resolve(true), ms);
            });
        };

        tph.utility.number_value = function(number_string) {
            // takes a string like post_id_123 and returns 123

            if (!isNaN(number_string) && typeof number_string != 'undefined') {
                if (number_string == 0) {
                    return number_string;
                } else {
                    tph.utility.admin_console_log(number_string);
                    tph.utility.admin_console_log(typeof number_string);
                    var number_value = parseInt(number_string.match(/\d+/));
                    if (isNaN(number_value)) {
                        return 0;
                    } else {
                        return number_value;
                    }
                }
            } else {
                return number_string;
            }
        };

        tph.utility.performance_check = function(action_string) {
            if (typeof action_string == 'undefined') {
                var action_string = 'Last action';
            } else {
                var action_string = "The action " + action_string;
            }

            if (typeof tph.temp_data['performance_check_' + action_string] == 'undefined') {
                tph.temp_data['performance_check_' + action_string] = (+new Date());
                // console.log('Started performance check for '+action_string );
            } else {
                var speed = ((+new Date()) - tph.temp_data['performance_check_' + action_string]);
                tph.utility.debug.log(action_string + ' took ' + speed + 'ms');
                return speed;
            }
        };

        tph.utility.api = function(api_payload) {
            return new Promise(function(resolve, reject) {
                var call_key = 'API call ' + JSON.stringify(api_payload);
                // tph.utility.performance_check(call_key);
                // be sure that we have a valid object for method_data, if not, create an empty object
                if (typeof method_data == 'undefined') {
                    var method_data = {
                        'data': typeof method_data
                    };
                } else if (typeof method_data == 'string') {
                    var method_data = {
                        'data': method_data
                    };
                }

                if (window.location.hostname == 'www.thepennyhoarder.com') {
                    method_data['db_server'] = "prod";
                } else {
                    method_data['db_server'] = "staging";
                }

                if (typeof method_data['extra_data'] == 'undefined') {
                    method_data['extra_data'] = {};
                }

                method_data['extra_data']['request_source'] = window.location.hostname;

                var api_url = 'https://api.thepennyhoarder.com';

                $.ajax({
                    // headers: { 'tph-api-key': master_api_key },
                    type: "POST",
                    url: api_url,
                    data: api_payload,
                    dataType: 'json',
                    success: function(worker_reply) {
                        // tph.utility.performance_check(call_key);
                        resolve(worker_reply);
                    },
                    error: function(worker_reply) {
                        reject(worker_reply);
                    },
                    complete: function(worker_reply) {},
                });
            });
        };

        tph.utility.date_string_to_timestamp = function(date_string) {
            if (typeof date_string == 'string') {
                return (new Date(date_string)).getTime() / 1000;
            } else {
                return false;
            }
        };

        tph.utility.base64_decode = function(str) {
            return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
            }).join(''))
        };

        tph.utility.wpautop = function(content, br) {

            // source: https://github.com/andymantell/node-wpautop
            function _autop_newline_preservation_helper(matches) {
                return matches[0].replace("\n", "<WPPreserveNewline />");
            }

            if (typeof(br) === 'undefined') {
                br = true;
            }

            var pre_tags = {};
            if (content.trim() === '') {
                return '';
            }

            content = content + "\n"; // just to make things a little easier, pad the end
            if (content.indexOf('<pre') > -1) {
                var content_parts = content.split('</pre>');
                var last_content = content_parts.pop();
                content = '';
                content_parts.forEach(function(content_part, index) {
                    var start = content_part.indexOf('<pre');

                    // Malformed html?
                    if (start === -1) {
                        content += content_part;
                        return;
                    }

                    var name = "<pre wp-pre-tag-" + index + "></pre>";
                    pre_tags[name] = content_part.substr(start) + '</pre>';
                    content += content_part.substr(0, start) + name;

                });

                content += last_content;
            }

            content = content.replace(/<br \/>\s*<br \/>/, "\n\n");

            // Space things out a little
            var allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|form|map|area|blockquote|address|math|style|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';
            content = content.replace(new RegExp('(<' + allblocks + '[^>]*>)', 'gmi'), "\n$1");
            content = content.replace(new RegExp('(</' + allblocks + '>)', 'gmi'), "$1\n\n");
            content = content.replace(/\r\n|\r/, "\n"); // cross-platform newlines

            if (content.indexOf('<option') > -1) {
                // no P/BR around option
                content = content.replace(/\s*<option'/gmi, '<option');
                content = content.replace(/<\/option>\s*/gmi, '</option>');
            }

            if (content.indexOf('</object>') > -1) {
                // no P/BR around param and embed
                content = content.replace(/(<object[^>]*>)\s*/gmi, '$1');
                content = content.replace(/\s*<\/object>/gmi, '</object>');
                content = content.replace(/\s*(<\/?(?:param|embed)[^>]*>)\s*/gmi, '$1');
            }

            if (content.indexOf('<source') > -1 || content.indexOf('<track') > -1) {
                // no P/BR around source and track
                content = content.replace(/([<\[](?:audio|video)[^>\]]*[>\]])\s*/gmi, '$1');
                content = content.replace(/\s*([<\[]\/(?:audio|video)[>\]])/gmi, '$1');
                content = content.replace(/\s*(<(?:source|track)[^>]*>)\s*/gmi, '$1');
            }

            content = content.replace(/\n\n+/gmi, "\n\n"); // take care of duplicates

            // make paragraphs, including one at the end
            var contents = content.split(/\n\s*\n/);
            content = '';
            contents.forEach(function(tinkle) {
                content += '<p>' + tinkle.replace(/^\s+|\s+$/g, '') + "</p>\n";
            });

            content = content.replace(/<p>\s*<\/p>/gmi, ''); // under certain strange conditions it could create a P of entirely whitespace
            content = content.replace(/<p>([^<]+)<\/(div|address|form)>/gmi, "<p>$1</p></$2>");
            content = content.replace(new RegExp('<p>\s*(</?' + allblocks + '[^>]*>)\s*</p>', 'gmi'), "$1", content); // don't content all over a tag
            content = content.replace(/<p>(<li.+?)<\/p>/gmi, "$1"); // problem with nested lists
            content = content.replace(/<p><blockquote([^>]*)>/gmi, "<blockquote$1><p>");
            content = content.replace(/<\/blockquote><\/p>/gmi, '</p></blockquote>');
            content = content.replace(new RegExp('<p>\s*(</?' + allblocks + '[^>]*>)', 'gmi'), "$1");
            content = content.replace(new RegExp('(</?' + allblocks + '[^>]*>)\s*</p>', 'gmi'), "$1");

            if (br) {
                content = content.replace(/<(script|style)(?:.|\n)*?<\/\\1>/gmi, _autop_newline_preservation_helper); // /s modifier from php PCRE regexp replaced with (?:.|\n)
                content = content.replace(/(<br \/>)?\s*\n/gmi, "<br />\n"); // optionally make line breaks
                content = content.replace('<WPPreserveNewline />', "\n");
            }

            content = content.replace(new RegExp('(</?' + allblocks + '[^>]*>)\s*<br />', 'gmi'), "$1");
            content = content.replace(/<br \/>(\s*<\/?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)/gmi, '$1');
            content = content.replace(/\n<\/p>$/gmi, '</p>');

            if (Object.keys(pre_tags).length) {
                content = content.replace(new RegExp(Object.keys(pre_tags).join('|'), "gi"), function(matched) {
                    return pre_tags[matched];
                });
            }

            return content;
        };

        tph.utility.load_script = function(script_url, location, async) {
            if (!location) {
                var location = 'head';
            }
            var script = document.createElement('script');
            script.src = script_url;
            if (async) {
                script.async = 'true';
            }
            document[location].appendChild(script);
        };

        // page modifications
        tph.utility.lazy_load_background = function(element_to_lazyload_background) {
            function is_visible_on_screen(elem) {
                if ($(elem).is(':visible')) {
                    return true;
                }
                if ($(elem).length == 0) {
                    return false;
                }
                var docViewTop = $(window).scrollTop();
                var docViewBottom = docViewTop + $(window).height();
                var elemTop = $(elem).offset().top;
                var elemBottom = elemTop + $(elem).height();
                return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
            }
            if (is_visible_on_screen(element_to_lazyload_background)) {
                $(element_to_lazyload_background).each(function() {
                    if (typeof $(this).attr('data-lazy-load-bg-class') != 'undefined') {
                        $(this).removeClass('lazy-load-background');
                        var class_to_add = $(this).attr('data-lazy-load-bg-class');
                        $(this).addClass(class_to_add);
                    }
                })
            }
        };

        // screen size
        tph.utility.screen_size_is_at_most = function(pixels_wide) {
            // just make sure a valid number over 5 was passed
            // 5 just means it's for sure a number passed, no one should ever pass 5

            if (pixels_wide && parseInt(pixels_wide) > 5) {
                return ($('html').width() <= pixels_wide);
            } else {
                return false;
            }
        };

        tph.utility.screen_size_is_at_least = function(pixels_wide) {
            // just make sure a valid number over 5 was passed
            // 5 just means it's for sure a number passed, no one should ever pass 5

            if (pixels_wide && parseInt(pixels_wide) > 5) {
                return ($('html').width() >= pixels_wide);
            } else {
                return false;
            }
        };

        tph.utility.screen_size_is_over = function(pixels_wide) {
            // just make sure a valid number over 5 was passed
            // 5 just means it's for sure a number passed, no one should ever pass 5

            if (pixels_wide && parseInt(pixels_wide) > 5) {
                return ($('html').width() > parseInt(pixels_wide));
            } else {
                return false;
            }
        };

        tph.utility.screen_size_is_under = function(pixels_wide) {
            // just make sure a valid number over 5 was passed
            // 5 just means it's for sure a number passed, no one should ever pass 5

            if (pixels_wide && parseInt(pixels_wide) > 5) {
                return ($('html').width() < parseInt(pixels_wide));
            } else {
                return false;
            }
        };


        // debugging
        tph.utility.debug = {};
        tph.utility.debug.log = function() {
            if (typeof arguments != 'undefined' && (get_browser_query_parameter('debug'))) {
                for (var i = 0; i < arguments.length; i++) {
                    console.log("DEBUG [" + typeof arguments[i] + "]", arguments[i]);
                }
            }
        };

        // Google Tag Manager - GTM
        tph.utility.gtm = {};
        tph.utility.gtm.track = {};
        tph.utility.gtm.track.pageview = function(event_data) {
            var sample_event_data = {
                'virtualPageURL': window.location.pathname + window.location.search,
                'virtualPageTitle': document.title
            };

            if (typeof event_data == 'undefined') {
                var event_data = sample_event_data;
            }

            event_data.event = 'VirtualPageview';

            // gtm trigger virtual pageview
            tph.utility.gtm.track.event(event_data);
        };

        tph.utility.gtm.track.event = function(event_data) {

            var sample_gtm_event_data = {
                event: 'UserInteraction', // used as a trigger in GTM
                UserInteractionCategory: 'UserInteraction',
                UserInteractionAction: 'click',
                UserInteractionLabel: 'single-posts-sidebar-trending-1',
                UserInteractionValue: 1,
            };

            // GA GTM Tracking
            try {
                // add uuid to all events
                event_data._tph_custom_uuids = tph.utility.uuid.get();
                dataLayer.push(event_data);
                tph.utility.debug.log('Fired GTM event - Success', event_data)
            } catch (error) {
                tph.utility.debug.log('Fired GTM event - Error', event_data, error);
            }

        };

        // parse.ly
        tph.utility.parsely = {};
        tph.utility.parsely.track = {};
        tph.utility.parsely.track.pageview = function(event_data) {
            if (typeof parselyPreload == 'undefined' || typeof PARSELY == 'undefined') {
                tph.utility.debug.log('Parsely not setup. Cannot fire parsely event.');
                return false;
            }

            // PAGEVIEW DATA
            var pageview_data = {};

            pageview_data.url = window.location.href;

            // if custom data is passed, then use it when recording the page view
            if (event_data) {
                if (event_data.url) {
                    pageview_data.url = event_data.url;
                }

                if (event_data.referrer) {
                    pageview_data.urlref = event_data.referrer;
                }

                if (event_data.data) {
                    pageview_data.data = event_data.data;
                }
            }

            pageview_data.data._tph_custom_uuids = tph.utility.uuid.get();

            pageview_data.js = 1; // flag indicating this is dynamic

            if (parselyPreload.loaded) {
                tph.utility.debug.log('Fired Parsely pageview.', pageview_data);
                PARSELY.beacon.trackPageView(pageview_data);
            } else {
                tph.utility.debug.log('Preloaded Parsely pageview.', pageview_data);
                parselyPreload.eventQueue.push(pageview_data);
            }

            pageview_data.timestamp = Number(+new Date() / 1000).toFixed(0);

            PARSELY.tph_events.push(pageview_data);
            tph.utility.debug.log(PARSELY.tph_events);
        };

        tph.utility.parsely.track.event = function(event_data) {
            // https://www.parse.ly/help/integration/dynamic/
            if (typeof event_data != 'object') {
                tph.utility.debug.log('Nothing to fire for parsely event.');
                return false;
            } else if (typeof parselyPreload == 'undefined' || typeof PARSELY == 'undefined') {
                tph.utility.debug.log('Parsely not setup. Cannot fire parsely event.');
                return false;
            }
            // Lets append UUID to every parsely event.
            if (typeof event_data.data != 'object') {
                event_data.data = {};
            }

            event_data.data._tph_custom_uuids = tph.utility.uuid.get();

            // EVENT DATA FORMAT
            var url = window.location.href;

            var sample_custom_event_data = {
                url: url,
                action: '_wfh_jobs_social_share_click',
                data: {
                    _wfh_jobs_social_share_platform: 'facebook',
                }
            };

            if (typeof event_data.url == 'undefined') {
                event_data.url = window.location.href;
            }

            event_data.js = 1;

            if (parselyPreload.loaded) {
                tph.utility.debug.log('Fired Parsely event.', event_data);
                PARSELY.beacon.trackPageView(event_data);
            } else {
                tph.utility.debug.log('Preloaded Parsely event.', event_data);
                parselyPreload.eventQueue.push(event_data);
            }

            event_data.timestamp = Number(+new Date() / 1000).toFixed(0);

            PARSELY.tph_events.push(event_data);
            tph.utility.debug.log(PARSELY.tph_events);
        };

        // cookie management
        tph.utility.cookie = {};
        tph.utility.cookie.create = function(name, value, days) {
            var expires = "";
            if (typeof days == 'undefined') {
                var days = 7;
            }
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + value + expires + "; path=/";
        };

        tph.utility.cookie.read = function(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) {
                        return c.substring(nameEQ.length, c.length);
                    }
                }
            }
            return false;
        };

        tph.utility.cookie.erase = function(name) {
            tph.utility.cookie.create(name, "", -1);
        };

        // UUID
        tph.utility.uuid = {};
        tph.utility.uuid.create = function() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            )
        };
        tph.utility.uuid.get = function(return_a_string_rather_than_an_object) {
            if (typeof return_a_string_rather_than_an_object == 'undefined') {
                var return_a_string_rather_than_an_object = false;
            }

            var tph_uuids_object = {};
            var tph_uuid;
            var ylm_uuid;
            var itr_uuid;

            // Check for existing LTV cookie.
            // This cookie is used as a target in Google Tag Manager for the Facebook multi step conversion.
            if (tph.utility.cookie.read('tph_uuid_LTV')) {
                tph_uuid = tph.utility.cookie.read('tph_uuid_LTV');
            } else { // If we didn't find one, set one!
                tph_uuid = tph.utility.uuid.create();
                tph.utility.cookie.create('tph_uuid_LTV', tph_uuid, 365);
            }

            // Check for existing YLM cookie.
            if (tph.utility.cookie.read('tph_uuid_YLM')) {
                ylm_uuid = tph.utility.cookie.read('tph_uuid_YLM');
            }

            // Check for existing ITR cookie.  If there's a fresher one, use that instead.
            if (tph.utility.cookie.read('tph_uuid_ITR')) {
                itr_uuid = tph.utility.cookie.read('tph_uuid_ITR');
            }

            if (tph.utility.get_browser_query_parameter('aff_unique2') !== false) {
                itr_uuid = tph.utility.get_browser_query_parameter('aff_unique2');
                if (itr_uuid.length >= 3) { // basic data check
                    tph.utility.cookie.create('tph_uuid_ITR', itr_uuid, 365);
                }
            }

            // Do you want a string back?  Clear transformation for HasOffers.
            if (return_a_string_rather_than_an_object) {
                var tph_uuids_string = "TPH-" + tph_uuid;
                if (typeof ylm_uuid != 'undefined') {
                    tph_uuids_string = tph_uuids_string + "|" + ylm_uuid;
                } else if (typeof itr_uuid != "undefined") {
                    tph_uuids_string = tph_uuids_string + "|" + itr_uuid;
                }
                return tph_uuids_string;
            }

            //Set the object for Parsely.
            tph_uuids_object = {
                "TPH": tph_uuid,
                "YLM": ylm_uuid,
                "ITR": itr_uuid
            };
            return tph_uuids_object;
        }

        // tools that affect the entire page

        // standardize affiliate links on the page
        // the PHP implementation of this script is located in tph-class.php
        tph.utility.append_tracking_to_affiliate_links = function(){
            try {
                var aff_offer_position = 0;
                jQuery('a[href*="go2cloud.org"],a[href*="//t.thepennyhoarder.com"]').each(function () {
                    var href = jQuery(this).attr('href');
                    
                    // just incase it's a bad root domain
                    href = href.replace('thepennyhoarder.go2cloud.org', 't.thepennyhoarder.com')
                    var variableStartPosition = href.indexOf('?');
                    var newHrefBase = href.substr(0, variableStartPosition);
                    var parameterString = ''
                    var linkQueryParams = tph.utility.get_params_from_url_as_object(href);
                    aff_offer_position++;
    
                    var attributeOrder = ['offer_id','campaign_id','aff_id','source','aff_sub','aff_sub2','aff_sub3','aff_sub4','aff_sub5','aff_unique1','aff_unique2','aff_unique3','aff_unique4'];
                    attributeOrder.forEach(function(paramKey, index){
                        var variable_value_in_browser_query_params = tph.utility.get_browser_query_parameter(paramKey);
    
                        if (typeof linkQueryParams[paramKey] != 'undefined') { 
                            parameterString = tph.utility.update_param_in_string(paramKey, linkQueryParams[paramKey], parameterString);
                        }
                        
                        // dynamically populate aff_sub5
                        if (paramKey == 'aff_sub5' && variable_value_in_browser_query_params === false) {
                            parameterString = tph.utility.update_param_in_string(paramKey, aff_offer_position, parameterString);
                        }
    
                        // dynamically populate aff_unique2 with the uuid
                        if (paramKey == 'aff_unique2') {
                            var uuid_string = tph.utility.uuid.get(true);
                            parameterString = tph.utility.update_param_in_string(paramKey, uuid_string, parameterString);
                        }
    
                        // dynamically populate source with the the post slug, minus the category
                        if (paramKey == 'source') {
                            var source = window.location.pathname.split('/');
                            source.pop();
                            var true_source = source[source.length - 1]
                            if (true_source == 'amp') {
                                true_source = source[source.length - 2];
                            }
                            parameterString = tph.utility.update_param_in_string(paramKey, true_source, parameterString);
                        }
    
                        // append the variable from the browser query string
                        if (variable_value_in_browser_query_params !== false) {
                            parameterString = tph.utility.update_param_in_string(paramKey, variable_value_in_browser_query_params, parameterString);
                        }
                        
                    })
    
                    var newHref = newHrefBase + '?' + parameterString.substr(1);
                    jQuery(this).attr('href', newHref);
                })
            } catch(error) {
                logThis("Error appending tracking to affiliate links: "+ error);
            }
        }

    });
}