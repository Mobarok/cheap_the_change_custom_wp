<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/5/2019
 * Time: 12:30 AM
 * Template Name: Net Worth Calculator
*/
get_header();
?>


<div class="page-container category-page">

    <div class="row breadcrumbs flex-row">
        <div class="container flex-container">
            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">

                <span property="itemListElement" typeof="ListItem">
                    <a property="item" typeof="WebPage" title="Go to Cheap The Change" href="<?php bloginfo('home')?>" class="home">
                        <span property="name">Home</span>
                    </a>
                    <meta property="position" content="1"></span>
                <span>&gt;</span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name">
                        <?php
                        echo get_the_title($post->ID);
                        ?></span>
                    <meta property="position" content="2"></span>
            </div>
        </div>
    </div>

    <div class="row category-trending  net-worth-page flex-row">
        <div class="container flex-container">
            <div id="fbuilder">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 " id="fbuilder_1" data-original-title="" title="">
                    <div id="formheader_1">
                        <div class="fform" id="field">
                            <h1>Net worth calculator</h1>
                            <span class="cff-form-description">Enter the monetary value of your assets and liabilities to find your net worth.</span>
                        </div>

                        <hr class="main-vertical-block-separator hidden-sm">
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <h3 class="text-center">Assets</h3>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname4_1" class="nw-has-popover">
                                    Real estate
                                    <span data-toggle="tooltip" title="The current market value of your home, if you own it, and other property" data-original-title="" title="">?</span>
                                </label>
                                <div class="dfield">
                                    <input id="fieldname4_1" name="fieldname4_1" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname4_2" class="nw-has-popover">
                                    Checking accounts
                                </label>
                                <div class="dfield">
                                    <input id="fieldname4_2" name="fieldname4_2" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname4_3" class="nw-has-popover">
                                    Savings accounts
                                </label>
                                <div class="dfield">
                                    <input id="fieldname4_3" name="fieldname4_3" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname4_4" class="nw-has-popover">
                                    Retirement accounts
                                </label>
                                <div class="dfield">
                                    <input id="fieldname4_4" name="fieldname4_4" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname4_5" class="nw-has-popover">
                                    Autos
                                </label>
                                <div class="dfield">
                                    <input id="fieldname4_5" name="fieldname4_5" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname4_6" class="nw-has-popover">
                                    Other
                                </label>
                                <div class="dfield">
                                    <input id="fieldname4_6" name="fieldname4_6" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 col-lg-5 col-md-push-2 col-lg-push-2 col-sm-12 col-xs-12">
                            <h3  class="text-center">Liabilities</h3>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname5_1" class="nw-has-popover">
                                    Mortgages
                                    <span data-toggle="tooltip" title="The current market value of your home, if you own it, and other property" data-original-title="" title="">?</span>
                                </label>
                                <div class="dfield">
                                    <input id="fieldname5_1" name="fieldname5_1" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname5_2" class="nw-has-popover">
                                    Consumer debt
                                </label>
                                <div class="dfield">
                                    <input id="fieldname5_2" name="fieldname5_2" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname5_3" class="nw-has-popover">
                                    Personal loans
                                </label>
                                <div class="dfield">
                                    <input id="fieldname5_3" name="fieldname5_3" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname5_4" class="nw-has-popover">
                                    Student loans
                                </label>
                                <div class="dfield">
                                    <input id="fieldname5_4" name="fieldname5_4" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname5_5" class="nw-has-popover">
                                    Auto loans
                                </label>
                                <div class="dfield">
                                    <input id="fieldname5_5" name="fieldname5_5" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                            <div class="fields " id="field_1-4">
                                <label for="fieldname5_6" class="nw-has-popover">
                                    Other debt
                                </label>
                                <div class="dfield">
                                    <input id="fieldname5_6" name="fieldname5_6" class="form-control" type="text" value="" placeholder="$0" data-previousvalue="1000">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class=" col-md-12 col-lg-12">
                        <h2>
                            Net worth
                        </h2>

                        <h2 class="worth-result">
                            $0
                        </h2>
                    </div>
                </div>

            </div>
        </div>
</div>

<?php
//    echo do_shortcode("[footer_image_navigation]");
?>
</div>

<script type="application/javascript">
    jQuery(document).ready( function () {

        var $body = jQuery('body');


        $('.dfield > input').keyup(function() {
//            var $elem = jQuery(this);
//            var input = $elem.val();
            var asset = 0;
            var libilities = 0;
            for (var index = 1; index < 7; index++) {
                var $elem = "#fieldname4_"+index;
                var input = $($elem).val();
                input = input != '' ? input : 0;
                asset +=parseInt(input);
//                console.log(index+"Val:  "+input);
            }

            for (var index = 1; index < 7; index++) {
                var $elem = "#fieldname5_"+index;
                var input = $($elem).val();
                input = input != '' ? input : 0;
                libilities +=parseInt(input);
//                console.log(index+"Val:  "+input);
            }

            $(".worth-result").text("$ "+(asset-libilities));

        });
    });
</script>
<?php
    get_footer();
?>
