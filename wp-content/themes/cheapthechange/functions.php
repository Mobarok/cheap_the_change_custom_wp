<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/15/2018
 * Time: 7:58 PM
 */


function default_functions(){

    add_theme_support('title-tag');
    add_theme_support('custom-header');
    add_theme_support('post-thumbnails');
//    add_image_size( 'post', 750, 360, true );
//    add_image_size( 'related-post', 222, 148, true );
//    add_image_size( 'post-img-sidebar', 50, 50, true );


    load_theme_textdomain('cheapTheChange', get_template_directory_uri().'/languages');

    if(function_exists('register_nav_menu')){

        register_nav_menu('main-menu', __('Main Menu', 'cheapTheChange'));
        register_nav_menu('footer-menu', __('Footer Menu', 'cheapTheChange'));
    }



    if(function_exists('get_field')){
        if(get_field('get_custom_field')){
            the_field('some_custom_field');
        }
    }


    /**
     * Custom template tags for this theme.
     */
    require get_template_directory() . '/inc/WP_Bootstrap_Navwalker.php';
    require get_template_directory() . '/inc/shortcode.php';
}


function widgets_design(){

    register_sidebar(array(
        'name' => 'Left Sidebar',
        'description' => 'Include Left sidebar widgets',
        'id' => 'left-sidebar',
    ));

    register_sidebar(array(
        'name' => 'Right Sidebar',
        'description' => 'Include Right sidebar widgets',
        'id' => 'right-sidebar',
    ));

    register_sidebar(array(
        'name' => __('Top Footer Panel', 'top_footer'),
        'description' => 'Include Top Footer panel',
        'id' => 'top-footer-panel',
    ));
}
//
//// function to count views.
//function setPostViews_anthemes($postID) {
//    $count_key = 'post_views_count';
//    $count = rand(1,99);
//    update_post_meta($postID, $count_key, $count);
//}
//
//for($i=12106; $i<12157; $i++){
//    setPostViews_anthemes($i);
//}


function time_ago( $type = 'post' ) {

    $d = 'comment' == $type ? 'get_comment_time' : 'get_post_time';
    $time_ago =  human_time_diff($d('U'), current_time('timestamp'));

    $arr = explode(' ', $time_ago);

    if($arr['1']=='hours'):
        return $time_ago." " . __('ago');
    else:
        return  $d('F j, Y');
    endif;

}

function create_slug($string){
    $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    return $slug;
}


// function to display number of posts.
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}


// Add it to a column in WP-Admin
add_filter('manage_posts_columns', 'posts_column_views');
add_action('manage_posts_custom_column', 'posts_custom_column_views',5,2);
function posts_column_views($defaults){
    $defaults['post_views'] = __('Views');
    return $defaults;
}
function posts_custom_column_views($column_name, $id){
    if($column_name === 'post_views'){
        echo getPostViews(get_the_ID());
    }
}

add_action('after_setup_theme', 'default_functions');
// Admin top header
add_filter('show_admin_bar','__return_false');
//add_filter('','do_shortcode');
add_action('widgets_init','widgets_design');
add_filter('the_content', 'do_shortcode');



?>