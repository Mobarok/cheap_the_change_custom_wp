<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/18/2018
 * Time: 1:37 PM
 *
 * Template Name:Category Post by Page
 *
 */

get_header();
?>
<div class="page-container category-page">

    <div class="row breadcrumbs flex-row">
        <div class="container flex-container">
            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">

                <span property="itemListElement" typeof="ListItem">
                    <a property="item" typeof="WebPage" title="Go to Cheap The Change" href="<?php bloginfo('home')?>" class="home">
                        <span property="name">Home</span>
                    </a>
                    <meta property="position" content="1"></span>
                <span>&gt;</span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name">
                        <?php
                            echo get_the_title($post->ID);
                        ?></span>
                    <meta property="position" content="2"></span>
            </div>
        </div>
    </div>




        <?php
            echo do_shortcode("[single_top_post_by_category category='".$post->post_name."']");
        ?>


    <?php
    echo do_shortcode("[five_sub_category_post category='".$post->post_name."']");
    ?>

    <div class="row category-subcategories flex-row">
        <div class="container flex-container">

            <div class="category-subcategory-wrapper">
                <div class="col-xs-12 category-subcategory-title text-center">
                    <h3 class="hidden-xs"><span>More</span></h3>
                    <h3 class="hidden-sm hidden-md hidden-lg"><span>More</span></h3>
                </div>

                <?php
                    $queryObject = new  Wp_Query( array(
                        'posts_per_page'=> 55,
                        'ignore_sticky_posts' => 0,
                        'category__not_in' => array( 86, 66, 67, 1 ),
                        'post__in'  => get_option( 'sticky_posts' ),
                    ));

                    $i = $j = 1;

                    if ( $queryObject->have_posts() ) :
                    while ( $queryObject->have_posts() ) :
                    $queryObject->the_post();

                    if($i==1){

                ?>

                <div class="col-xs-12 category-subcategory-posts text-center <?php if($j>1){ echo 'category-main-load-more-'.($j-1); } ?>" <?php if($j>1) {?>style="display: none;" <?php } ?> >

                    <?php $j++; } ?>

                    <div class="col-xs-12 col-sm-2 category-subcategory-post text-center">
                        <div class="col-xs-12 category-subcategory-post-image">
                            <a href="<?php echo esc_url(get_permalink());?>" class="">
                                <div class="four_by_six">
                                    <?php
                                    if(has_post_thumbnail()):
                                        the_post_thumbnail('post');
                                    endif
                                    ?>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 category-subcategory-post-content">
                            <a href="<?php echo esc_url(get_permalink());?>" class="photo-essay-article-content-title">
                                <?php the_title();?>
                            </a>
                        </div>
                    </div>

                    <?php
                    if($i==5){
                        $i=0;
                        echo "</div>";
                    }
                    $i++;

                    endwhile;
                    endif;
                wp_reset_postdata();
                ?>

            </div>
    </div>

    <div class="row load-more-button">
        <div class="container flex-container text-center">
            <a href="#" class="btn btn-default btn-main-load-more">
                Load More
            </a>
        </div>
    </div>

    <div class="row category-trending flex-row">
        <div class="container flex-container">
            <div class="category-subcategory-wrapper">
                <div class="col-xs-12 category-subcategory-title text-center">
                    <h3 class="hidden-xs"><span>Trending in <?php echo get_the_title($post->ID);?></span></h3>
                    <h3 class="hidden-sm hidden-md hidden-lg">
                        <span>Trending in <?php echo get_the_title($post->ID);?></span>
                    </h3>
                </div>
            </div>


            <div class="col-xs-12 category-subcategory-posts category-trending-posts text-center">
            <?php
                echo do_shortcode("[trending_post_show popular='Yes' category='".$post->post_name."']");

            ?>
            </div>


        </div>
    </div>

<?php
        echo do_shortcode("[footer_sign_up]");
//        echo do_shortcode("[footer_image_navigation]");

    ?>
</div>


<?php
get_footer();
?>


