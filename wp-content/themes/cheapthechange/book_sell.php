<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/4/2019
 * Time: 5:42 PM
 *
 * Template Name:Books
 */
get_header();
?>

<div class="page-container category-page">

    <div class="row breadcrumbs flex-row">
        <div class="container flex-container">
            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">

                <span property="itemListElement" typeof="ListItem">
                    <a property="item" typeof="WebPage" title="Go to Cheap The Change" href="<?php bloginfo('home')?>" class="home">
                        <span property="name">Home</span>
                    </a>
                    <meta property="position" content="1"></span>
                <span>&gt;</span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name">
                        <?php
                        echo get_the_title($post->ID);
                        ?></span>
                    <meta property="position" content="2"></span>
            </div>
        </div>
    </div>

    <div class="row category-trending flex-row">
        <div class="container flex-container">
            <div class="category-subcategory-wrapper">
                <div class="col-xs-12 category-subcategory-title text-center">
                    <h3 class="hidden-xs"><span>Buy Our <?php echo get_the_title($post->ID);?></span></h3>
                    <h3 class="hidden-sm hidden-md hidden-lg">
                        <span>Buy Our <?php echo get_the_title($post->ID);?></span>
                    </h3>
                </div>
            </div>

            <div class="col-xs-12 category-subcategory-posts category-trending-posts category-books text-center">

                <?php
                $queryObject = new  Wp_Query( array(
                    'post_type' => array('post'),
                    'category_name' => 'book',
                    'order'            => 'DESC'
                ));

                if ( $queryObject->have_posts() ) :
                    while ( $queryObject->have_posts() ) :
                        $queryObject->the_post();
                ?>
                <div class="col-md-3 book-img">
                    <?php if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'post');
                    } ?>
                </div>
                <div class="col-md-7 book-desc">
                    <h3 class="text-left"> <?php the_title()?> </h3>
                    <h6 class="main-vertical-block-featured-post-author author-name-text text-left">
                        By <?php the_author()?></h6>
                    <p class="book-content">
                        <?php the_content(); ?>
                    </p>
                </div>
                <hr class="main-vertical-block-separator hidden-sm">
                <?php
                    endwhile;
                    endif;
                    wp_reset_postdata();
                ?>

            </div>


        </div>
    </div>

    <?php
//    echo do_shortcode("[footer_image_navigation]");

    ?>
</div>


<?php
get_footer();
?>

